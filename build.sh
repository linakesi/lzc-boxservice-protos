#!/bin/bash
set -e

IMAGE=registry.corp.lazycat.cloud/lzc/lzc-api-protoc

docker run -ti -w "$PWD" -v "$PWD:$PWD" --rm  --user "$(id -u):$(id -g)" "$IMAGE" ./tools/proto-build.sh

# docker run -ti -w "$PWD" -v "$PWD:$PWD" --rm "$IMAGE" chown -R $UID:$UID ./

(
  cd ./lang/go
  go mod tidy
  go list ./... | xargs go build
)
