/* eslint-disable */
import { grpc } from "@improbable-eng/grpc-web";
import { BrowserHeaders } from "browser-headers";
import Long from "long";
import _m0 from "protobufjs/minimal";
import { Observable } from "rxjs";
import { share } from "rxjs/operators";
import { Empty } from "../../google/protobuf/empty";

export enum KeyMgmt {
  KeyMgmtNone = 0,
  /** KeyMgmtWEP - WEP  （最老的协议了，目前几乎没人用） */
  KeyMgmtWEP = 1,
  /** KeyMgmtWPA_PSK - WPA/WPA2-Personal  （一般大概率都是这个，应当作为默认值） */
  KeyMgmtWPA_PSK = 2,
  /** KeyMgmtSAE - WPA3-Personal  （新一代协议，用的人比较少） */
  KeyMgmtSAE = 3,
  UNRECOGNIZED = -1,
}

export function keyMgmtFromJSON(object: any): KeyMgmt {
  switch (object) {
    case 0:
    case "KeyMgmtNone":
      return KeyMgmt.KeyMgmtNone;
    case 1:
    case "KeyMgmtWEP":
      return KeyMgmt.KeyMgmtWEP;
    case 2:
    case "KeyMgmtWPA_PSK":
      return KeyMgmt.KeyMgmtWPA_PSK;
    case 3:
    case "KeyMgmtSAE":
      return KeyMgmt.KeyMgmtSAE;
    case -1:
    case "UNRECOGNIZED":
    default:
      return KeyMgmt.UNRECOGNIZED;
  }
}

export function keyMgmtToJSON(object: KeyMgmt): string {
  switch (object) {
    case KeyMgmt.KeyMgmtNone:
      return "KeyMgmtNone";
    case KeyMgmt.KeyMgmtWEP:
      return "KeyMgmtWEP";
    case KeyMgmt.KeyMgmtWPA_PSK:
      return "KeyMgmtWPA_PSK";
    case KeyMgmt.KeyMgmtSAE:
      return "KeyMgmtSAE";
    case KeyMgmt.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export enum WifiConnectResult {
  WifiConnectResultSuccess = 0,
  WifiConnectResultNoSuchBssid = 1,
  WifiConnectResultWrongPassword = 2,
  WifiConnectResultUnknownError = 3,
  UNRECOGNIZED = -1,
}

export function wifiConnectResultFromJSON(object: any): WifiConnectResult {
  switch (object) {
    case 0:
    case "WifiConnectResultSuccess":
      return WifiConnectResult.WifiConnectResultSuccess;
    case 1:
    case "WifiConnectResultNoSuchBssid":
      return WifiConnectResult.WifiConnectResultNoSuchBssid;
    case 2:
    case "WifiConnectResultWrongPassword":
      return WifiConnectResult.WifiConnectResultWrongPassword;
    case 3:
    case "WifiConnectResultUnknownError":
      return WifiConnectResult.WifiConnectResultUnknownError;
    case -1:
    case "UNRECOGNIZED":
    default:
      return WifiConnectResult.UNRECOGNIZED;
  }
}

export function wifiConnectResultToJSON(object: WifiConnectResult): string {
  switch (object) {
    case WifiConnectResult.WifiConnectResultSuccess:
      return "WifiConnectResultSuccess";
    case WifiConnectResult.WifiConnectResultNoSuchBssid:
      return "WifiConnectResultNoSuchBssid";
    case WifiConnectResult.WifiConnectResultWrongPassword:
      return "WifiConnectResultWrongPassword";
    case WifiConnectResult.WifiConnectResultUnknownError:
      return "WifiConnectResultUnknownError";
    case WifiConnectResult.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface GetDataDisksCountReply {
  count: number;
}

export interface IsBoxSetupFinishResponse {
  finish: boolean;
}

export interface EncryptTestRequest {
  passwd: string;
}

export interface RegisterBoxResponse {
  /** 初始化成功 */
  success: boolean;
  /** 失败原因 */
  failedReason?: string | undefined;
}

export interface RegisterBoxRequest {
  /** 微服名 */
  boxname: string;
  /** 管理员名 */
  user: string;
  /** 密码 */
  passwd: string;
}

export interface ButtonTestReply {
  /** 按钮状态 */
  status: ButtonTestReply_Status;
}

export enum ButtonTestReply_Status {
  /** Pending - 等待按下 */
  Pending = 0,
  /** Pressed - 按钮被按下 */
  Pressed = 1,
  /** Timeout - 等待超时 */
  Timeout = 2,
  /** Done - 结束 */
  Done = 3,
  UNRECOGNIZED = -1,
}

export function buttonTestReply_StatusFromJSON(object: any): ButtonTestReply_Status {
  switch (object) {
    case 0:
    case "Pending":
      return ButtonTestReply_Status.Pending;
    case 1:
    case "Pressed":
      return ButtonTestReply_Status.Pressed;
    case 2:
    case "Timeout":
      return ButtonTestReply_Status.Timeout;
    case 3:
    case "Done":
      return ButtonTestReply_Status.Done;
    case -1:
    case "UNRECOGNIZED":
    default:
      return ButtonTestReply_Status.UNRECOGNIZED;
  }
}

export function buttonTestReply_StatusToJSON(object: ButtonTestReply_Status): string {
  switch (object) {
    case ButtonTestReply_Status.Pending:
      return "Pending";
    case ButtonTestReply_Status.Pressed:
      return "Pressed";
    case ButtonTestReply_Status.Timeout:
      return "Timeout";
    case ButtonTestReply_Status.Done:
      return "Done";
    case ButtonTestReply_Status.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface WifiConnectInfo {
  /** bssid 和 ssid 指定其一即可 */
  bssid: string;
  ssid: string;
  password: string;
  keyMgmt?: KeyMgmt | undefined;
}

export interface WifiConnectReply {
  result: WifiConnectResult;
}

export interface AccessPointInfoList {
  list: AccessPointInfo[];
}

export interface AccessPointInfo {
  /** 热点的网卡 mac 地址（由于 ssid 可能重复，所以将此字段作为整个列表的 index） */
  bssid: string;
  /** 热点的 ssid */
  ssid: string;
  /** 信号强度（范围 0 <= signal <= 100） */
  signal: number;
  /** 是否需要密码 */
  security: boolean;
  /** 是否已连接 */
  connected: boolean;
  /** 是否已保存密码 */
  saved: boolean;
  /** 是否自动连接 */
  autoConnected: boolean;
}

export interface NetworkDeviceStatusInfoRespone {
  /** 是否有互联网 */
  hasInternet: boolean;
  /** 有线连接状态（已假设只有一块有线网卡） */
  WiredDevice: NetworkDeviceStatusInfoRespone_Status;
  /** 无线设备状态（已假设只有一块无线网卡） */
  WirelessDevice: NetworkDeviceStatusInfoRespone_Status;
}

export enum NetworkDeviceStatusInfoRespone_Status {
  Disconnected = 0,
  Connecting = 1,
  Connected = 2,
  Disconnecting = 3,
  Disabled = 4,
  UNRECOGNIZED = -1,
}

export function networkDeviceStatusInfoRespone_StatusFromJSON(object: any): NetworkDeviceStatusInfoRespone_Status {
  switch (object) {
    case 0:
    case "Disconnected":
      return NetworkDeviceStatusInfoRespone_Status.Disconnected;
    case 1:
    case "Connecting":
      return NetworkDeviceStatusInfoRespone_Status.Connecting;
    case 2:
    case "Connected":
      return NetworkDeviceStatusInfoRespone_Status.Connected;
    case 3:
    case "Disconnecting":
      return NetworkDeviceStatusInfoRespone_Status.Disconnecting;
    case 4:
    case "Disabled":
      return NetworkDeviceStatusInfoRespone_Status.Disabled;
    case -1:
    case "UNRECOGNIZED":
    default:
      return NetworkDeviceStatusInfoRespone_Status.UNRECOGNIZED;
  }
}

export function networkDeviceStatusInfoRespone_StatusToJSON(object: NetworkDeviceStatusInfoRespone_Status): string {
  switch (object) {
    case NetworkDeviceStatusInfoRespone_Status.Disconnected:
      return "Disconnected";
    case NetworkDeviceStatusInfoRespone_Status.Connecting:
      return "Connecting";
    case NetworkDeviceStatusInfoRespone_Status.Connected:
      return "Connected";
    case NetworkDeviceStatusInfoRespone_Status.Disconnecting:
      return "Disconnecting";
    case NetworkDeviceStatusInfoRespone_Status.Disabled:
      return "Disabled";
    case NetworkDeviceStatusInfoRespone_Status.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface BoxNameRegisteredRequest {
  boxName: string;
}

export interface BoxNameRegisteredReply {
  status: BoxNameRegisteredReply_Status;
}

export enum BoxNameRegisteredReply_Status {
  NOT_AVAILABLE = 0,
  OK = 1,
  NEED_RECYCLE = 2,
  UNRECOGNIZED = -1,
}

export function boxNameRegisteredReply_StatusFromJSON(object: any): BoxNameRegisteredReply_Status {
  switch (object) {
    case 0:
    case "NOT_AVAILABLE":
      return BoxNameRegisteredReply_Status.NOT_AVAILABLE;
    case 1:
    case "OK":
      return BoxNameRegisteredReply_Status.OK;
    case 2:
    case "NEED_RECYCLE":
      return BoxNameRegisteredReply_Status.NEED_RECYCLE;
    case -1:
    case "UNRECOGNIZED":
    default:
      return BoxNameRegisteredReply_Status.UNRECOGNIZED;
  }
}

export function boxNameRegisteredReply_StatusToJSON(object: BoxNameRegisteredReply_Status): string {
  switch (object) {
    case BoxNameRegisteredReply_Status.NOT_AVAILABLE:
      return "NOT_AVAILABLE";
    case BoxNameRegisteredReply_Status.OK:
      return "OK";
    case BoxNameRegisteredReply_Status.NEED_RECYCLE:
      return "NEED_RECYCLE";
    case BoxNameRegisteredReply_Status.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface BoxSetupRequest {
  originServer: string;
  boxName: string;
  userInfo: BoxSetupRequest_UserInfo | undefined;
  diskInfo: BoxSetupRequest_DiskInfo | undefined;
}

export interface BoxSetupRequest_UserInfo {
  username: string;
  password: string;
  phonenumber?: string | undefined;
}

/** 此处 copy 自 ./proto/recovery/box-status.proto ，请在修改时保持同步 */
export interface BoxSetupRequest_DiskInfo {
  password: string;
  /** 是否使用 bcache */
  bcache: boolean;
  /**
   * 磁盘阵列 raid 相关
   * btrfs 对此选项的标准全称叫作 "block group profile"
   * 不主动设置，即使用 btrfs 默认，当前默认是 data=single, metadata=dup
   * 我厌倦了搞一堆 enum 然后 string 和 enum 转换来转换去，还不如直接进 string 呢
   */
  dataProfile: string;
  metadataProfile: string;
}

export interface BoxSetupReply {
  status: BoxSetupReply_Status;
  failedStatus?:
    | BoxSetupReply_FailedStatus
    | undefined;
  /**
   * 如果是InitializeFailed，那么可能有如下失败原因
   * 微服名被别人注册了
   * 微服名不符合规范
   * 账号密码不符合规范
   * 其他的失败就直接用其字面意思就行
   */
  failedReason?: string | undefined;
}

export enum BoxSetupReply_Status {
  /** PendingButtonClick - 等待按钮点击 */
  PendingButtonClick = 0,
  /** Initializing - 初始化中，进行申请证书/域名等操作 */
  Initializing = 1,
  /** Failed - 失败 */
  Failed = 2,
  /** Success - 成功 */
  Success = 3,
  UNRECOGNIZED = -1,
}

export function boxSetupReply_StatusFromJSON(object: any): BoxSetupReply_Status {
  switch (object) {
    case 0:
    case "PendingButtonClick":
      return BoxSetupReply_Status.PendingButtonClick;
    case 1:
    case "Initializing":
      return BoxSetupReply_Status.Initializing;
    case 2:
    case "Failed":
      return BoxSetupReply_Status.Failed;
    case 3:
    case "Success":
      return BoxSetupReply_Status.Success;
    case -1:
    case "UNRECOGNIZED":
    default:
      return BoxSetupReply_Status.UNRECOGNIZED;
  }
}

export function boxSetupReply_StatusToJSON(object: BoxSetupReply_Status): string {
  switch (object) {
    case BoxSetupReply_Status.PendingButtonClick:
      return "PendingButtonClick";
    case BoxSetupReply_Status.Initializing:
      return "Initializing";
    case BoxSetupReply_Status.Failed:
      return "Failed";
    case BoxSetupReply_Status.Success:
      return "Success";
    case BoxSetupReply_Status.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export enum BoxSetupReply_FailedStatus {
  /** WaitButtonTimeout - 等待点击按钮超时 */
  WaitButtonTimeout = 0,
  /** InitializeFailed - 初始化失败 */
  InitializeFailed = 1,
  UNRECOGNIZED = -1,
}

export function boxSetupReply_FailedStatusFromJSON(object: any): BoxSetupReply_FailedStatus {
  switch (object) {
    case 0:
    case "WaitButtonTimeout":
      return BoxSetupReply_FailedStatus.WaitButtonTimeout;
    case 1:
    case "InitializeFailed":
      return BoxSetupReply_FailedStatus.InitializeFailed;
    case -1:
    case "UNRECOGNIZED":
    default:
      return BoxSetupReply_FailedStatus.UNRECOGNIZED;
  }
}

export function boxSetupReply_FailedStatusToJSON(object: BoxSetupReply_FailedStatus): string {
  switch (object) {
    case BoxSetupReply_FailedStatus.WaitButtonTimeout:
      return "WaitButtonTimeout";
    case BoxSetupReply_FailedStatus.InitializeFailed:
      return "InitializeFailed";
    case BoxSetupReply_FailedStatus.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface HasInternetResponse {
  result: boolean;
}

export interface ConnectivityRequest {
  url: string;
}

export interface ConnectivityReply {
  connectivity: ConnectivityReply_Connectivity;
}

export enum ConnectivityReply_Connectivity {
  None = 0,
  Portal = 1,
  Limited = 2,
  Full = 3,
  Unknown = 4,
  UNRECOGNIZED = -1,
}

export function connectivityReply_ConnectivityFromJSON(object: any): ConnectivityReply_Connectivity {
  switch (object) {
    case 0:
    case "None":
      return ConnectivityReply_Connectivity.None;
    case 1:
    case "Portal":
      return ConnectivityReply_Connectivity.Portal;
    case 2:
    case "Limited":
      return ConnectivityReply_Connectivity.Limited;
    case 3:
    case "Full":
      return ConnectivityReply_Connectivity.Full;
    case 4:
    case "Unknown":
      return ConnectivityReply_Connectivity.Unknown;
    case -1:
    case "UNRECOGNIZED":
    default:
      return ConnectivityReply_Connectivity.UNRECOGNIZED;
  }
}

export function connectivityReply_ConnectivityToJSON(object: ConnectivityReply_Connectivity): string {
  switch (object) {
    case ConnectivityReply_Connectivity.None:
      return "None";
    case ConnectivityReply_Connectivity.Portal:
      return "Portal";
    case ConnectivityReply_Connectivity.Limited:
      return "Limited";
    case ConnectivityReply_Connectivity.Full:
      return "Full";
    case ConnectivityReply_Connectivity.Unknown:
      return "Unknown";
    case ConnectivityReply_Connectivity.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface GetConnectivityReply {
  connectivity: GetConnectivityReply_Connectivity;
}

export enum GetConnectivityReply_Connectivity {
  None = 0,
  Portal = 1,
  Limited = 2,
  Full = 3,
  Unknown = 4,
  UNRECOGNIZED = -1,
}

export function getConnectivityReply_ConnectivityFromJSON(object: any): GetConnectivityReply_Connectivity {
  switch (object) {
    case 0:
    case "None":
      return GetConnectivityReply_Connectivity.None;
    case 1:
    case "Portal":
      return GetConnectivityReply_Connectivity.Portal;
    case 2:
    case "Limited":
      return GetConnectivityReply_Connectivity.Limited;
    case 3:
    case "Full":
      return GetConnectivityReply_Connectivity.Full;
    case 4:
    case "Unknown":
      return GetConnectivityReply_Connectivity.Unknown;
    case -1:
    case "UNRECOGNIZED":
    default:
      return GetConnectivityReply_Connectivity.UNRECOGNIZED;
  }
}

export function getConnectivityReply_ConnectivityToJSON(object: GetConnectivityReply_Connectivity): string {
  switch (object) {
    case GetConnectivityReply_Connectivity.None:
      return "None";
    case GetConnectivityReply_Connectivity.Portal:
      return "Portal";
    case GetConnectivityReply_Connectivity.Limited:
      return "Limited";
    case GetConnectivityReply_Connectivity.Full:
      return "Full";
    case GetConnectivityReply_Connectivity.Unknown:
      return "Unknown";
    case GetConnectivityReply_Connectivity.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface NmcliCallRequest {
  /** nmcli connection add */
  args: string[];
  uploads: NmcliCallRequest_Upload[];
}

export interface NmcliCallRequest_Upload {
  filename: string;
  content: Uint8Array;
}

export interface NmcliCallReply {
  /** 命令的输出 */
  out: string;
}

function createBaseGetDataDisksCountReply(): GetDataDisksCountReply {
  return { count: 0 };
}

export const GetDataDisksCountReply = {
  encode(message: GetDataDisksCountReply, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.count !== 0) {
      writer.uint32(8).int64(message.count);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetDataDisksCountReply {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetDataDisksCountReply();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.count = longToNumber(reader.int64() as Long);
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): GetDataDisksCountReply {
    return { count: isSet(object.count) ? Number(object.count) : 0 };
  },

  toJSON(message: GetDataDisksCountReply): unknown {
    const obj: any = {};
    if (message.count !== 0) {
      obj.count = Math.round(message.count);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<GetDataDisksCountReply>, I>>(base?: I): GetDataDisksCountReply {
    return GetDataDisksCountReply.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<GetDataDisksCountReply>, I>>(object: I): GetDataDisksCountReply {
    const message = createBaseGetDataDisksCountReply();
    message.count = object.count ?? 0;
    return message;
  },
};

function createBaseIsBoxSetupFinishResponse(): IsBoxSetupFinishResponse {
  return { finish: false };
}

export const IsBoxSetupFinishResponse = {
  encode(message: IsBoxSetupFinishResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.finish === true) {
      writer.uint32(8).bool(message.finish);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): IsBoxSetupFinishResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseIsBoxSetupFinishResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.finish = reader.bool();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): IsBoxSetupFinishResponse {
    return { finish: isSet(object.finish) ? Boolean(object.finish) : false };
  },

  toJSON(message: IsBoxSetupFinishResponse): unknown {
    const obj: any = {};
    if (message.finish === true) {
      obj.finish = message.finish;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<IsBoxSetupFinishResponse>, I>>(base?: I): IsBoxSetupFinishResponse {
    return IsBoxSetupFinishResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<IsBoxSetupFinishResponse>, I>>(object: I): IsBoxSetupFinishResponse {
    const message = createBaseIsBoxSetupFinishResponse();
    message.finish = object.finish ?? false;
    return message;
  },
};

function createBaseEncryptTestRequest(): EncryptTestRequest {
  return { passwd: "" };
}

export const EncryptTestRequest = {
  encode(message: EncryptTestRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.passwd !== "") {
      writer.uint32(10).string(message.passwd);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): EncryptTestRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseEncryptTestRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.passwd = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): EncryptTestRequest {
    return { passwd: isSet(object.passwd) ? String(object.passwd) : "" };
  },

  toJSON(message: EncryptTestRequest): unknown {
    const obj: any = {};
    if (message.passwd !== "") {
      obj.passwd = message.passwd;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<EncryptTestRequest>, I>>(base?: I): EncryptTestRequest {
    return EncryptTestRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<EncryptTestRequest>, I>>(object: I): EncryptTestRequest {
    const message = createBaseEncryptTestRequest();
    message.passwd = object.passwd ?? "";
    return message;
  },
};

function createBaseRegisterBoxResponse(): RegisterBoxResponse {
  return { success: false, failedReason: undefined };
}

export const RegisterBoxResponse = {
  encode(message: RegisterBoxResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.success === true) {
      writer.uint32(8).bool(message.success);
    }
    if (message.failedReason !== undefined) {
      writer.uint32(18).string(message.failedReason);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): RegisterBoxResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseRegisterBoxResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.success = reader.bool();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.failedReason = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): RegisterBoxResponse {
    return {
      success: isSet(object.success) ? Boolean(object.success) : false,
      failedReason: isSet(object.failedReason) ? String(object.failedReason) : undefined,
    };
  },

  toJSON(message: RegisterBoxResponse): unknown {
    const obj: any = {};
    if (message.success === true) {
      obj.success = message.success;
    }
    if (message.failedReason !== undefined) {
      obj.failedReason = message.failedReason;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<RegisterBoxResponse>, I>>(base?: I): RegisterBoxResponse {
    return RegisterBoxResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<RegisterBoxResponse>, I>>(object: I): RegisterBoxResponse {
    const message = createBaseRegisterBoxResponse();
    message.success = object.success ?? false;
    message.failedReason = object.failedReason ?? undefined;
    return message;
  },
};

function createBaseRegisterBoxRequest(): RegisterBoxRequest {
  return { boxname: "", user: "", passwd: "" };
}

export const RegisterBoxRequest = {
  encode(message: RegisterBoxRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.boxname !== "") {
      writer.uint32(10).string(message.boxname);
    }
    if (message.user !== "") {
      writer.uint32(18).string(message.user);
    }
    if (message.passwd !== "") {
      writer.uint32(26).string(message.passwd);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): RegisterBoxRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseRegisterBoxRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.boxname = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.user = reader.string();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.passwd = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): RegisterBoxRequest {
    return {
      boxname: isSet(object.boxname) ? String(object.boxname) : "",
      user: isSet(object.user) ? String(object.user) : "",
      passwd: isSet(object.passwd) ? String(object.passwd) : "",
    };
  },

  toJSON(message: RegisterBoxRequest): unknown {
    const obj: any = {};
    if (message.boxname !== "") {
      obj.boxname = message.boxname;
    }
    if (message.user !== "") {
      obj.user = message.user;
    }
    if (message.passwd !== "") {
      obj.passwd = message.passwd;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<RegisterBoxRequest>, I>>(base?: I): RegisterBoxRequest {
    return RegisterBoxRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<RegisterBoxRequest>, I>>(object: I): RegisterBoxRequest {
    const message = createBaseRegisterBoxRequest();
    message.boxname = object.boxname ?? "";
    message.user = object.user ?? "";
    message.passwd = object.passwd ?? "";
    return message;
  },
};

function createBaseButtonTestReply(): ButtonTestReply {
  return { status: 0 };
}

export const ButtonTestReply = {
  encode(message: ButtonTestReply, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.status !== 0) {
      writer.uint32(8).int32(message.status);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ButtonTestReply {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseButtonTestReply();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.status = reader.int32() as any;
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): ButtonTestReply {
    return { status: isSet(object.status) ? buttonTestReply_StatusFromJSON(object.status) : 0 };
  },

  toJSON(message: ButtonTestReply): unknown {
    const obj: any = {};
    if (message.status !== 0) {
      obj.status = buttonTestReply_StatusToJSON(message.status);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<ButtonTestReply>, I>>(base?: I): ButtonTestReply {
    return ButtonTestReply.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<ButtonTestReply>, I>>(object: I): ButtonTestReply {
    const message = createBaseButtonTestReply();
    message.status = object.status ?? 0;
    return message;
  },
};

function createBaseWifiConnectInfo(): WifiConnectInfo {
  return { bssid: "", ssid: "", password: "", keyMgmt: undefined };
}

export const WifiConnectInfo = {
  encode(message: WifiConnectInfo, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.bssid !== "") {
      writer.uint32(10).string(message.bssid);
    }
    if (message.ssid !== "") {
      writer.uint32(18).string(message.ssid);
    }
    if (message.password !== "") {
      writer.uint32(26).string(message.password);
    }
    if (message.keyMgmt !== undefined) {
      writer.uint32(32).int32(message.keyMgmt);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): WifiConnectInfo {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseWifiConnectInfo();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.bssid = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.ssid = reader.string();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.password = reader.string();
          continue;
        case 4:
          if (tag !== 32) {
            break;
          }

          message.keyMgmt = reader.int32() as any;
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): WifiConnectInfo {
    return {
      bssid: isSet(object.bssid) ? String(object.bssid) : "",
      ssid: isSet(object.ssid) ? String(object.ssid) : "",
      password: isSet(object.password) ? String(object.password) : "",
      keyMgmt: isSet(object.keyMgmt) ? keyMgmtFromJSON(object.keyMgmt) : undefined,
    };
  },

  toJSON(message: WifiConnectInfo): unknown {
    const obj: any = {};
    if (message.bssid !== "") {
      obj.bssid = message.bssid;
    }
    if (message.ssid !== "") {
      obj.ssid = message.ssid;
    }
    if (message.password !== "") {
      obj.password = message.password;
    }
    if (message.keyMgmt !== undefined) {
      obj.keyMgmt = keyMgmtToJSON(message.keyMgmt);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<WifiConnectInfo>, I>>(base?: I): WifiConnectInfo {
    return WifiConnectInfo.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<WifiConnectInfo>, I>>(object: I): WifiConnectInfo {
    const message = createBaseWifiConnectInfo();
    message.bssid = object.bssid ?? "";
    message.ssid = object.ssid ?? "";
    message.password = object.password ?? "";
    message.keyMgmt = object.keyMgmt ?? undefined;
    return message;
  },
};

function createBaseWifiConnectReply(): WifiConnectReply {
  return { result: 0 };
}

export const WifiConnectReply = {
  encode(message: WifiConnectReply, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.result !== 0) {
      writer.uint32(8).int32(message.result);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): WifiConnectReply {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseWifiConnectReply();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.result = reader.int32() as any;
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): WifiConnectReply {
    return { result: isSet(object.result) ? wifiConnectResultFromJSON(object.result) : 0 };
  },

  toJSON(message: WifiConnectReply): unknown {
    const obj: any = {};
    if (message.result !== 0) {
      obj.result = wifiConnectResultToJSON(message.result);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<WifiConnectReply>, I>>(base?: I): WifiConnectReply {
    return WifiConnectReply.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<WifiConnectReply>, I>>(object: I): WifiConnectReply {
    const message = createBaseWifiConnectReply();
    message.result = object.result ?? 0;
    return message;
  },
};

function createBaseAccessPointInfoList(): AccessPointInfoList {
  return { list: [] };
}

export const AccessPointInfoList = {
  encode(message: AccessPointInfoList, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.list) {
      AccessPointInfo.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): AccessPointInfoList {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseAccessPointInfoList();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.list.push(AccessPointInfo.decode(reader, reader.uint32()));
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): AccessPointInfoList {
    return { list: Array.isArray(object?.list) ? object.list.map((e: any) => AccessPointInfo.fromJSON(e)) : [] };
  },

  toJSON(message: AccessPointInfoList): unknown {
    const obj: any = {};
    if (message.list?.length) {
      obj.list = message.list.map((e) => AccessPointInfo.toJSON(e));
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<AccessPointInfoList>, I>>(base?: I): AccessPointInfoList {
    return AccessPointInfoList.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<AccessPointInfoList>, I>>(object: I): AccessPointInfoList {
    const message = createBaseAccessPointInfoList();
    message.list = object.list?.map((e) => AccessPointInfo.fromPartial(e)) || [];
    return message;
  },
};

function createBaseAccessPointInfo(): AccessPointInfo {
  return { bssid: "", ssid: "", signal: 0, security: false, connected: false, saved: false, autoConnected: false };
}

export const AccessPointInfo = {
  encode(message: AccessPointInfo, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.bssid !== "") {
      writer.uint32(10).string(message.bssid);
    }
    if (message.ssid !== "") {
      writer.uint32(18).string(message.ssid);
    }
    if (message.signal !== 0) {
      writer.uint32(24).int32(message.signal);
    }
    if (message.security === true) {
      writer.uint32(32).bool(message.security);
    }
    if (message.connected === true) {
      writer.uint32(40).bool(message.connected);
    }
    if (message.saved === true) {
      writer.uint32(48).bool(message.saved);
    }
    if (message.autoConnected === true) {
      writer.uint32(56).bool(message.autoConnected);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): AccessPointInfo {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseAccessPointInfo();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.bssid = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.ssid = reader.string();
          continue;
        case 3:
          if (tag !== 24) {
            break;
          }

          message.signal = reader.int32();
          continue;
        case 4:
          if (tag !== 32) {
            break;
          }

          message.security = reader.bool();
          continue;
        case 5:
          if (tag !== 40) {
            break;
          }

          message.connected = reader.bool();
          continue;
        case 6:
          if (tag !== 48) {
            break;
          }

          message.saved = reader.bool();
          continue;
        case 7:
          if (tag !== 56) {
            break;
          }

          message.autoConnected = reader.bool();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): AccessPointInfo {
    return {
      bssid: isSet(object.bssid) ? String(object.bssid) : "",
      ssid: isSet(object.ssid) ? String(object.ssid) : "",
      signal: isSet(object.signal) ? Number(object.signal) : 0,
      security: isSet(object.security) ? Boolean(object.security) : false,
      connected: isSet(object.connected) ? Boolean(object.connected) : false,
      saved: isSet(object.saved) ? Boolean(object.saved) : false,
      autoConnected: isSet(object.autoConnected) ? Boolean(object.autoConnected) : false,
    };
  },

  toJSON(message: AccessPointInfo): unknown {
    const obj: any = {};
    if (message.bssid !== "") {
      obj.bssid = message.bssid;
    }
    if (message.ssid !== "") {
      obj.ssid = message.ssid;
    }
    if (message.signal !== 0) {
      obj.signal = Math.round(message.signal);
    }
    if (message.security === true) {
      obj.security = message.security;
    }
    if (message.connected === true) {
      obj.connected = message.connected;
    }
    if (message.saved === true) {
      obj.saved = message.saved;
    }
    if (message.autoConnected === true) {
      obj.autoConnected = message.autoConnected;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<AccessPointInfo>, I>>(base?: I): AccessPointInfo {
    return AccessPointInfo.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<AccessPointInfo>, I>>(object: I): AccessPointInfo {
    const message = createBaseAccessPointInfo();
    message.bssid = object.bssid ?? "";
    message.ssid = object.ssid ?? "";
    message.signal = object.signal ?? 0;
    message.security = object.security ?? false;
    message.connected = object.connected ?? false;
    message.saved = object.saved ?? false;
    message.autoConnected = object.autoConnected ?? false;
    return message;
  },
};

function createBaseNetworkDeviceStatusInfoRespone(): NetworkDeviceStatusInfoRespone {
  return { hasInternet: false, WiredDevice: 0, WirelessDevice: 0 };
}

export const NetworkDeviceStatusInfoRespone = {
  encode(message: NetworkDeviceStatusInfoRespone, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.hasInternet === true) {
      writer.uint32(8).bool(message.hasInternet);
    }
    if (message.WiredDevice !== 0) {
      writer.uint32(16).int32(message.WiredDevice);
    }
    if (message.WirelessDevice !== 0) {
      writer.uint32(24).int32(message.WirelessDevice);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): NetworkDeviceStatusInfoRespone {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseNetworkDeviceStatusInfoRespone();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.hasInternet = reader.bool();
          continue;
        case 2:
          if (tag !== 16) {
            break;
          }

          message.WiredDevice = reader.int32() as any;
          continue;
        case 3:
          if (tag !== 24) {
            break;
          }

          message.WirelessDevice = reader.int32() as any;
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): NetworkDeviceStatusInfoRespone {
    return {
      hasInternet: isSet(object.hasInternet) ? Boolean(object.hasInternet) : false,
      WiredDevice: isSet(object.WiredDevice) ? networkDeviceStatusInfoRespone_StatusFromJSON(object.WiredDevice) : 0,
      WirelessDevice: isSet(object.WirelessDevice)
        ? networkDeviceStatusInfoRespone_StatusFromJSON(object.WirelessDevice)
        : 0,
    };
  },

  toJSON(message: NetworkDeviceStatusInfoRespone): unknown {
    const obj: any = {};
    if (message.hasInternet === true) {
      obj.hasInternet = message.hasInternet;
    }
    if (message.WiredDevice !== 0) {
      obj.WiredDevice = networkDeviceStatusInfoRespone_StatusToJSON(message.WiredDevice);
    }
    if (message.WirelessDevice !== 0) {
      obj.WirelessDevice = networkDeviceStatusInfoRespone_StatusToJSON(message.WirelessDevice);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<NetworkDeviceStatusInfoRespone>, I>>(base?: I): NetworkDeviceStatusInfoRespone {
    return NetworkDeviceStatusInfoRespone.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<NetworkDeviceStatusInfoRespone>, I>>(
    object: I,
  ): NetworkDeviceStatusInfoRespone {
    const message = createBaseNetworkDeviceStatusInfoRespone();
    message.hasInternet = object.hasInternet ?? false;
    message.WiredDevice = object.WiredDevice ?? 0;
    message.WirelessDevice = object.WirelessDevice ?? 0;
    return message;
  },
};

function createBaseBoxNameRegisteredRequest(): BoxNameRegisteredRequest {
  return { boxName: "" };
}

export const BoxNameRegisteredRequest = {
  encode(message: BoxNameRegisteredRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.boxName !== "") {
      writer.uint32(10).string(message.boxName);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BoxNameRegisteredRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseBoxNameRegisteredRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.boxName = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): BoxNameRegisteredRequest {
    return { boxName: isSet(object.boxName) ? String(object.boxName) : "" };
  },

  toJSON(message: BoxNameRegisteredRequest): unknown {
    const obj: any = {};
    if (message.boxName !== "") {
      obj.boxName = message.boxName;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<BoxNameRegisteredRequest>, I>>(base?: I): BoxNameRegisteredRequest {
    return BoxNameRegisteredRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<BoxNameRegisteredRequest>, I>>(object: I): BoxNameRegisteredRequest {
    const message = createBaseBoxNameRegisteredRequest();
    message.boxName = object.boxName ?? "";
    return message;
  },
};

function createBaseBoxNameRegisteredReply(): BoxNameRegisteredReply {
  return { status: 0 };
}

export const BoxNameRegisteredReply = {
  encode(message: BoxNameRegisteredReply, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.status !== 0) {
      writer.uint32(8).int32(message.status);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BoxNameRegisteredReply {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseBoxNameRegisteredReply();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.status = reader.int32() as any;
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): BoxNameRegisteredReply {
    return { status: isSet(object.status) ? boxNameRegisteredReply_StatusFromJSON(object.status) : 0 };
  },

  toJSON(message: BoxNameRegisteredReply): unknown {
    const obj: any = {};
    if (message.status !== 0) {
      obj.status = boxNameRegisteredReply_StatusToJSON(message.status);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<BoxNameRegisteredReply>, I>>(base?: I): BoxNameRegisteredReply {
    return BoxNameRegisteredReply.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<BoxNameRegisteredReply>, I>>(object: I): BoxNameRegisteredReply {
    const message = createBaseBoxNameRegisteredReply();
    message.status = object.status ?? 0;
    return message;
  },
};

function createBaseBoxSetupRequest(): BoxSetupRequest {
  return { originServer: "", boxName: "", userInfo: undefined, diskInfo: undefined };
}

export const BoxSetupRequest = {
  encode(message: BoxSetupRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.originServer !== "") {
      writer.uint32(10).string(message.originServer);
    }
    if (message.boxName !== "") {
      writer.uint32(18).string(message.boxName);
    }
    if (message.userInfo !== undefined) {
      BoxSetupRequest_UserInfo.encode(message.userInfo, writer.uint32(26).fork()).ldelim();
    }
    if (message.diskInfo !== undefined) {
      BoxSetupRequest_DiskInfo.encode(message.diskInfo, writer.uint32(34).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BoxSetupRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseBoxSetupRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.originServer = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.boxName = reader.string();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.userInfo = BoxSetupRequest_UserInfo.decode(reader, reader.uint32());
          continue;
        case 4:
          if (tag !== 34) {
            break;
          }

          message.diskInfo = BoxSetupRequest_DiskInfo.decode(reader, reader.uint32());
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): BoxSetupRequest {
    return {
      originServer: isSet(object.originServer) ? String(object.originServer) : "",
      boxName: isSet(object.boxName) ? String(object.boxName) : "",
      userInfo: isSet(object.userInfo) ? BoxSetupRequest_UserInfo.fromJSON(object.userInfo) : undefined,
      diskInfo: isSet(object.diskInfo) ? BoxSetupRequest_DiskInfo.fromJSON(object.diskInfo) : undefined,
    };
  },

  toJSON(message: BoxSetupRequest): unknown {
    const obj: any = {};
    if (message.originServer !== "") {
      obj.originServer = message.originServer;
    }
    if (message.boxName !== "") {
      obj.boxName = message.boxName;
    }
    if (message.userInfo !== undefined) {
      obj.userInfo = BoxSetupRequest_UserInfo.toJSON(message.userInfo);
    }
    if (message.diskInfo !== undefined) {
      obj.diskInfo = BoxSetupRequest_DiskInfo.toJSON(message.diskInfo);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<BoxSetupRequest>, I>>(base?: I): BoxSetupRequest {
    return BoxSetupRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<BoxSetupRequest>, I>>(object: I): BoxSetupRequest {
    const message = createBaseBoxSetupRequest();
    message.originServer = object.originServer ?? "";
    message.boxName = object.boxName ?? "";
    message.userInfo = (object.userInfo !== undefined && object.userInfo !== null)
      ? BoxSetupRequest_UserInfo.fromPartial(object.userInfo)
      : undefined;
    message.diskInfo = (object.diskInfo !== undefined && object.diskInfo !== null)
      ? BoxSetupRequest_DiskInfo.fromPartial(object.diskInfo)
      : undefined;
    return message;
  },
};

function createBaseBoxSetupRequest_UserInfo(): BoxSetupRequest_UserInfo {
  return { username: "", password: "", phonenumber: undefined };
}

export const BoxSetupRequest_UserInfo = {
  encode(message: BoxSetupRequest_UserInfo, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.username !== "") {
      writer.uint32(10).string(message.username);
    }
    if (message.password !== "") {
      writer.uint32(18).string(message.password);
    }
    if (message.phonenumber !== undefined) {
      writer.uint32(26).string(message.phonenumber);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BoxSetupRequest_UserInfo {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseBoxSetupRequest_UserInfo();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.username = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.password = reader.string();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.phonenumber = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): BoxSetupRequest_UserInfo {
    return {
      username: isSet(object.username) ? String(object.username) : "",
      password: isSet(object.password) ? String(object.password) : "",
      phonenumber: isSet(object.phonenumber) ? String(object.phonenumber) : undefined,
    };
  },

  toJSON(message: BoxSetupRequest_UserInfo): unknown {
    const obj: any = {};
    if (message.username !== "") {
      obj.username = message.username;
    }
    if (message.password !== "") {
      obj.password = message.password;
    }
    if (message.phonenumber !== undefined) {
      obj.phonenumber = message.phonenumber;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<BoxSetupRequest_UserInfo>, I>>(base?: I): BoxSetupRequest_UserInfo {
    return BoxSetupRequest_UserInfo.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<BoxSetupRequest_UserInfo>, I>>(object: I): BoxSetupRequest_UserInfo {
    const message = createBaseBoxSetupRequest_UserInfo();
    message.username = object.username ?? "";
    message.password = object.password ?? "";
    message.phonenumber = object.phonenumber ?? undefined;
    return message;
  },
};

function createBaseBoxSetupRequest_DiskInfo(): BoxSetupRequest_DiskInfo {
  return { password: "", bcache: false, dataProfile: "", metadataProfile: "" };
}

export const BoxSetupRequest_DiskInfo = {
  encode(message: BoxSetupRequest_DiskInfo, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.password !== "") {
      writer.uint32(10).string(message.password);
    }
    if (message.bcache === true) {
      writer.uint32(24).bool(message.bcache);
    }
    if (message.dataProfile !== "") {
      writer.uint32(34).string(message.dataProfile);
    }
    if (message.metadataProfile !== "") {
      writer.uint32(42).string(message.metadataProfile);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BoxSetupRequest_DiskInfo {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseBoxSetupRequest_DiskInfo();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.password = reader.string();
          continue;
        case 3:
          if (tag !== 24) {
            break;
          }

          message.bcache = reader.bool();
          continue;
        case 4:
          if (tag !== 34) {
            break;
          }

          message.dataProfile = reader.string();
          continue;
        case 5:
          if (tag !== 42) {
            break;
          }

          message.metadataProfile = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): BoxSetupRequest_DiskInfo {
    return {
      password: isSet(object.password) ? String(object.password) : "",
      bcache: isSet(object.bcache) ? Boolean(object.bcache) : false,
      dataProfile: isSet(object.dataProfile) ? String(object.dataProfile) : "",
      metadataProfile: isSet(object.metadataProfile) ? String(object.metadataProfile) : "",
    };
  },

  toJSON(message: BoxSetupRequest_DiskInfo): unknown {
    const obj: any = {};
    if (message.password !== "") {
      obj.password = message.password;
    }
    if (message.bcache === true) {
      obj.bcache = message.bcache;
    }
    if (message.dataProfile !== "") {
      obj.dataProfile = message.dataProfile;
    }
    if (message.metadataProfile !== "") {
      obj.metadataProfile = message.metadataProfile;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<BoxSetupRequest_DiskInfo>, I>>(base?: I): BoxSetupRequest_DiskInfo {
    return BoxSetupRequest_DiskInfo.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<BoxSetupRequest_DiskInfo>, I>>(object: I): BoxSetupRequest_DiskInfo {
    const message = createBaseBoxSetupRequest_DiskInfo();
    message.password = object.password ?? "";
    message.bcache = object.bcache ?? false;
    message.dataProfile = object.dataProfile ?? "";
    message.metadataProfile = object.metadataProfile ?? "";
    return message;
  },
};

function createBaseBoxSetupReply(): BoxSetupReply {
  return { status: 0, failedStatus: undefined, failedReason: undefined };
}

export const BoxSetupReply = {
  encode(message: BoxSetupReply, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.status !== 0) {
      writer.uint32(8).int32(message.status);
    }
    if (message.failedStatus !== undefined) {
      writer.uint32(16).int32(message.failedStatus);
    }
    if (message.failedReason !== undefined) {
      writer.uint32(26).string(message.failedReason);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BoxSetupReply {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseBoxSetupReply();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.status = reader.int32() as any;
          continue;
        case 2:
          if (tag !== 16) {
            break;
          }

          message.failedStatus = reader.int32() as any;
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.failedReason = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): BoxSetupReply {
    return {
      status: isSet(object.status) ? boxSetupReply_StatusFromJSON(object.status) : 0,
      failedStatus: isSet(object.failedStatus) ? boxSetupReply_FailedStatusFromJSON(object.failedStatus) : undefined,
      failedReason: isSet(object.failedReason) ? String(object.failedReason) : undefined,
    };
  },

  toJSON(message: BoxSetupReply): unknown {
    const obj: any = {};
    if (message.status !== 0) {
      obj.status = boxSetupReply_StatusToJSON(message.status);
    }
    if (message.failedStatus !== undefined) {
      obj.failedStatus = boxSetupReply_FailedStatusToJSON(message.failedStatus);
    }
    if (message.failedReason !== undefined) {
      obj.failedReason = message.failedReason;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<BoxSetupReply>, I>>(base?: I): BoxSetupReply {
    return BoxSetupReply.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<BoxSetupReply>, I>>(object: I): BoxSetupReply {
    const message = createBaseBoxSetupReply();
    message.status = object.status ?? 0;
    message.failedStatus = object.failedStatus ?? undefined;
    message.failedReason = object.failedReason ?? undefined;
    return message;
  },
};

function createBaseHasInternetResponse(): HasInternetResponse {
  return { result: false };
}

export const HasInternetResponse = {
  encode(message: HasInternetResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.result === true) {
      writer.uint32(8).bool(message.result);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): HasInternetResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseHasInternetResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.result = reader.bool();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): HasInternetResponse {
    return { result: isSet(object.result) ? Boolean(object.result) : false };
  },

  toJSON(message: HasInternetResponse): unknown {
    const obj: any = {};
    if (message.result === true) {
      obj.result = message.result;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<HasInternetResponse>, I>>(base?: I): HasInternetResponse {
    return HasInternetResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<HasInternetResponse>, I>>(object: I): HasInternetResponse {
    const message = createBaseHasInternetResponse();
    message.result = object.result ?? false;
    return message;
  },
};

function createBaseConnectivityRequest(): ConnectivityRequest {
  return { url: "" };
}

export const ConnectivityRequest = {
  encode(message: ConnectivityRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.url !== "") {
      writer.uint32(10).string(message.url);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ConnectivityRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseConnectivityRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.url = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): ConnectivityRequest {
    return { url: isSet(object.url) ? String(object.url) : "" };
  },

  toJSON(message: ConnectivityRequest): unknown {
    const obj: any = {};
    if (message.url !== "") {
      obj.url = message.url;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<ConnectivityRequest>, I>>(base?: I): ConnectivityRequest {
    return ConnectivityRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<ConnectivityRequest>, I>>(object: I): ConnectivityRequest {
    const message = createBaseConnectivityRequest();
    message.url = object.url ?? "";
    return message;
  },
};

function createBaseConnectivityReply(): ConnectivityReply {
  return { connectivity: 0 };
}

export const ConnectivityReply = {
  encode(message: ConnectivityReply, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.connectivity !== 0) {
      writer.uint32(8).int32(message.connectivity);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ConnectivityReply {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseConnectivityReply();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.connectivity = reader.int32() as any;
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): ConnectivityReply {
    return {
      connectivity: isSet(object.connectivity) ? connectivityReply_ConnectivityFromJSON(object.connectivity) : 0,
    };
  },

  toJSON(message: ConnectivityReply): unknown {
    const obj: any = {};
    if (message.connectivity !== 0) {
      obj.connectivity = connectivityReply_ConnectivityToJSON(message.connectivity);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<ConnectivityReply>, I>>(base?: I): ConnectivityReply {
    return ConnectivityReply.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<ConnectivityReply>, I>>(object: I): ConnectivityReply {
    const message = createBaseConnectivityReply();
    message.connectivity = object.connectivity ?? 0;
    return message;
  },
};

function createBaseGetConnectivityReply(): GetConnectivityReply {
  return { connectivity: 0 };
}

export const GetConnectivityReply = {
  encode(message: GetConnectivityReply, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.connectivity !== 0) {
      writer.uint32(8).int32(message.connectivity);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetConnectivityReply {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetConnectivityReply();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.connectivity = reader.int32() as any;
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): GetConnectivityReply {
    return {
      connectivity: isSet(object.connectivity) ? getConnectivityReply_ConnectivityFromJSON(object.connectivity) : 0,
    };
  },

  toJSON(message: GetConnectivityReply): unknown {
    const obj: any = {};
    if (message.connectivity !== 0) {
      obj.connectivity = getConnectivityReply_ConnectivityToJSON(message.connectivity);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<GetConnectivityReply>, I>>(base?: I): GetConnectivityReply {
    return GetConnectivityReply.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<GetConnectivityReply>, I>>(object: I): GetConnectivityReply {
    const message = createBaseGetConnectivityReply();
    message.connectivity = object.connectivity ?? 0;
    return message;
  },
};

function createBaseNmcliCallRequest(): NmcliCallRequest {
  return { args: [], uploads: [] };
}

export const NmcliCallRequest = {
  encode(message: NmcliCallRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.args) {
      writer.uint32(18).string(v!);
    }
    for (const v of message.uploads) {
      NmcliCallRequest_Upload.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): NmcliCallRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseNmcliCallRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 2:
          if (tag !== 18) {
            break;
          }

          message.args.push(reader.string());
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.uploads.push(NmcliCallRequest_Upload.decode(reader, reader.uint32()));
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): NmcliCallRequest {
    return {
      args: Array.isArray(object?.args) ? object.args.map((e: any) => String(e)) : [],
      uploads: Array.isArray(object?.uploads)
        ? object.uploads.map((e: any) => NmcliCallRequest_Upload.fromJSON(e))
        : [],
    };
  },

  toJSON(message: NmcliCallRequest): unknown {
    const obj: any = {};
    if (message.args?.length) {
      obj.args = message.args;
    }
    if (message.uploads?.length) {
      obj.uploads = message.uploads.map((e) => NmcliCallRequest_Upload.toJSON(e));
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<NmcliCallRequest>, I>>(base?: I): NmcliCallRequest {
    return NmcliCallRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<NmcliCallRequest>, I>>(object: I): NmcliCallRequest {
    const message = createBaseNmcliCallRequest();
    message.args = object.args?.map((e) => e) || [];
    message.uploads = object.uploads?.map((e) => NmcliCallRequest_Upload.fromPartial(e)) || [];
    return message;
  },
};

function createBaseNmcliCallRequest_Upload(): NmcliCallRequest_Upload {
  return { filename: "", content: new Uint8Array(0) };
}

export const NmcliCallRequest_Upload = {
  encode(message: NmcliCallRequest_Upload, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.filename !== "") {
      writer.uint32(10).string(message.filename);
    }
    if (message.content.length !== 0) {
      writer.uint32(18).bytes(message.content);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): NmcliCallRequest_Upload {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseNmcliCallRequest_Upload();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.filename = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.content = reader.bytes();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): NmcliCallRequest_Upload {
    return {
      filename: isSet(object.filename) ? String(object.filename) : "",
      content: isSet(object.content) ? bytesFromBase64(object.content) : new Uint8Array(0),
    };
  },

  toJSON(message: NmcliCallRequest_Upload): unknown {
    const obj: any = {};
    if (message.filename !== "") {
      obj.filename = message.filename;
    }
    if (message.content.length !== 0) {
      obj.content = base64FromBytes(message.content);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<NmcliCallRequest_Upload>, I>>(base?: I): NmcliCallRequest_Upload {
    return NmcliCallRequest_Upload.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<NmcliCallRequest_Upload>, I>>(object: I): NmcliCallRequest_Upload {
    const message = createBaseNmcliCallRequest_Upload();
    message.filename = object.filename ?? "";
    message.content = object.content ?? new Uint8Array(0);
    return message;
  },
};

function createBaseNmcliCallReply(): NmcliCallReply {
  return { out: "" };
}

export const NmcliCallReply = {
  encode(message: NmcliCallReply, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.out !== "") {
      writer.uint32(10).string(message.out);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): NmcliCallReply {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseNmcliCallReply();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.out = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): NmcliCallReply {
    return { out: isSet(object.out) ? String(object.out) : "" };
  },

  toJSON(message: NmcliCallReply): unknown {
    const obj: any = {};
    if (message.out !== "") {
      obj.out = message.out;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<NmcliCallReply>, I>>(base?: I): NmcliCallReply {
    return NmcliCallReply.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<NmcliCallReply>, I>>(object: I): NmcliCallReply {
    const message = createBaseNmcliCallReply();
    message.out = object.out ?? "";
    return message;
  },
};

export interface InstallerService {
  /** 初始化微服 */
  BoxNameRegistered(
    request: DeepPartial<BoxNameRegisteredRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<BoxNameRegisteredReply>;
  /** 初始化微服 */
  BoxSetup(
    request: DeepPartial<BoxSetupRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Observable<BoxSetupReply>;
  /** 临时接口，获取数据盘数量 */
  GetDataDisksCount(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<GetDataDisksCountReply>;
  /** 微服是否有互联网 */
  HasInternet(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<HasInternetResponse>;
  /** 列出内部缓存中的 Wi-Fi 列表， 如果没有则会扫描后返回 */
  WifiList(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<AccessPointInfoList>;
  /** 连接Wi-Fi */
  WifiConnect(
    request: DeepPartial<WifiConnectInfo>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<WifiConnectReply>;
  /** Scan 扫描盒子附近Wi-Fi热点信息，扫描结果在内部缓存里（阻塞，可能耗费数秒） */
  WifiScan(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty>;
  /** 当前连接的Wi-Fi */
  WifiGetConnected(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<AccessPointInfo>;
  /** 获取网络设备的状态信息 */
  NetworkDeviceStatusInfo(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<NetworkDeviceStatusInfoRespone>;
  /** Ping? Pong! */
  Ping(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty>;
  /** 微服是否初始化结束 */
  IsBoxSetupFinish(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<IsBoxSetupFinishResponse>;
  /** nmcli networking connectivity check */
  GetConnectivity(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<GetConnectivityReply>;
  /** 自己实现的，返回格式和 nmcli networking connectivity check 一样 */
  Connectivity(
    request: DeepPartial<ConnectivityRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<ConnectivityReply>;
  /** 直接调用 nmcli */
  NmcliCall(
    request: DeepPartial<NmcliCallRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<NmcliCallReply>;
}

export class InstallerServiceClientImpl implements InstallerService {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.BoxNameRegistered = this.BoxNameRegistered.bind(this);
    this.BoxSetup = this.BoxSetup.bind(this);
    this.GetDataDisksCount = this.GetDataDisksCount.bind(this);
    this.HasInternet = this.HasInternet.bind(this);
    this.WifiList = this.WifiList.bind(this);
    this.WifiConnect = this.WifiConnect.bind(this);
    this.WifiScan = this.WifiScan.bind(this);
    this.WifiGetConnected = this.WifiGetConnected.bind(this);
    this.NetworkDeviceStatusInfo = this.NetworkDeviceStatusInfo.bind(this);
    this.Ping = this.Ping.bind(this);
    this.IsBoxSetupFinish = this.IsBoxSetupFinish.bind(this);
    this.GetConnectivity = this.GetConnectivity.bind(this);
    this.Connectivity = this.Connectivity.bind(this);
    this.NmcliCall = this.NmcliCall.bind(this);
  }

  BoxNameRegistered(
    request: DeepPartial<BoxNameRegisteredRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<BoxNameRegisteredReply> {
    return this.rpc.unary(
      InstallerServiceBoxNameRegisteredDesc,
      BoxNameRegisteredRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  BoxSetup(
    request: DeepPartial<BoxSetupRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Observable<BoxSetupReply> {
    return this.rpc.invoke(InstallerServiceBoxSetupDesc, BoxSetupRequest.fromPartial(request), metadata, abortSignal);
  }

  GetDataDisksCount(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<GetDataDisksCountReply> {
    return this.rpc.unary(InstallerServiceGetDataDisksCountDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  HasInternet(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<HasInternetResponse> {
    return this.rpc.unary(InstallerServiceHasInternetDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  WifiList(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<AccessPointInfoList> {
    return this.rpc.unary(InstallerServiceWifiListDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  WifiConnect(
    request: DeepPartial<WifiConnectInfo>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<WifiConnectReply> {
    return this.rpc.unary(InstallerServiceWifiConnectDesc, WifiConnectInfo.fromPartial(request), metadata, abortSignal);
  }

  WifiScan(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty> {
    return this.rpc.unary(InstallerServiceWifiScanDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  WifiGetConnected(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<AccessPointInfo> {
    return this.rpc.unary(InstallerServiceWifiGetConnectedDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  NetworkDeviceStatusInfo(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<NetworkDeviceStatusInfoRespone> {
    return this.rpc.unary(
      InstallerServiceNetworkDeviceStatusInfoDesc,
      Empty.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  Ping(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty> {
    return this.rpc.unary(InstallerServicePingDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  IsBoxSetupFinish(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<IsBoxSetupFinishResponse> {
    return this.rpc.unary(InstallerServiceIsBoxSetupFinishDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  GetConnectivity(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<GetConnectivityReply> {
    return this.rpc.unary(InstallerServiceGetConnectivityDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  Connectivity(
    request: DeepPartial<ConnectivityRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<ConnectivityReply> {
    return this.rpc.unary(
      InstallerServiceConnectivityDesc,
      ConnectivityRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  NmcliCall(
    request: DeepPartial<NmcliCallRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<NmcliCallReply> {
    return this.rpc.unary(InstallerServiceNmcliCallDesc, NmcliCallRequest.fromPartial(request), metadata, abortSignal);
  }
}

export const InstallerServiceDesc = { serviceName: "cloud.lazycat.boxservice.installer.InstallerService" };

export const InstallerServiceBoxNameRegisteredDesc: UnaryMethodDefinitionish = {
  methodName: "BoxNameRegistered",
  service: InstallerServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return BoxNameRegisteredRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = BoxNameRegisteredReply.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const InstallerServiceBoxSetupDesc: UnaryMethodDefinitionish = {
  methodName: "BoxSetup",
  service: InstallerServiceDesc,
  requestStream: false,
  responseStream: true,
  requestType: {
    serializeBinary() {
      return BoxSetupRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = BoxSetupReply.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const InstallerServiceGetDataDisksCountDesc: UnaryMethodDefinitionish = {
  methodName: "GetDataDisksCount",
  service: InstallerServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = GetDataDisksCountReply.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const InstallerServiceHasInternetDesc: UnaryMethodDefinitionish = {
  methodName: "HasInternet",
  service: InstallerServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = HasInternetResponse.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const InstallerServiceWifiListDesc: UnaryMethodDefinitionish = {
  methodName: "WifiList",
  service: InstallerServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = AccessPointInfoList.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const InstallerServiceWifiConnectDesc: UnaryMethodDefinitionish = {
  methodName: "WifiConnect",
  service: InstallerServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return WifiConnectInfo.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = WifiConnectReply.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const InstallerServiceWifiScanDesc: UnaryMethodDefinitionish = {
  methodName: "WifiScan",
  service: InstallerServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const InstallerServiceWifiGetConnectedDesc: UnaryMethodDefinitionish = {
  methodName: "WifiGetConnected",
  service: InstallerServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = AccessPointInfo.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const InstallerServiceNetworkDeviceStatusInfoDesc: UnaryMethodDefinitionish = {
  methodName: "NetworkDeviceStatusInfo",
  service: InstallerServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = NetworkDeviceStatusInfoRespone.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const InstallerServicePingDesc: UnaryMethodDefinitionish = {
  methodName: "Ping",
  service: InstallerServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const InstallerServiceIsBoxSetupFinishDesc: UnaryMethodDefinitionish = {
  methodName: "IsBoxSetupFinish",
  service: InstallerServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = IsBoxSetupFinishResponse.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const InstallerServiceGetConnectivityDesc: UnaryMethodDefinitionish = {
  methodName: "GetConnectivity",
  service: InstallerServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = GetConnectivityReply.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const InstallerServiceConnectivityDesc: UnaryMethodDefinitionish = {
  methodName: "Connectivity",
  service: InstallerServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ConnectivityRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = ConnectivityReply.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const InstallerServiceNmcliCallDesc: UnaryMethodDefinitionish = {
  methodName: "NmcliCall",
  service: InstallerServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return NmcliCallRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = NmcliCallReply.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Promise<any>;
  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Observable<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;
    streamingTransport?: grpc.TransportFactory;
    debug?: boolean;
    metadata?: grpc.Metadata;
    upStreamRetryCodes?: number[];
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;
      streamingTransport?: grpc.TransportFactory;
      debug?: boolean;
      metadata?: grpc.Metadata;
      upStreamRetryCodes?: number[];
    },
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata = metadata && this.options.metadata
      ? new BrowserHeaders({ ...this.options?.metadata.headersMap, ...metadata?.headersMap })
      : metadata ?? this.options.metadata;
    return new Promise((resolve, reject) => {
      const client = grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata ?? {},
        ...(this.options.transport !== undefined ? { transport: this.options.transport } : {}),
        debug: this.options.debug ?? false,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message!.toObject());
          } else {
            const err = new GrpcWebError(response.statusMessage, response.status, response.trailers);
            reject(err);
          }
        },
      });

      if (abortSignal) {
        abortSignal.addEventListener("abort", () => {
          client.close();
          reject(abortSignal.reason);
        });
      }
    });
  }

  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Observable<any> {
    const upStreamCodes = this.options.upStreamRetryCodes ?? [];
    const DEFAULT_TIMEOUT_TIME: number = 3_000;
    const request = { ..._request, ...methodDesc.requestType };
    const transport = this.options.streamingTransport ?? this.options.transport;
    const maybeCombinedMetadata = metadata && this.options.metadata
      ? new BrowserHeaders({ ...this.options?.metadata.headersMap, ...metadata?.headersMap })
      : metadata ?? this.options.metadata;
    return new Observable((observer) => {
      const upStream = () => {
        const client = grpc.invoke(methodDesc, {
          host: this.host,
          request,
          ...(transport !== undefined ? { transport } : {}),
          metadata: maybeCombinedMetadata ?? {},
          debug: this.options.debug ?? false,
          onMessage: (next) => observer.next(next),
          onEnd: (code: grpc.Code, message: string, trailers: grpc.Metadata) => {
            if (code === 0) {
              observer.complete();
            } else if (upStreamCodes.includes(code)) {
              setTimeout(upStream, DEFAULT_TIMEOUT_TIME);
            } else {
              const err = new Error(message) as any;
              err.code = code;
              err.metadata = trailers;
              observer.error(err);
            }
          },
        });
        observer.add(() => {
          if (!abortSignal || !abortSignal.aborted) {
            return client.close();
          }
        });

        if (abortSignal) {
          abortSignal.addEventListener("abort", () => {
            observer.error(abortSignal.reason);
            client.close();
          });
        }
      };
      upStream();
    }).pipe(share());
  }
}

declare const self: any | undefined;
declare const window: any | undefined;
declare const global: any | undefined;
const tsProtoGlobalThis: any = (() => {
  if (typeof globalThis !== "undefined") {
    return globalThis;
  }
  if (typeof self !== "undefined") {
    return self;
  }
  if (typeof window !== "undefined") {
    return window;
  }
  if (typeof global !== "undefined") {
    return global;
  }
  throw "Unable to locate global object";
})();

function bytesFromBase64(b64: string): Uint8Array {
  if (tsProtoGlobalThis.Buffer) {
    return Uint8Array.from(tsProtoGlobalThis.Buffer.from(b64, "base64"));
  } else {
    const bin = tsProtoGlobalThis.atob(b64);
    const arr = new Uint8Array(bin.length);
    for (let i = 0; i < bin.length; ++i) {
      arr[i] = bin.charCodeAt(i);
    }
    return arr;
  }
}

function base64FromBytes(arr: Uint8Array): string {
  if (tsProtoGlobalThis.Buffer) {
    return tsProtoGlobalThis.Buffer.from(arr).toString("base64");
  } else {
    const bin: string[] = [];
    arr.forEach((byte) => {
      bin.push(String.fromCharCode(byte));
    });
    return tsProtoGlobalThis.btoa(bin.join(""));
  }
}

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new tsProtoGlobalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

export class GrpcWebError extends tsProtoGlobalThis.Error {
  constructor(message: string, public code: grpc.Code, public metadata: grpc.Metadata) {
    super(message);
  }
}
