/* eslint-disable */
import { grpc } from "@improbable-eng/grpc-web";
import { BrowserHeaders } from "browser-headers";
import _m0 from "protobufjs/minimal";

export interface UploadLogRequest {
  /** 保存日志到本地 */
  keepLogLocal: boolean;
  /** 日志上传地址 */
  uploadToUrl?: string | undefined;
}

export interface UploadLogResponse {
}

function createBaseUploadLogRequest(): UploadLogRequest {
  return { keepLogLocal: false, uploadToUrl: undefined };
}

export const UploadLogRequest = {
  encode(message: UploadLogRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.keepLogLocal === true) {
      writer.uint32(8).bool(message.keepLogLocal);
    }
    if (message.uploadToUrl !== undefined) {
      writer.uint32(18).string(message.uploadToUrl);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): UploadLogRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseUploadLogRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.keepLogLocal = reader.bool();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.uploadToUrl = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): UploadLogRequest {
    return {
      keepLogLocal: isSet(object.keepLogLocal) ? Boolean(object.keepLogLocal) : false,
      uploadToUrl: isSet(object.uploadToUrl) ? String(object.uploadToUrl) : undefined,
    };
  },

  toJSON(message: UploadLogRequest): unknown {
    const obj: any = {};
    if (message.keepLogLocal === true) {
      obj.keepLogLocal = message.keepLogLocal;
    }
    if (message.uploadToUrl !== undefined) {
      obj.uploadToUrl = message.uploadToUrl;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<UploadLogRequest>, I>>(base?: I): UploadLogRequest {
    return UploadLogRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<UploadLogRequest>, I>>(object: I): UploadLogRequest {
    const message = createBaseUploadLogRequest();
    message.keepLogLocal = object.keepLogLocal ?? false;
    message.uploadToUrl = object.uploadToUrl ?? undefined;
    return message;
  },
};

function createBaseUploadLogResponse(): UploadLogResponse {
  return {};
}

export const UploadLogResponse = {
  encode(_: UploadLogResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): UploadLogResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseUploadLogResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(_: any): UploadLogResponse {
    return {};
  },

  toJSON(_: UploadLogResponse): unknown {
    const obj: any = {};
    return obj;
  },

  create<I extends Exact<DeepPartial<UploadLogResponse>, I>>(base?: I): UploadLogResponse {
    return UploadLogResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<UploadLogResponse>, I>>(_: I): UploadLogResponse {
    const message = createBaseUploadLogResponse();
    return message;
  },
};

export interface SysDebugService {
  UploadLog(
    request: DeepPartial<UploadLogRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<UploadLogResponse>;
}

export class SysDebugServiceClientImpl implements SysDebugService {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.UploadLog = this.UploadLog.bind(this);
  }

  UploadLog(
    request: DeepPartial<UploadLogRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<UploadLogResponse> {
    return this.rpc.unary(SysDebugServiceUploadLogDesc, UploadLogRequest.fromPartial(request), metadata, abortSignal);
  }
}

export const SysDebugServiceDesc = { serviceName: "cloud.lazycat.boxservice.sysdebug.SysDebugService" };

export const SysDebugServiceUploadLogDesc: UnaryMethodDefinitionish = {
  methodName: "UploadLog",
  service: SysDebugServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return UploadLogRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = UploadLogResponse.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Promise<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;

    debug?: boolean;
    metadata?: grpc.Metadata;
    upStreamRetryCodes?: number[];
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;

      debug?: boolean;
      metadata?: grpc.Metadata;
      upStreamRetryCodes?: number[];
    },
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata = metadata && this.options.metadata
      ? new BrowserHeaders({ ...this.options?.metadata.headersMap, ...metadata?.headersMap })
      : metadata ?? this.options.metadata;
    return new Promise((resolve, reject) => {
      const client = grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata ?? {},
        ...(this.options.transport !== undefined ? { transport: this.options.transport } : {}),
        debug: this.options.debug ?? false,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message!.toObject());
          } else {
            const err = new GrpcWebError(response.statusMessage, response.status, response.trailers);
            reject(err);
          }
        },
      });

      if (abortSignal) {
        abortSignal.addEventListener("abort", () => {
          client.close();
          reject(abortSignal.reason);
        });
      }
    });
  }
}

declare const self: any | undefined;
declare const window: any | undefined;
declare const global: any | undefined;
const tsProtoGlobalThis: any = (() => {
  if (typeof globalThis !== "undefined") {
    return globalThis;
  }
  if (typeof self !== "undefined") {
    return self;
  }
  if (typeof window !== "undefined") {
    return window;
  }
  if (typeof global !== "undefined") {
    return global;
  }
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

export class GrpcWebError extends tsProtoGlobalThis.Error {
  constructor(message: string, public code: grpc.Code, public metadata: grpc.Metadata) {
    super(message);
  }
}
