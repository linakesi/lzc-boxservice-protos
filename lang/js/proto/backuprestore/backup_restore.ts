/* eslint-disable */
import { grpc } from "@improbable-eng/grpc-web";
import { BrowserHeaders } from "browser-headers";
import Long from "long";
import _m0 from "protobufjs/minimal";
import { Observable } from "rxjs";
import { share } from "rxjs/operators";
import { Empty } from "../../google/protobuf/empty";
import { Timestamp } from "../../google/protobuf/timestamp";

export enum BackupDiskStatus {
  /** Idel - 空闲中 */
  Idel = 0,
  /** Backuping - 备份中 */
  Backuping = 1,
  /** NotConfig - 未配置（用于用户格式化成懒猫备份盘，但还没有配置备份策略的情况） */
  NotConfig = 2,
  /** Error - 备份错误 */
  Error = 3,
  /** NoSpace - 没有空间 */
  NoSpace = 4,
  /** Restoring - 还原中 */
  Restoring = 5,
  /** Paused - 暂停中 */
  Paused = 6,
  UNRECOGNIZED = -1,
}

export function backupDiskStatusFromJSON(object: any): BackupDiskStatus {
  switch (object) {
    case 0:
    case "Idel":
      return BackupDiskStatus.Idel;
    case 1:
    case "Backuping":
      return BackupDiskStatus.Backuping;
    case 2:
    case "NotConfig":
      return BackupDiskStatus.NotConfig;
    case 3:
    case "Error":
      return BackupDiskStatus.Error;
    case 4:
    case "NoSpace":
      return BackupDiskStatus.NoSpace;
    case 5:
    case "Restoring":
      return BackupDiskStatus.Restoring;
    case 6:
    case "Paused":
      return BackupDiskStatus.Paused;
    case -1:
    case "UNRECOGNIZED":
    default:
      return BackupDiskStatus.UNRECOGNIZED;
  }
}

export function backupDiskStatusToJSON(object: BackupDiskStatus): string {
  switch (object) {
    case BackupDiskStatus.Idel:
      return "Idel";
    case BackupDiskStatus.Backuping:
      return "Backuping";
    case BackupDiskStatus.NotConfig:
      return "NotConfig";
    case BackupDiskStatus.Error:
      return "Error";
    case BackupDiskStatus.NoSpace:
      return "NoSpace";
    case BackupDiskStatus.Restoring:
      return "Restoring";
    case BackupDiskStatus.Paused:
      return "Paused";
    case BackupDiskStatus.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface BackupNowRequest {
  /** 备份盘的uuid */
  uuid?: string | undefined;
}

export interface BackupNowResponse {
}

export interface BackupDiskListResponse {
  disks: BackupDiskDetail[];
}

export interface BackupDiskDetail {
  /** 备份盘的uuid */
  uuid: string;
  /** 备份盘的名称 */
  name: string;
  /** 硬盘的总容量 */
  size: number;
  /** 硬盘可用空间 */
  avail: number;
  /** 最早备份的时间 */
  firstDate:
    | Date
    | undefined;
  /** 最近备份的时间 */
  latestDate:
    | Date
    | undefined;
  /** 当前备份盘的状态 */
  status: BackupDiskStatus;
  /** 距离下一次备份的时间(单位为秒) */
  nextBackupTime: number;
  /** 备份盘是否属于当前盒子（限制为一个盒子只能备份一个盒子中的数据，要不然数据会乱掉） */
  isCurrentBox: boolean;
  /** 状态信息 */
  StatusMsg: string;
}

export interface RemovableDiskListResponse {
  disks: RemovableDiskDetail[];
}

export interface RemovableDiskDetail {
  /** 硬盘的uuid */
  uuid: string;
  /** 硬盘的型号 */
  model: string;
  /** 硬盘的总容量 */
  size: number;
  /** 硬盘可用空间 */
  avail: number;
  /** 硬盘路径(在 wipefs 后， device 可能会没有 uuid) */
  path: string;
}

export interface FormatToBackupDiskRequest {
  /** 需要格式化硬盘的uuid */
  uuid: string;
  /** 指定备份盘的显示的名称 */
  name: string;
  /** 如果uuid为空，将使用 path 寻找对应的 device */
  path?: string | undefined;
}

export interface BackupRequest {
  /** 指定备份的盘 */
  uuid: string;
}

export interface BackupResponse {
  /** 备份的大小 */
  total: number;
  /** 备份的速度 */
  speed: number;
  /** 已备份的大小 */
  finished: number;
  status: BackupResponse_Status;
}

export enum BackupResponse_Status {
  /** Config - 配置完成 */
  Config = 0,
  /** Prepare - 准备中 */
  Prepare = 1,
  /** Begin - 开始备份 */
  Begin = 2,
  /** Copying - 拷贝中 */
  Copying = 3,
  /** Done - 备份完成 */
  Done = 4,
  UNRECOGNIZED = -1,
}

export function backupResponse_StatusFromJSON(object: any): BackupResponse_Status {
  switch (object) {
    case 0:
    case "Config":
      return BackupResponse_Status.Config;
    case 1:
    case "Prepare":
      return BackupResponse_Status.Prepare;
    case 2:
    case "Begin":
      return BackupResponse_Status.Begin;
    case 3:
    case "Copying":
      return BackupResponse_Status.Copying;
    case 4:
    case "Done":
      return BackupResponse_Status.Done;
    case -1:
    case "UNRECOGNIZED":
    default:
      return BackupResponse_Status.UNRECOGNIZED;
  }
}

export function backupResponse_StatusToJSON(object: BackupResponse_Status): string {
  switch (object) {
    case BackupResponse_Status.Config:
      return "Config";
    case BackupResponse_Status.Prepare:
      return "Prepare";
    case BackupResponse_Status.Begin:
      return "Begin";
    case BackupResponse_Status.Copying:
      return "Copying";
    case BackupResponse_Status.Done:
      return "Done";
    case BackupResponse_Status.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface BackupDataUsageResponse {
  osDiskOverall: BtrfsUsageOverall | undefined;
  dataDiskOverall: BtrfsUsageOverall | undefined;
}

export interface BtrfsUsageOverall {
  DeviceSize: number;
  DeviceAllocated: number;
  DeviceUnallocated: number;
  Used: number;
  FreeEstimated: number;
  FreeStatFs: number;
}

export interface BackupDataListResponse {
  users: BackupRestoreUserData[];
  apps: BackupRestoreAppData[];
  os: BackupRestoreOSData | undefined;
}

export interface BackupRestoreUserData {
  /** 用户 id */
  uid: string;
  /** 使用数据大小 */
  used: number;
}

export interface BackupRestoreAppData {
  /** 应用id */
  appId: string;
  /** 应用名称 */
  appName: string;
  /** 应用使用数据大小 */
  used: number;
}

export interface BackupRestoreOSData {
  /** 系统使用数据大小 */
  used: number;
}

export interface SavepointsRequest {
  /** 硬盘的uuid */
  uuid: string;
}

export interface SavepointsResponse {
  datas: SavepointDetail[];
}

export interface SavepointDetail {
  /** 时间点 */
  date:
    | Date
    | undefined;
  /** 用户文稿数据详情 */
  users: BackupRestoreUserData[];
  /** 应用数据 */
  apps: BackupRestoreAppData[];
  /** 系统数据 */
  os: BackupRestoreOSData | undefined;
}

export interface RestoreRequest {
  /** 硬盘的uuid */
  uuid: string;
  /** 需要还原的时间点 */
  date:
    | Date
    | undefined;
  /** 指定需要还原的部分数据，按照当前的设计，这里只需要布尔值判断即可 */
  users: boolean;
  apps: boolean;
  os: boolean;
}

export interface RestoreResponse {
  /** 总大小 */
  total: number;
  /** 速度 */
  speed: number;
  /** 已完成大小 */
  finished: number;
  /** 还原的项目详情 */
  detail:
    | RestoreResponseInfo
    | undefined;
  /** 还原所花费的时间，从开始到当前状态的间隔(单位为秒) */
  spendTime: number;
}

export interface RestoreResponseInfo {
  item: RestoreResponseInfo_Item;
  status: RestoreResponseInfo_Status;
}

/** 表示当前正在处理的项目，结合Status展示 */
export enum RestoreResponseInfo_Item {
  Lpk = 0,
  UserDocument = 1,
  App = 2,
  Lzcos = 3,
  Baseos = 4,
  UNRECOGNIZED = -1,
}

export function restoreResponseInfo_ItemFromJSON(object: any): RestoreResponseInfo_Item {
  switch (object) {
    case 0:
    case "Lpk":
      return RestoreResponseInfo_Item.Lpk;
    case 1:
    case "UserDocument":
      return RestoreResponseInfo_Item.UserDocument;
    case 2:
    case "App":
      return RestoreResponseInfo_Item.App;
    case 3:
    case "Lzcos":
      return RestoreResponseInfo_Item.Lzcos;
    case 4:
    case "Baseos":
      return RestoreResponseInfo_Item.Baseos;
    case -1:
    case "UNRECOGNIZED":
    default:
      return RestoreResponseInfo_Item.UNRECOGNIZED;
  }
}

export function restoreResponseInfo_ItemToJSON(object: RestoreResponseInfo_Item): string {
  switch (object) {
    case RestoreResponseInfo_Item.Lpk:
      return "Lpk";
    case RestoreResponseInfo_Item.UserDocument:
      return "UserDocument";
    case RestoreResponseInfo_Item.App:
      return "App";
    case RestoreResponseInfo_Item.Lzcos:
      return "Lzcos";
    case RestoreResponseInfo_Item.Baseos:
      return "Baseos";
    case RestoreResponseInfo_Item.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export enum RestoreResponseInfo_Status {
  /** Prepare - 准备中 */
  Prepare = 0,
  /** Begin - 开始还原 */
  Begin = 1,
  /** Copying - 拷贝中 */
  Copying = 2,
  /** Restoring - 还原中 */
  Restoring = 3,
  /** RestartRuntime - 重启runtime (如果没有还原系统配置，就只会重启runtime，否则将直接重启lzcos) */
  RestartRuntime = 4,
  /** Reboot - 重启系统 */
  Reboot = 5,
  UNRECOGNIZED = -1,
}

export function restoreResponseInfo_StatusFromJSON(object: any): RestoreResponseInfo_Status {
  switch (object) {
    case 0:
    case "Prepare":
      return RestoreResponseInfo_Status.Prepare;
    case 1:
    case "Begin":
      return RestoreResponseInfo_Status.Begin;
    case 2:
    case "Copying":
      return RestoreResponseInfo_Status.Copying;
    case 3:
    case "Restoring":
      return RestoreResponseInfo_Status.Restoring;
    case 4:
    case "RestartRuntime":
      return RestoreResponseInfo_Status.RestartRuntime;
    case 5:
    case "Reboot":
      return RestoreResponseInfo_Status.Reboot;
    case -1:
    case "UNRECOGNIZED":
    default:
      return RestoreResponseInfo_Status.UNRECOGNIZED;
  }
}

export function restoreResponseInfo_StatusToJSON(object: RestoreResponseInfo_Status): string {
  switch (object) {
    case RestoreResponseInfo_Status.Prepare:
      return "Prepare";
    case RestoreResponseInfo_Status.Begin:
      return "Begin";
    case RestoreResponseInfo_Status.Copying:
      return "Copying";
    case RestoreResponseInfo_Status.Restoring:
      return "Restoring";
    case RestoreResponseInfo_Status.RestartRuntime:
      return "RestartRuntime";
    case RestoreResponseInfo_Status.Reboot:
      return "Reboot";
    case RestoreResponseInfo_Status.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface EjectRequest {
  /** 硬盘的uuid */
  uuid: string;
}

function createBaseBackupNowRequest(): BackupNowRequest {
  return { uuid: undefined };
}

export const BackupNowRequest = {
  encode(message: BackupNowRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uuid !== undefined) {
      writer.uint32(10).string(message.uuid);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BackupNowRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseBackupNowRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uuid = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): BackupNowRequest {
    return { uuid: isSet(object.uuid) ? String(object.uuid) : undefined };
  },

  toJSON(message: BackupNowRequest): unknown {
    const obj: any = {};
    if (message.uuid !== undefined) {
      obj.uuid = message.uuid;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<BackupNowRequest>, I>>(base?: I): BackupNowRequest {
    return BackupNowRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<BackupNowRequest>, I>>(object: I): BackupNowRequest {
    const message = createBaseBackupNowRequest();
    message.uuid = object.uuid ?? undefined;
    return message;
  },
};

function createBaseBackupNowResponse(): BackupNowResponse {
  return {};
}

export const BackupNowResponse = {
  encode(_: BackupNowResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BackupNowResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseBackupNowResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(_: any): BackupNowResponse {
    return {};
  },

  toJSON(_: BackupNowResponse): unknown {
    const obj: any = {};
    return obj;
  },

  create<I extends Exact<DeepPartial<BackupNowResponse>, I>>(base?: I): BackupNowResponse {
    return BackupNowResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<BackupNowResponse>, I>>(_: I): BackupNowResponse {
    const message = createBaseBackupNowResponse();
    return message;
  },
};

function createBaseBackupDiskListResponse(): BackupDiskListResponse {
  return { disks: [] };
}

export const BackupDiskListResponse = {
  encode(message: BackupDiskListResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.disks) {
      BackupDiskDetail.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BackupDiskListResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseBackupDiskListResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.disks.push(BackupDiskDetail.decode(reader, reader.uint32()));
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): BackupDiskListResponse {
    return { disks: Array.isArray(object?.disks) ? object.disks.map((e: any) => BackupDiskDetail.fromJSON(e)) : [] };
  },

  toJSON(message: BackupDiskListResponse): unknown {
    const obj: any = {};
    if (message.disks?.length) {
      obj.disks = message.disks.map((e) => BackupDiskDetail.toJSON(e));
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<BackupDiskListResponse>, I>>(base?: I): BackupDiskListResponse {
    return BackupDiskListResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<BackupDiskListResponse>, I>>(object: I): BackupDiskListResponse {
    const message = createBaseBackupDiskListResponse();
    message.disks = object.disks?.map((e) => BackupDiskDetail.fromPartial(e)) || [];
    return message;
  },
};

function createBaseBackupDiskDetail(): BackupDiskDetail {
  return {
    uuid: "",
    name: "",
    size: 0,
    avail: 0,
    firstDate: undefined,
    latestDate: undefined,
    status: 0,
    nextBackupTime: 0,
    isCurrentBox: false,
    StatusMsg: "",
  };
}

export const BackupDiskDetail = {
  encode(message: BackupDiskDetail, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uuid !== "") {
      writer.uint32(10).string(message.uuid);
    }
    if (message.name !== "") {
      writer.uint32(18).string(message.name);
    }
    if (message.size !== 0) {
      writer.uint32(24).uint64(message.size);
    }
    if (message.avail !== 0) {
      writer.uint32(32).uint64(message.avail);
    }
    if (message.firstDate !== undefined) {
      Timestamp.encode(toTimestamp(message.firstDate), writer.uint32(42).fork()).ldelim();
    }
    if (message.latestDate !== undefined) {
      Timestamp.encode(toTimestamp(message.latestDate), writer.uint32(50).fork()).ldelim();
    }
    if (message.status !== 0) {
      writer.uint32(56).int32(message.status);
    }
    if (message.nextBackupTime !== 0) {
      writer.uint32(64).uint64(message.nextBackupTime);
    }
    if (message.isCurrentBox === true) {
      writer.uint32(72).bool(message.isCurrentBox);
    }
    if (message.StatusMsg !== "") {
      writer.uint32(82).string(message.StatusMsg);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BackupDiskDetail {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseBackupDiskDetail();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uuid = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.name = reader.string();
          continue;
        case 3:
          if (tag !== 24) {
            break;
          }

          message.size = longToNumber(reader.uint64() as Long);
          continue;
        case 4:
          if (tag !== 32) {
            break;
          }

          message.avail = longToNumber(reader.uint64() as Long);
          continue;
        case 5:
          if (tag !== 42) {
            break;
          }

          message.firstDate = fromTimestamp(Timestamp.decode(reader, reader.uint32()));
          continue;
        case 6:
          if (tag !== 50) {
            break;
          }

          message.latestDate = fromTimestamp(Timestamp.decode(reader, reader.uint32()));
          continue;
        case 7:
          if (tag !== 56) {
            break;
          }

          message.status = reader.int32() as any;
          continue;
        case 8:
          if (tag !== 64) {
            break;
          }

          message.nextBackupTime = longToNumber(reader.uint64() as Long);
          continue;
        case 9:
          if (tag !== 72) {
            break;
          }

          message.isCurrentBox = reader.bool();
          continue;
        case 10:
          if (tag !== 82) {
            break;
          }

          message.StatusMsg = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): BackupDiskDetail {
    return {
      uuid: isSet(object.uuid) ? String(object.uuid) : "",
      name: isSet(object.name) ? String(object.name) : "",
      size: isSet(object.size) ? Number(object.size) : 0,
      avail: isSet(object.avail) ? Number(object.avail) : 0,
      firstDate: isSet(object.firstDate) ? fromJsonTimestamp(object.firstDate) : undefined,
      latestDate: isSet(object.latestDate) ? fromJsonTimestamp(object.latestDate) : undefined,
      status: isSet(object.status) ? backupDiskStatusFromJSON(object.status) : 0,
      nextBackupTime: isSet(object.nextBackupTime) ? Number(object.nextBackupTime) : 0,
      isCurrentBox: isSet(object.isCurrentBox) ? Boolean(object.isCurrentBox) : false,
      StatusMsg: isSet(object.StatusMsg) ? String(object.StatusMsg) : "",
    };
  },

  toJSON(message: BackupDiskDetail): unknown {
    const obj: any = {};
    if (message.uuid !== "") {
      obj.uuid = message.uuid;
    }
    if (message.name !== "") {
      obj.name = message.name;
    }
    if (message.size !== 0) {
      obj.size = Math.round(message.size);
    }
    if (message.avail !== 0) {
      obj.avail = Math.round(message.avail);
    }
    if (message.firstDate !== undefined) {
      obj.firstDate = message.firstDate.toISOString();
    }
    if (message.latestDate !== undefined) {
      obj.latestDate = message.latestDate.toISOString();
    }
    if (message.status !== 0) {
      obj.status = backupDiskStatusToJSON(message.status);
    }
    if (message.nextBackupTime !== 0) {
      obj.nextBackupTime = Math.round(message.nextBackupTime);
    }
    if (message.isCurrentBox === true) {
      obj.isCurrentBox = message.isCurrentBox;
    }
    if (message.StatusMsg !== "") {
      obj.StatusMsg = message.StatusMsg;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<BackupDiskDetail>, I>>(base?: I): BackupDiskDetail {
    return BackupDiskDetail.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<BackupDiskDetail>, I>>(object: I): BackupDiskDetail {
    const message = createBaseBackupDiskDetail();
    message.uuid = object.uuid ?? "";
    message.name = object.name ?? "";
    message.size = object.size ?? 0;
    message.avail = object.avail ?? 0;
    message.firstDate = object.firstDate ?? undefined;
    message.latestDate = object.latestDate ?? undefined;
    message.status = object.status ?? 0;
    message.nextBackupTime = object.nextBackupTime ?? 0;
    message.isCurrentBox = object.isCurrentBox ?? false;
    message.StatusMsg = object.StatusMsg ?? "";
    return message;
  },
};

function createBaseRemovableDiskListResponse(): RemovableDiskListResponse {
  return { disks: [] };
}

export const RemovableDiskListResponse = {
  encode(message: RemovableDiskListResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.disks) {
      RemovableDiskDetail.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): RemovableDiskListResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseRemovableDiskListResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.disks.push(RemovableDiskDetail.decode(reader, reader.uint32()));
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): RemovableDiskListResponse {
    return { disks: Array.isArray(object?.disks) ? object.disks.map((e: any) => RemovableDiskDetail.fromJSON(e)) : [] };
  },

  toJSON(message: RemovableDiskListResponse): unknown {
    const obj: any = {};
    if (message.disks?.length) {
      obj.disks = message.disks.map((e) => RemovableDiskDetail.toJSON(e));
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<RemovableDiskListResponse>, I>>(base?: I): RemovableDiskListResponse {
    return RemovableDiskListResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<RemovableDiskListResponse>, I>>(object: I): RemovableDiskListResponse {
    const message = createBaseRemovableDiskListResponse();
    message.disks = object.disks?.map((e) => RemovableDiskDetail.fromPartial(e)) || [];
    return message;
  },
};

function createBaseRemovableDiskDetail(): RemovableDiskDetail {
  return { uuid: "", model: "", size: 0, avail: 0, path: "" };
}

export const RemovableDiskDetail = {
  encode(message: RemovableDiskDetail, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uuid !== "") {
      writer.uint32(10).string(message.uuid);
    }
    if (message.model !== "") {
      writer.uint32(18).string(message.model);
    }
    if (message.size !== 0) {
      writer.uint32(24).uint64(message.size);
    }
    if (message.avail !== 0) {
      writer.uint32(32).uint64(message.avail);
    }
    if (message.path !== "") {
      writer.uint32(42).string(message.path);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): RemovableDiskDetail {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseRemovableDiskDetail();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uuid = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.model = reader.string();
          continue;
        case 3:
          if (tag !== 24) {
            break;
          }

          message.size = longToNumber(reader.uint64() as Long);
          continue;
        case 4:
          if (tag !== 32) {
            break;
          }

          message.avail = longToNumber(reader.uint64() as Long);
          continue;
        case 5:
          if (tag !== 42) {
            break;
          }

          message.path = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): RemovableDiskDetail {
    return {
      uuid: isSet(object.uuid) ? String(object.uuid) : "",
      model: isSet(object.model) ? String(object.model) : "",
      size: isSet(object.size) ? Number(object.size) : 0,
      avail: isSet(object.avail) ? Number(object.avail) : 0,
      path: isSet(object.path) ? String(object.path) : "",
    };
  },

  toJSON(message: RemovableDiskDetail): unknown {
    const obj: any = {};
    if (message.uuid !== "") {
      obj.uuid = message.uuid;
    }
    if (message.model !== "") {
      obj.model = message.model;
    }
    if (message.size !== 0) {
      obj.size = Math.round(message.size);
    }
    if (message.avail !== 0) {
      obj.avail = Math.round(message.avail);
    }
    if (message.path !== "") {
      obj.path = message.path;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<RemovableDiskDetail>, I>>(base?: I): RemovableDiskDetail {
    return RemovableDiskDetail.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<RemovableDiskDetail>, I>>(object: I): RemovableDiskDetail {
    const message = createBaseRemovableDiskDetail();
    message.uuid = object.uuid ?? "";
    message.model = object.model ?? "";
    message.size = object.size ?? 0;
    message.avail = object.avail ?? 0;
    message.path = object.path ?? "";
    return message;
  },
};

function createBaseFormatToBackupDiskRequest(): FormatToBackupDiskRequest {
  return { uuid: "", name: "", path: undefined };
}

export const FormatToBackupDiskRequest = {
  encode(message: FormatToBackupDiskRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uuid !== "") {
      writer.uint32(10).string(message.uuid);
    }
    if (message.name !== "") {
      writer.uint32(18).string(message.name);
    }
    if (message.path !== undefined) {
      writer.uint32(26).string(message.path);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): FormatToBackupDiskRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseFormatToBackupDiskRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uuid = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.name = reader.string();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.path = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): FormatToBackupDiskRequest {
    return {
      uuid: isSet(object.uuid) ? String(object.uuid) : "",
      name: isSet(object.name) ? String(object.name) : "",
      path: isSet(object.path) ? String(object.path) : undefined,
    };
  },

  toJSON(message: FormatToBackupDiskRequest): unknown {
    const obj: any = {};
    if (message.uuid !== "") {
      obj.uuid = message.uuid;
    }
    if (message.name !== "") {
      obj.name = message.name;
    }
    if (message.path !== undefined) {
      obj.path = message.path;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<FormatToBackupDiskRequest>, I>>(base?: I): FormatToBackupDiskRequest {
    return FormatToBackupDiskRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<FormatToBackupDiskRequest>, I>>(object: I): FormatToBackupDiskRequest {
    const message = createBaseFormatToBackupDiskRequest();
    message.uuid = object.uuid ?? "";
    message.name = object.name ?? "";
    message.path = object.path ?? undefined;
    return message;
  },
};

function createBaseBackupRequest(): BackupRequest {
  return { uuid: "" };
}

export const BackupRequest = {
  encode(message: BackupRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uuid !== "") {
      writer.uint32(10).string(message.uuid);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BackupRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseBackupRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uuid = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): BackupRequest {
    return { uuid: isSet(object.uuid) ? String(object.uuid) : "" };
  },

  toJSON(message: BackupRequest): unknown {
    const obj: any = {};
    if (message.uuid !== "") {
      obj.uuid = message.uuid;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<BackupRequest>, I>>(base?: I): BackupRequest {
    return BackupRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<BackupRequest>, I>>(object: I): BackupRequest {
    const message = createBaseBackupRequest();
    message.uuid = object.uuid ?? "";
    return message;
  },
};

function createBaseBackupResponse(): BackupResponse {
  return { total: 0, speed: 0, finished: 0, status: 0 };
}

export const BackupResponse = {
  encode(message: BackupResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.total !== 0) {
      writer.uint32(8).uint64(message.total);
    }
    if (message.speed !== 0) {
      writer.uint32(16).uint64(message.speed);
    }
    if (message.finished !== 0) {
      writer.uint32(24).uint64(message.finished);
    }
    if (message.status !== 0) {
      writer.uint32(32).int32(message.status);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BackupResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseBackupResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.total = longToNumber(reader.uint64() as Long);
          continue;
        case 2:
          if (tag !== 16) {
            break;
          }

          message.speed = longToNumber(reader.uint64() as Long);
          continue;
        case 3:
          if (tag !== 24) {
            break;
          }

          message.finished = longToNumber(reader.uint64() as Long);
          continue;
        case 4:
          if (tag !== 32) {
            break;
          }

          message.status = reader.int32() as any;
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): BackupResponse {
    return {
      total: isSet(object.total) ? Number(object.total) : 0,
      speed: isSet(object.speed) ? Number(object.speed) : 0,
      finished: isSet(object.finished) ? Number(object.finished) : 0,
      status: isSet(object.status) ? backupResponse_StatusFromJSON(object.status) : 0,
    };
  },

  toJSON(message: BackupResponse): unknown {
    const obj: any = {};
    if (message.total !== 0) {
      obj.total = Math.round(message.total);
    }
    if (message.speed !== 0) {
      obj.speed = Math.round(message.speed);
    }
    if (message.finished !== 0) {
      obj.finished = Math.round(message.finished);
    }
    if (message.status !== 0) {
      obj.status = backupResponse_StatusToJSON(message.status);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<BackupResponse>, I>>(base?: I): BackupResponse {
    return BackupResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<BackupResponse>, I>>(object: I): BackupResponse {
    const message = createBaseBackupResponse();
    message.total = object.total ?? 0;
    message.speed = object.speed ?? 0;
    message.finished = object.finished ?? 0;
    message.status = object.status ?? 0;
    return message;
  },
};

function createBaseBackupDataUsageResponse(): BackupDataUsageResponse {
  return { osDiskOverall: undefined, dataDiskOverall: undefined };
}

export const BackupDataUsageResponse = {
  encode(message: BackupDataUsageResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.osDiskOverall !== undefined) {
      BtrfsUsageOverall.encode(message.osDiskOverall, writer.uint32(10).fork()).ldelim();
    }
    if (message.dataDiskOverall !== undefined) {
      BtrfsUsageOverall.encode(message.dataDiskOverall, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BackupDataUsageResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseBackupDataUsageResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.osDiskOverall = BtrfsUsageOverall.decode(reader, reader.uint32());
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.dataDiskOverall = BtrfsUsageOverall.decode(reader, reader.uint32());
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): BackupDataUsageResponse {
    return {
      osDiskOverall: isSet(object.osDiskOverall) ? BtrfsUsageOverall.fromJSON(object.osDiskOverall) : undefined,
      dataDiskOverall: isSet(object.dataDiskOverall) ? BtrfsUsageOverall.fromJSON(object.dataDiskOverall) : undefined,
    };
  },

  toJSON(message: BackupDataUsageResponse): unknown {
    const obj: any = {};
    if (message.osDiskOverall !== undefined) {
      obj.osDiskOverall = BtrfsUsageOverall.toJSON(message.osDiskOverall);
    }
    if (message.dataDiskOverall !== undefined) {
      obj.dataDiskOverall = BtrfsUsageOverall.toJSON(message.dataDiskOverall);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<BackupDataUsageResponse>, I>>(base?: I): BackupDataUsageResponse {
    return BackupDataUsageResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<BackupDataUsageResponse>, I>>(object: I): BackupDataUsageResponse {
    const message = createBaseBackupDataUsageResponse();
    message.osDiskOverall = (object.osDiskOverall !== undefined && object.osDiskOverall !== null)
      ? BtrfsUsageOverall.fromPartial(object.osDiskOverall)
      : undefined;
    message.dataDiskOverall = (object.dataDiskOverall !== undefined && object.dataDiskOverall !== null)
      ? BtrfsUsageOverall.fromPartial(object.dataDiskOverall)
      : undefined;
    return message;
  },
};

function createBaseBtrfsUsageOverall(): BtrfsUsageOverall {
  return { DeviceSize: 0, DeviceAllocated: 0, DeviceUnallocated: 0, Used: 0, FreeEstimated: 0, FreeStatFs: 0 };
}

export const BtrfsUsageOverall = {
  encode(message: BtrfsUsageOverall, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.DeviceSize !== 0) {
      writer.uint32(8).uint64(message.DeviceSize);
    }
    if (message.DeviceAllocated !== 0) {
      writer.uint32(16).uint64(message.DeviceAllocated);
    }
    if (message.DeviceUnallocated !== 0) {
      writer.uint32(24).uint64(message.DeviceUnallocated);
    }
    if (message.Used !== 0) {
      writer.uint32(32).uint64(message.Used);
    }
    if (message.FreeEstimated !== 0) {
      writer.uint32(40).uint64(message.FreeEstimated);
    }
    if (message.FreeStatFs !== 0) {
      writer.uint32(48).uint64(message.FreeStatFs);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BtrfsUsageOverall {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseBtrfsUsageOverall();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.DeviceSize = longToNumber(reader.uint64() as Long);
          continue;
        case 2:
          if (tag !== 16) {
            break;
          }

          message.DeviceAllocated = longToNumber(reader.uint64() as Long);
          continue;
        case 3:
          if (tag !== 24) {
            break;
          }

          message.DeviceUnallocated = longToNumber(reader.uint64() as Long);
          continue;
        case 4:
          if (tag !== 32) {
            break;
          }

          message.Used = longToNumber(reader.uint64() as Long);
          continue;
        case 5:
          if (tag !== 40) {
            break;
          }

          message.FreeEstimated = longToNumber(reader.uint64() as Long);
          continue;
        case 6:
          if (tag !== 48) {
            break;
          }

          message.FreeStatFs = longToNumber(reader.uint64() as Long);
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): BtrfsUsageOverall {
    return {
      DeviceSize: isSet(object.DeviceSize) ? Number(object.DeviceSize) : 0,
      DeviceAllocated: isSet(object.DeviceAllocated) ? Number(object.DeviceAllocated) : 0,
      DeviceUnallocated: isSet(object.DeviceUnallocated) ? Number(object.DeviceUnallocated) : 0,
      Used: isSet(object.Used) ? Number(object.Used) : 0,
      FreeEstimated: isSet(object.FreeEstimated) ? Number(object.FreeEstimated) : 0,
      FreeStatFs: isSet(object.FreeStatFs) ? Number(object.FreeStatFs) : 0,
    };
  },

  toJSON(message: BtrfsUsageOverall): unknown {
    const obj: any = {};
    if (message.DeviceSize !== 0) {
      obj.DeviceSize = Math.round(message.DeviceSize);
    }
    if (message.DeviceAllocated !== 0) {
      obj.DeviceAllocated = Math.round(message.DeviceAllocated);
    }
    if (message.DeviceUnallocated !== 0) {
      obj.DeviceUnallocated = Math.round(message.DeviceUnallocated);
    }
    if (message.Used !== 0) {
      obj.Used = Math.round(message.Used);
    }
    if (message.FreeEstimated !== 0) {
      obj.FreeEstimated = Math.round(message.FreeEstimated);
    }
    if (message.FreeStatFs !== 0) {
      obj.FreeStatFs = Math.round(message.FreeStatFs);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<BtrfsUsageOverall>, I>>(base?: I): BtrfsUsageOverall {
    return BtrfsUsageOverall.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<BtrfsUsageOverall>, I>>(object: I): BtrfsUsageOverall {
    const message = createBaseBtrfsUsageOverall();
    message.DeviceSize = object.DeviceSize ?? 0;
    message.DeviceAllocated = object.DeviceAllocated ?? 0;
    message.DeviceUnallocated = object.DeviceUnallocated ?? 0;
    message.Used = object.Used ?? 0;
    message.FreeEstimated = object.FreeEstimated ?? 0;
    message.FreeStatFs = object.FreeStatFs ?? 0;
    return message;
  },
};

function createBaseBackupDataListResponse(): BackupDataListResponse {
  return { users: [], apps: [], os: undefined };
}

export const BackupDataListResponse = {
  encode(message: BackupDataListResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.users) {
      BackupRestoreUserData.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.apps) {
      BackupRestoreAppData.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    if (message.os !== undefined) {
      BackupRestoreOSData.encode(message.os, writer.uint32(26).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BackupDataListResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseBackupDataListResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.users.push(BackupRestoreUserData.decode(reader, reader.uint32()));
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.apps.push(BackupRestoreAppData.decode(reader, reader.uint32()));
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.os = BackupRestoreOSData.decode(reader, reader.uint32());
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): BackupDataListResponse {
    return {
      users: Array.isArray(object?.users) ? object.users.map((e: any) => BackupRestoreUserData.fromJSON(e)) : [],
      apps: Array.isArray(object?.apps) ? object.apps.map((e: any) => BackupRestoreAppData.fromJSON(e)) : [],
      os: isSet(object.os) ? BackupRestoreOSData.fromJSON(object.os) : undefined,
    };
  },

  toJSON(message: BackupDataListResponse): unknown {
    const obj: any = {};
    if (message.users?.length) {
      obj.users = message.users.map((e) => BackupRestoreUserData.toJSON(e));
    }
    if (message.apps?.length) {
      obj.apps = message.apps.map((e) => BackupRestoreAppData.toJSON(e));
    }
    if (message.os !== undefined) {
      obj.os = BackupRestoreOSData.toJSON(message.os);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<BackupDataListResponse>, I>>(base?: I): BackupDataListResponse {
    return BackupDataListResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<BackupDataListResponse>, I>>(object: I): BackupDataListResponse {
    const message = createBaseBackupDataListResponse();
    message.users = object.users?.map((e) => BackupRestoreUserData.fromPartial(e)) || [];
    message.apps = object.apps?.map((e) => BackupRestoreAppData.fromPartial(e)) || [];
    message.os = (object.os !== undefined && object.os !== null)
      ? BackupRestoreOSData.fromPartial(object.os)
      : undefined;
    return message;
  },
};

function createBaseBackupRestoreUserData(): BackupRestoreUserData {
  return { uid: "", used: 0 };
}

export const BackupRestoreUserData = {
  encode(message: BackupRestoreUserData, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uid !== "") {
      writer.uint32(10).string(message.uid);
    }
    if (message.used !== 0) {
      writer.uint32(16).uint64(message.used);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BackupRestoreUserData {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseBackupRestoreUserData();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uid = reader.string();
          continue;
        case 2:
          if (tag !== 16) {
            break;
          }

          message.used = longToNumber(reader.uint64() as Long);
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): BackupRestoreUserData {
    return { uid: isSet(object.uid) ? String(object.uid) : "", used: isSet(object.used) ? Number(object.used) : 0 };
  },

  toJSON(message: BackupRestoreUserData): unknown {
    const obj: any = {};
    if (message.uid !== "") {
      obj.uid = message.uid;
    }
    if (message.used !== 0) {
      obj.used = Math.round(message.used);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<BackupRestoreUserData>, I>>(base?: I): BackupRestoreUserData {
    return BackupRestoreUserData.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<BackupRestoreUserData>, I>>(object: I): BackupRestoreUserData {
    const message = createBaseBackupRestoreUserData();
    message.uid = object.uid ?? "";
    message.used = object.used ?? 0;
    return message;
  },
};

function createBaseBackupRestoreAppData(): BackupRestoreAppData {
  return { appId: "", appName: "", used: 0 };
}

export const BackupRestoreAppData = {
  encode(message: BackupRestoreAppData, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.appId !== "") {
      writer.uint32(10).string(message.appId);
    }
    if (message.appName !== "") {
      writer.uint32(18).string(message.appName);
    }
    if (message.used !== 0) {
      writer.uint32(24).uint64(message.used);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BackupRestoreAppData {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseBackupRestoreAppData();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.appId = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.appName = reader.string();
          continue;
        case 3:
          if (tag !== 24) {
            break;
          }

          message.used = longToNumber(reader.uint64() as Long);
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): BackupRestoreAppData {
    return {
      appId: isSet(object.appId) ? String(object.appId) : "",
      appName: isSet(object.appName) ? String(object.appName) : "",
      used: isSet(object.used) ? Number(object.used) : 0,
    };
  },

  toJSON(message: BackupRestoreAppData): unknown {
    const obj: any = {};
    if (message.appId !== "") {
      obj.appId = message.appId;
    }
    if (message.appName !== "") {
      obj.appName = message.appName;
    }
    if (message.used !== 0) {
      obj.used = Math.round(message.used);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<BackupRestoreAppData>, I>>(base?: I): BackupRestoreAppData {
    return BackupRestoreAppData.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<BackupRestoreAppData>, I>>(object: I): BackupRestoreAppData {
    const message = createBaseBackupRestoreAppData();
    message.appId = object.appId ?? "";
    message.appName = object.appName ?? "";
    message.used = object.used ?? 0;
    return message;
  },
};

function createBaseBackupRestoreOSData(): BackupRestoreOSData {
  return { used: 0 };
}

export const BackupRestoreOSData = {
  encode(message: BackupRestoreOSData, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.used !== 0) {
      writer.uint32(8).uint64(message.used);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BackupRestoreOSData {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseBackupRestoreOSData();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.used = longToNumber(reader.uint64() as Long);
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): BackupRestoreOSData {
    return { used: isSet(object.used) ? Number(object.used) : 0 };
  },

  toJSON(message: BackupRestoreOSData): unknown {
    const obj: any = {};
    if (message.used !== 0) {
      obj.used = Math.round(message.used);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<BackupRestoreOSData>, I>>(base?: I): BackupRestoreOSData {
    return BackupRestoreOSData.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<BackupRestoreOSData>, I>>(object: I): BackupRestoreOSData {
    const message = createBaseBackupRestoreOSData();
    message.used = object.used ?? 0;
    return message;
  },
};

function createBaseSavepointsRequest(): SavepointsRequest {
  return { uuid: "" };
}

export const SavepointsRequest = {
  encode(message: SavepointsRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uuid !== "") {
      writer.uint32(10).string(message.uuid);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SavepointsRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseSavepointsRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uuid = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): SavepointsRequest {
    return { uuid: isSet(object.uuid) ? String(object.uuid) : "" };
  },

  toJSON(message: SavepointsRequest): unknown {
    const obj: any = {};
    if (message.uuid !== "") {
      obj.uuid = message.uuid;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<SavepointsRequest>, I>>(base?: I): SavepointsRequest {
    return SavepointsRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<SavepointsRequest>, I>>(object: I): SavepointsRequest {
    const message = createBaseSavepointsRequest();
    message.uuid = object.uuid ?? "";
    return message;
  },
};

function createBaseSavepointsResponse(): SavepointsResponse {
  return { datas: [] };
}

export const SavepointsResponse = {
  encode(message: SavepointsResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.datas) {
      SavepointDetail.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SavepointsResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseSavepointsResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.datas.push(SavepointDetail.decode(reader, reader.uint32()));
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): SavepointsResponse {
    return { datas: Array.isArray(object?.datas) ? object.datas.map((e: any) => SavepointDetail.fromJSON(e)) : [] };
  },

  toJSON(message: SavepointsResponse): unknown {
    const obj: any = {};
    if (message.datas?.length) {
      obj.datas = message.datas.map((e) => SavepointDetail.toJSON(e));
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<SavepointsResponse>, I>>(base?: I): SavepointsResponse {
    return SavepointsResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<SavepointsResponse>, I>>(object: I): SavepointsResponse {
    const message = createBaseSavepointsResponse();
    message.datas = object.datas?.map((e) => SavepointDetail.fromPartial(e)) || [];
    return message;
  },
};

function createBaseSavepointDetail(): SavepointDetail {
  return { date: undefined, users: [], apps: [], os: undefined };
}

export const SavepointDetail = {
  encode(message: SavepointDetail, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.date !== undefined) {
      Timestamp.encode(toTimestamp(message.date), writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.users) {
      BackupRestoreUserData.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    for (const v of message.apps) {
      BackupRestoreAppData.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    if (message.os !== undefined) {
      BackupRestoreOSData.encode(message.os, writer.uint32(34).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SavepointDetail {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseSavepointDetail();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.date = fromTimestamp(Timestamp.decode(reader, reader.uint32()));
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.users.push(BackupRestoreUserData.decode(reader, reader.uint32()));
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.apps.push(BackupRestoreAppData.decode(reader, reader.uint32()));
          continue;
        case 4:
          if (tag !== 34) {
            break;
          }

          message.os = BackupRestoreOSData.decode(reader, reader.uint32());
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): SavepointDetail {
    return {
      date: isSet(object.date) ? fromJsonTimestamp(object.date) : undefined,
      users: Array.isArray(object?.users) ? object.users.map((e: any) => BackupRestoreUserData.fromJSON(e)) : [],
      apps: Array.isArray(object?.apps) ? object.apps.map((e: any) => BackupRestoreAppData.fromJSON(e)) : [],
      os: isSet(object.os) ? BackupRestoreOSData.fromJSON(object.os) : undefined,
    };
  },

  toJSON(message: SavepointDetail): unknown {
    const obj: any = {};
    if (message.date !== undefined) {
      obj.date = message.date.toISOString();
    }
    if (message.users?.length) {
      obj.users = message.users.map((e) => BackupRestoreUserData.toJSON(e));
    }
    if (message.apps?.length) {
      obj.apps = message.apps.map((e) => BackupRestoreAppData.toJSON(e));
    }
    if (message.os !== undefined) {
      obj.os = BackupRestoreOSData.toJSON(message.os);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<SavepointDetail>, I>>(base?: I): SavepointDetail {
    return SavepointDetail.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<SavepointDetail>, I>>(object: I): SavepointDetail {
    const message = createBaseSavepointDetail();
    message.date = object.date ?? undefined;
    message.users = object.users?.map((e) => BackupRestoreUserData.fromPartial(e)) || [];
    message.apps = object.apps?.map((e) => BackupRestoreAppData.fromPartial(e)) || [];
    message.os = (object.os !== undefined && object.os !== null)
      ? BackupRestoreOSData.fromPartial(object.os)
      : undefined;
    return message;
  },
};

function createBaseRestoreRequest(): RestoreRequest {
  return { uuid: "", date: undefined, users: false, apps: false, os: false };
}

export const RestoreRequest = {
  encode(message: RestoreRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uuid !== "") {
      writer.uint32(10).string(message.uuid);
    }
    if (message.date !== undefined) {
      Timestamp.encode(toTimestamp(message.date), writer.uint32(18).fork()).ldelim();
    }
    if (message.users === true) {
      writer.uint32(24).bool(message.users);
    }
    if (message.apps === true) {
      writer.uint32(32).bool(message.apps);
    }
    if (message.os === true) {
      writer.uint32(40).bool(message.os);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): RestoreRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseRestoreRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uuid = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.date = fromTimestamp(Timestamp.decode(reader, reader.uint32()));
          continue;
        case 3:
          if (tag !== 24) {
            break;
          }

          message.users = reader.bool();
          continue;
        case 4:
          if (tag !== 32) {
            break;
          }

          message.apps = reader.bool();
          continue;
        case 5:
          if (tag !== 40) {
            break;
          }

          message.os = reader.bool();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): RestoreRequest {
    return {
      uuid: isSet(object.uuid) ? String(object.uuid) : "",
      date: isSet(object.date) ? fromJsonTimestamp(object.date) : undefined,
      users: isSet(object.users) ? Boolean(object.users) : false,
      apps: isSet(object.apps) ? Boolean(object.apps) : false,
      os: isSet(object.os) ? Boolean(object.os) : false,
    };
  },

  toJSON(message: RestoreRequest): unknown {
    const obj: any = {};
    if (message.uuid !== "") {
      obj.uuid = message.uuid;
    }
    if (message.date !== undefined) {
      obj.date = message.date.toISOString();
    }
    if (message.users === true) {
      obj.users = message.users;
    }
    if (message.apps === true) {
      obj.apps = message.apps;
    }
    if (message.os === true) {
      obj.os = message.os;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<RestoreRequest>, I>>(base?: I): RestoreRequest {
    return RestoreRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<RestoreRequest>, I>>(object: I): RestoreRequest {
    const message = createBaseRestoreRequest();
    message.uuid = object.uuid ?? "";
    message.date = object.date ?? undefined;
    message.users = object.users ?? false;
    message.apps = object.apps ?? false;
    message.os = object.os ?? false;
    return message;
  },
};

function createBaseRestoreResponse(): RestoreResponse {
  return { total: 0, speed: 0, finished: 0, detail: undefined, spendTime: 0 };
}

export const RestoreResponse = {
  encode(message: RestoreResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.total !== 0) {
      writer.uint32(8).uint64(message.total);
    }
    if (message.speed !== 0) {
      writer.uint32(16).uint64(message.speed);
    }
    if (message.finished !== 0) {
      writer.uint32(24).uint64(message.finished);
    }
    if (message.detail !== undefined) {
      RestoreResponseInfo.encode(message.detail, writer.uint32(34).fork()).ldelim();
    }
    if (message.spendTime !== 0) {
      writer.uint32(40).uint64(message.spendTime);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): RestoreResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseRestoreResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.total = longToNumber(reader.uint64() as Long);
          continue;
        case 2:
          if (tag !== 16) {
            break;
          }

          message.speed = longToNumber(reader.uint64() as Long);
          continue;
        case 3:
          if (tag !== 24) {
            break;
          }

          message.finished = longToNumber(reader.uint64() as Long);
          continue;
        case 4:
          if (tag !== 34) {
            break;
          }

          message.detail = RestoreResponseInfo.decode(reader, reader.uint32());
          continue;
        case 5:
          if (tag !== 40) {
            break;
          }

          message.spendTime = longToNumber(reader.uint64() as Long);
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): RestoreResponse {
    return {
      total: isSet(object.total) ? Number(object.total) : 0,
      speed: isSet(object.speed) ? Number(object.speed) : 0,
      finished: isSet(object.finished) ? Number(object.finished) : 0,
      detail: isSet(object.detail) ? RestoreResponseInfo.fromJSON(object.detail) : undefined,
      spendTime: isSet(object.spendTime) ? Number(object.spendTime) : 0,
    };
  },

  toJSON(message: RestoreResponse): unknown {
    const obj: any = {};
    if (message.total !== 0) {
      obj.total = Math.round(message.total);
    }
    if (message.speed !== 0) {
      obj.speed = Math.round(message.speed);
    }
    if (message.finished !== 0) {
      obj.finished = Math.round(message.finished);
    }
    if (message.detail !== undefined) {
      obj.detail = RestoreResponseInfo.toJSON(message.detail);
    }
    if (message.spendTime !== 0) {
      obj.spendTime = Math.round(message.spendTime);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<RestoreResponse>, I>>(base?: I): RestoreResponse {
    return RestoreResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<RestoreResponse>, I>>(object: I): RestoreResponse {
    const message = createBaseRestoreResponse();
    message.total = object.total ?? 0;
    message.speed = object.speed ?? 0;
    message.finished = object.finished ?? 0;
    message.detail = (object.detail !== undefined && object.detail !== null)
      ? RestoreResponseInfo.fromPartial(object.detail)
      : undefined;
    message.spendTime = object.spendTime ?? 0;
    return message;
  },
};

function createBaseRestoreResponseInfo(): RestoreResponseInfo {
  return { item: 0, status: 0 };
}

export const RestoreResponseInfo = {
  encode(message: RestoreResponseInfo, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.item !== 0) {
      writer.uint32(8).int32(message.item);
    }
    if (message.status !== 0) {
      writer.uint32(16).int32(message.status);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): RestoreResponseInfo {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseRestoreResponseInfo();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.item = reader.int32() as any;
          continue;
        case 2:
          if (tag !== 16) {
            break;
          }

          message.status = reader.int32() as any;
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): RestoreResponseInfo {
    return {
      item: isSet(object.item) ? restoreResponseInfo_ItemFromJSON(object.item) : 0,
      status: isSet(object.status) ? restoreResponseInfo_StatusFromJSON(object.status) : 0,
    };
  },

  toJSON(message: RestoreResponseInfo): unknown {
    const obj: any = {};
    if (message.item !== 0) {
      obj.item = restoreResponseInfo_ItemToJSON(message.item);
    }
    if (message.status !== 0) {
      obj.status = restoreResponseInfo_StatusToJSON(message.status);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<RestoreResponseInfo>, I>>(base?: I): RestoreResponseInfo {
    return RestoreResponseInfo.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<RestoreResponseInfo>, I>>(object: I): RestoreResponseInfo {
    const message = createBaseRestoreResponseInfo();
    message.item = object.item ?? 0;
    message.status = object.status ?? 0;
    return message;
  },
};

function createBaseEjectRequest(): EjectRequest {
  return { uuid: "" };
}

export const EjectRequest = {
  encode(message: EjectRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uuid !== "") {
      writer.uint32(10).string(message.uuid);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): EjectRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseEjectRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uuid = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): EjectRequest {
    return { uuid: isSet(object.uuid) ? String(object.uuid) : "" };
  },

  toJSON(message: EjectRequest): unknown {
    const obj: any = {};
    if (message.uuid !== "") {
      obj.uuid = message.uuid;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<EjectRequest>, I>>(base?: I): EjectRequest {
    return EjectRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<EjectRequest>, I>>(object: I): EjectRequest {
    const message = createBaseEjectRequest();
    message.uuid = object.uuid ?? "";
    return message;
  },
};

export interface BackupRestoreService {
  /** 获取当前接在盒子上的备份盘列表 */
  BackupDiskList(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<BackupDiskListResponse>;
  /** 获取当前接在盒子上所有的可插拔硬盘设备列表 */
  RemovableDiskList(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<RemovableDiskListResponse>;
  /** 格式化硬盘为懒猫微服备份盘 */
  FormatToBackupDisk(
    request: DeepPartial<FormatToBackupDiskRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<Empty>;
  /** 开始备份，如果指定的备份盘正在处于备份中，将返回当前备份的状态信息 */
  Backup(
    request: DeepPartial<BackupRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Observable<BackupResponse>;
  /** 立即备份 */
  BackupNow(
    request: DeepPartial<BackupNowRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<BackupNowResponse>;
  /** 获取需要备份的数据概览 */
  BackupDataUsage(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<BackupDataUsageResponse>;
  /** 获取支持备份的数据列表(计算各个用户和各个应用的数据大小，速度比较慢) */
  BackupDataList(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<BackupDataListResponse>;
  /** 获取指定备份盘上的所有还原点 */
  Savepoints(
    request: DeepPartial<SavepointsRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<SavepointsResponse>;
  /** 开始还原，如果指定的备份盘正处于还原中，将返回当前还原的状态信息 */
  Restore(
    request: DeepPartial<RestoreRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Observable<RestoreResponse>;
  /** 推出指定的设备 */
  Eject(request: DeepPartial<EjectRequest>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty>;
}

export class BackupRestoreServiceClientImpl implements BackupRestoreService {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.BackupDiskList = this.BackupDiskList.bind(this);
    this.RemovableDiskList = this.RemovableDiskList.bind(this);
    this.FormatToBackupDisk = this.FormatToBackupDisk.bind(this);
    this.Backup = this.Backup.bind(this);
    this.BackupNow = this.BackupNow.bind(this);
    this.BackupDataUsage = this.BackupDataUsage.bind(this);
    this.BackupDataList = this.BackupDataList.bind(this);
    this.Savepoints = this.Savepoints.bind(this);
    this.Restore = this.Restore.bind(this);
    this.Eject = this.Eject.bind(this);
  }

  BackupDiskList(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<BackupDiskListResponse> {
    return this.rpc.unary(BackupRestoreServiceBackupDiskListDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  RemovableDiskList(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<RemovableDiskListResponse> {
    return this.rpc.unary(BackupRestoreServiceRemovableDiskListDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  FormatToBackupDisk(
    request: DeepPartial<FormatToBackupDiskRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<Empty> {
    return this.rpc.unary(
      BackupRestoreServiceFormatToBackupDiskDesc,
      FormatToBackupDiskRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  Backup(
    request: DeepPartial<BackupRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Observable<BackupResponse> {
    return this.rpc.invoke(BackupRestoreServiceBackupDesc, BackupRequest.fromPartial(request), metadata, abortSignal);
  }

  BackupNow(
    request: DeepPartial<BackupNowRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<BackupNowResponse> {
    return this.rpc.unary(
      BackupRestoreServiceBackupNowDesc,
      BackupNowRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  BackupDataUsage(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<BackupDataUsageResponse> {
    return this.rpc.unary(BackupRestoreServiceBackupDataUsageDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  BackupDataList(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<BackupDataListResponse> {
    return this.rpc.unary(BackupRestoreServiceBackupDataListDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  Savepoints(
    request: DeepPartial<SavepointsRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<SavepointsResponse> {
    return this.rpc.unary(
      BackupRestoreServiceSavepointsDesc,
      SavepointsRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  Restore(
    request: DeepPartial<RestoreRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Observable<RestoreResponse> {
    return this.rpc.invoke(BackupRestoreServiceRestoreDesc, RestoreRequest.fromPartial(request), metadata, abortSignal);
  }

  Eject(request: DeepPartial<EjectRequest>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty> {
    return this.rpc.unary(BackupRestoreServiceEjectDesc, EjectRequest.fromPartial(request), metadata, abortSignal);
  }
}

export const BackupRestoreServiceDesc = { serviceName: "cloud.lazycat.boxservice.installer.BackupRestoreService" };

export const BackupRestoreServiceBackupDiskListDesc: UnaryMethodDefinitionish = {
  methodName: "BackupDiskList",
  service: BackupRestoreServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = BackupDiskListResponse.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const BackupRestoreServiceRemovableDiskListDesc: UnaryMethodDefinitionish = {
  methodName: "RemovableDiskList",
  service: BackupRestoreServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = RemovableDiskListResponse.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const BackupRestoreServiceFormatToBackupDiskDesc: UnaryMethodDefinitionish = {
  methodName: "FormatToBackupDisk",
  service: BackupRestoreServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return FormatToBackupDiskRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const BackupRestoreServiceBackupDesc: UnaryMethodDefinitionish = {
  methodName: "Backup",
  service: BackupRestoreServiceDesc,
  requestStream: false,
  responseStream: true,
  requestType: {
    serializeBinary() {
      return BackupRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = BackupResponse.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const BackupRestoreServiceBackupNowDesc: UnaryMethodDefinitionish = {
  methodName: "BackupNow",
  service: BackupRestoreServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return BackupNowRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = BackupNowResponse.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const BackupRestoreServiceBackupDataUsageDesc: UnaryMethodDefinitionish = {
  methodName: "BackupDataUsage",
  service: BackupRestoreServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = BackupDataUsageResponse.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const BackupRestoreServiceBackupDataListDesc: UnaryMethodDefinitionish = {
  methodName: "BackupDataList",
  service: BackupRestoreServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = BackupDataListResponse.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const BackupRestoreServiceSavepointsDesc: UnaryMethodDefinitionish = {
  methodName: "Savepoints",
  service: BackupRestoreServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return SavepointsRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = SavepointsResponse.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const BackupRestoreServiceRestoreDesc: UnaryMethodDefinitionish = {
  methodName: "Restore",
  service: BackupRestoreServiceDesc,
  requestStream: false,
  responseStream: true,
  requestType: {
    serializeBinary() {
      return RestoreRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = RestoreResponse.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const BackupRestoreServiceEjectDesc: UnaryMethodDefinitionish = {
  methodName: "Eject",
  service: BackupRestoreServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return EjectRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Promise<any>;
  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Observable<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;
    streamingTransport?: grpc.TransportFactory;
    debug?: boolean;
    metadata?: grpc.Metadata;
    upStreamRetryCodes?: number[];
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;
      streamingTransport?: grpc.TransportFactory;
      debug?: boolean;
      metadata?: grpc.Metadata;
      upStreamRetryCodes?: number[];
    },
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata = metadata && this.options.metadata
      ? new BrowserHeaders({ ...this.options?.metadata.headersMap, ...metadata?.headersMap })
      : metadata ?? this.options.metadata;
    return new Promise((resolve, reject) => {
      const client = grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata ?? {},
        ...(this.options.transport !== undefined ? { transport: this.options.transport } : {}),
        debug: this.options.debug ?? false,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message!.toObject());
          } else {
            const err = new GrpcWebError(response.statusMessage, response.status, response.trailers);
            reject(err);
          }
        },
      });

      if (abortSignal) {
        abortSignal.addEventListener("abort", () => {
          client.close();
          reject(abortSignal.reason);
        });
      }
    });
  }

  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Observable<any> {
    const upStreamCodes = this.options.upStreamRetryCodes ?? [];
    const DEFAULT_TIMEOUT_TIME: number = 3_000;
    const request = { ..._request, ...methodDesc.requestType };
    const transport = this.options.streamingTransport ?? this.options.transport;
    const maybeCombinedMetadata = metadata && this.options.metadata
      ? new BrowserHeaders({ ...this.options?.metadata.headersMap, ...metadata?.headersMap })
      : metadata ?? this.options.metadata;
    return new Observable((observer) => {
      const upStream = () => {
        const client = grpc.invoke(methodDesc, {
          host: this.host,
          request,
          ...(transport !== undefined ? { transport } : {}),
          metadata: maybeCombinedMetadata ?? {},
          debug: this.options.debug ?? false,
          onMessage: (next) => observer.next(next),
          onEnd: (code: grpc.Code, message: string, trailers: grpc.Metadata) => {
            if (code === 0) {
              observer.complete();
            } else if (upStreamCodes.includes(code)) {
              setTimeout(upStream, DEFAULT_TIMEOUT_TIME);
            } else {
              const err = new Error(message) as any;
              err.code = code;
              err.metadata = trailers;
              observer.error(err);
            }
          },
        });
        observer.add(() => {
          if (!abortSignal || !abortSignal.aborted) {
            return client.close();
          }
        });

        if (abortSignal) {
          abortSignal.addEventListener("abort", () => {
            observer.error(abortSignal.reason);
            client.close();
          });
        }
      };
      upStream();
    }).pipe(share());
  }
}

declare const self: any | undefined;
declare const window: any | undefined;
declare const global: any | undefined;
const tsProtoGlobalThis: any = (() => {
  if (typeof globalThis !== "undefined") {
    return globalThis;
  }
  if (typeof self !== "undefined") {
    return self;
  }
  if (typeof window !== "undefined") {
    return window;
  }
  if (typeof global !== "undefined") {
    return global;
  }
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function toTimestamp(date: Date): Timestamp {
  const seconds = date.getTime() / 1_000;
  const nanos = (date.getTime() % 1_000) * 1_000_000;
  return { seconds, nanos };
}

function fromTimestamp(t: Timestamp): Date {
  let millis = (t.seconds || 0) * 1_000;
  millis += (t.nanos || 0) / 1_000_000;
  return new Date(millis);
}

function fromJsonTimestamp(o: any): Date {
  if (o instanceof Date) {
    return o;
  } else if (typeof o === "string") {
    return new Date(o);
  } else {
    return fromTimestamp(Timestamp.fromJSON(o));
  }
}

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new tsProtoGlobalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

export class GrpcWebError extends tsProtoGlobalThis.Error {
  constructor(message: string, public code: grpc.Code, public metadata: grpc.Metadata) {
    super(message);
  }
}
