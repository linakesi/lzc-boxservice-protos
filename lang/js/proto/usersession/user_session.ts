/* eslint-disable */
import { grpc } from "@improbable-eng/grpc-web";
import { BrowserHeaders } from "browser-headers";
import _m0 from "protobufjs/minimal";
import { Timestamp } from "../../google/protobuf/timestamp";

export interface LoginRequest {
  deviceInfo: { [key: string]: string };
}

export interface LoginRequest_DeviceInfoEntry {
  key: string;
  value: string;
}

export interface LoginResponse {
  extraInfo: string;
  deviceId: string;
}

export interface AuthToken {
  type: AuthToken_TokenType;
  /** ExtraInfo的token */
  token: string;
  /**
   * 若token_type = HTTPBasicAuth
   * 则必须包含此字段，以便hportal可以分析出正确的登录设备
   * 若仅仅是为了验证帐号密码是否正确，直接调用CheckPassword接口
   */
  accessIp: string;
}

export enum AuthToken_TokenType {
  Raw = 0,
  HTTPBasicAuth = 1,
  UNRECOGNIZED = -1,
}

export function authToken_TokenTypeFromJSON(object: any): AuthToken_TokenType {
  switch (object) {
    case 0:
    case "Raw":
      return AuthToken_TokenType.Raw;
    case 1:
    case "HTTPBasicAuth":
      return AuthToken_TokenType.HTTPBasicAuth;
    case -1:
    case "UNRECOGNIZED":
    default:
      return AuthToken_TokenType.UNRECOGNIZED;
  }
}

export function authToken_TokenTypeToJSON(object: AuthToken_TokenType): string {
  switch (object) {
    case AuthToken_TokenType.Raw:
      return "Raw";
    case AuthToken_TokenType.HTTPBasicAuth:
      return "HTTPBasicAuth";
    case AuthToken_TokenType.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface LoginInfo {
  uid: string;
  deviceId: string;
  /**
   * 最近登录时间，因为针对弱网优化因此不一定等于本次登录时间
   * 只有hserver重启或调用了ClearLoginSession后，才会真属于退出状态
   */
  when:
    | Date
    | undefined;
  /** 当前登录设备的版本信息 */
  deviceVersion: string;
  /** 登录时服务器返回的token */
  token: string;
  peerId: string;
}

function createBaseLoginRequest(): LoginRequest {
  return { deviceInfo: {} };
}

export const LoginRequest = {
  encode(message: LoginRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    Object.entries(message.deviceInfo).forEach(([key, value]) => {
      LoginRequest_DeviceInfoEntry.encode({ key: key as any, value }, writer.uint32(10).fork()).ldelim();
    });
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): LoginRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseLoginRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          const entry1 = LoginRequest_DeviceInfoEntry.decode(reader, reader.uint32());
          if (entry1.value !== undefined) {
            message.deviceInfo[entry1.key] = entry1.value;
          }
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): LoginRequest {
    return {
      deviceInfo: isObject(object.deviceInfo)
        ? Object.entries(object.deviceInfo).reduce<{ [key: string]: string }>((acc, [key, value]) => {
          acc[key] = String(value);
          return acc;
        }, {})
        : {},
    };
  },

  toJSON(message: LoginRequest): unknown {
    const obj: any = {};
    if (message.deviceInfo) {
      const entries = Object.entries(message.deviceInfo);
      if (entries.length > 0) {
        obj.deviceInfo = {};
        entries.forEach(([k, v]) => {
          obj.deviceInfo[k] = v;
        });
      }
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<LoginRequest>, I>>(base?: I): LoginRequest {
    return LoginRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<LoginRequest>, I>>(object: I): LoginRequest {
    const message = createBaseLoginRequest();
    message.deviceInfo = Object.entries(object.deviceInfo ?? {}).reduce<{ [key: string]: string }>(
      (acc, [key, value]) => {
        if (value !== undefined) {
          acc[key] = String(value);
        }
        return acc;
      },
      {},
    );
    return message;
  },
};

function createBaseLoginRequest_DeviceInfoEntry(): LoginRequest_DeviceInfoEntry {
  return { key: "", value: "" };
}

export const LoginRequest_DeviceInfoEntry = {
  encode(message: LoginRequest_DeviceInfoEntry, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.key !== "") {
      writer.uint32(10).string(message.key);
    }
    if (message.value !== "") {
      writer.uint32(18).string(message.value);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): LoginRequest_DeviceInfoEntry {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseLoginRequest_DeviceInfoEntry();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.key = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.value = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): LoginRequest_DeviceInfoEntry {
    return { key: isSet(object.key) ? String(object.key) : "", value: isSet(object.value) ? String(object.value) : "" };
  },

  toJSON(message: LoginRequest_DeviceInfoEntry): unknown {
    const obj: any = {};
    if (message.key !== "") {
      obj.key = message.key;
    }
    if (message.value !== "") {
      obj.value = message.value;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<LoginRequest_DeviceInfoEntry>, I>>(base?: I): LoginRequest_DeviceInfoEntry {
    return LoginRequest_DeviceInfoEntry.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<LoginRequest_DeviceInfoEntry>, I>>(object: I): LoginRequest_DeviceInfoEntry {
    const message = createBaseLoginRequest_DeviceInfoEntry();
    message.key = object.key ?? "";
    message.value = object.value ?? "";
    return message;
  },
};

function createBaseLoginResponse(): LoginResponse {
  return { extraInfo: "", deviceId: "" };
}

export const LoginResponse = {
  encode(message: LoginResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.extraInfo !== "") {
      writer.uint32(10).string(message.extraInfo);
    }
    if (message.deviceId !== "") {
      writer.uint32(18).string(message.deviceId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): LoginResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseLoginResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.extraInfo = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.deviceId = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): LoginResponse {
    return {
      extraInfo: isSet(object.extraInfo) ? String(object.extraInfo) : "",
      deviceId: isSet(object.deviceId) ? String(object.deviceId) : "",
    };
  },

  toJSON(message: LoginResponse): unknown {
    const obj: any = {};
    if (message.extraInfo !== "") {
      obj.extraInfo = message.extraInfo;
    }
    if (message.deviceId !== "") {
      obj.deviceId = message.deviceId;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<LoginResponse>, I>>(base?: I): LoginResponse {
    return LoginResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<LoginResponse>, I>>(object: I): LoginResponse {
    const message = createBaseLoginResponse();
    message.extraInfo = object.extraInfo ?? "";
    message.deviceId = object.deviceId ?? "";
    return message;
  },
};

function createBaseAuthToken(): AuthToken {
  return { type: 0, token: "", accessIp: "" };
}

export const AuthToken = {
  encode(message: AuthToken, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.type !== 0) {
      writer.uint32(8).int32(message.type);
    }
    if (message.token !== "") {
      writer.uint32(18).string(message.token);
    }
    if (message.accessIp !== "") {
      writer.uint32(26).string(message.accessIp);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): AuthToken {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseAuthToken();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.type = reader.int32() as any;
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.token = reader.string();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.accessIp = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): AuthToken {
    return {
      type: isSet(object.type) ? authToken_TokenTypeFromJSON(object.type) : 0,
      token: isSet(object.token) ? String(object.token) : "",
      accessIp: isSet(object.accessIp) ? String(object.accessIp) : "",
    };
  },

  toJSON(message: AuthToken): unknown {
    const obj: any = {};
    if (message.type !== 0) {
      obj.type = authToken_TokenTypeToJSON(message.type);
    }
    if (message.token !== "") {
      obj.token = message.token;
    }
    if (message.accessIp !== "") {
      obj.accessIp = message.accessIp;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<AuthToken>, I>>(base?: I): AuthToken {
    return AuthToken.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<AuthToken>, I>>(object: I): AuthToken {
    const message = createBaseAuthToken();
    message.type = object.type ?? 0;
    message.token = object.token ?? "";
    message.accessIp = object.accessIp ?? "";
    return message;
  },
};

function createBaseLoginInfo(): LoginInfo {
  return { uid: "", deviceId: "", when: undefined, deviceVersion: "", token: "", peerId: "" };
}

export const LoginInfo = {
  encode(message: LoginInfo, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uid !== "") {
      writer.uint32(10).string(message.uid);
    }
    if (message.deviceId !== "") {
      writer.uint32(18).string(message.deviceId);
    }
    if (message.when !== undefined) {
      Timestamp.encode(toTimestamp(message.when), writer.uint32(26).fork()).ldelim();
    }
    if (message.deviceVersion !== "") {
      writer.uint32(34).string(message.deviceVersion);
    }
    if (message.token !== "") {
      writer.uint32(42).string(message.token);
    }
    if (message.peerId !== "") {
      writer.uint32(50).string(message.peerId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): LoginInfo {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseLoginInfo();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uid = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.deviceId = reader.string();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.when = fromTimestamp(Timestamp.decode(reader, reader.uint32()));
          continue;
        case 4:
          if (tag !== 34) {
            break;
          }

          message.deviceVersion = reader.string();
          continue;
        case 5:
          if (tag !== 42) {
            break;
          }

          message.token = reader.string();
          continue;
        case 6:
          if (tag !== 50) {
            break;
          }

          message.peerId = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): LoginInfo {
    return {
      uid: isSet(object.uid) ? String(object.uid) : "",
      deviceId: isSet(object.deviceId) ? String(object.deviceId) : "",
      when: isSet(object.when) ? fromJsonTimestamp(object.when) : undefined,
      deviceVersion: isSet(object.deviceVersion) ? String(object.deviceVersion) : "",
      token: isSet(object.token) ? String(object.token) : "",
      peerId: isSet(object.peerId) ? String(object.peerId) : "",
    };
  },

  toJSON(message: LoginInfo): unknown {
    const obj: any = {};
    if (message.uid !== "") {
      obj.uid = message.uid;
    }
    if (message.deviceId !== "") {
      obj.deviceId = message.deviceId;
    }
    if (message.when !== undefined) {
      obj.when = message.when.toISOString();
    }
    if (message.deviceVersion !== "") {
      obj.deviceVersion = message.deviceVersion;
    }
    if (message.token !== "") {
      obj.token = message.token;
    }
    if (message.peerId !== "") {
      obj.peerId = message.peerId;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<LoginInfo>, I>>(base?: I): LoginInfo {
    return LoginInfo.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<LoginInfo>, I>>(object: I): LoginInfo {
    const message = createBaseLoginInfo();
    message.uid = object.uid ?? "";
    message.deviceId = object.deviceId ?? "";
    message.when = object.when ?? undefined;
    message.deviceVersion = object.deviceVersion ?? "";
    message.token = object.token ?? "";
    message.peerId = object.peerId ?? "";
    return message;
  },
};

export interface UserSession {
  Login(
    request: DeepPartial<LoginRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<LoginResponse>;
  QueryLogin(request: DeepPartial<AuthToken>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<LoginInfo>;
}

export class UserSessionClientImpl implements UserSession {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.Login = this.Login.bind(this);
    this.QueryLogin = this.QueryLogin.bind(this);
  }

  Login(
    request: DeepPartial<LoginRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<LoginResponse> {
    return this.rpc.unary(UserSessionLoginDesc, LoginRequest.fromPartial(request), metadata, abortSignal);
  }

  QueryLogin(request: DeepPartial<AuthToken>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<LoginInfo> {
    return this.rpc.unary(UserSessionQueryLoginDesc, AuthToken.fromPartial(request), metadata, abortSignal);
  }
}

export const UserSessionDesc = { serviceName: "cloud.lazycat.boxservice.usersession.UserSession" };

export const UserSessionLoginDesc: UnaryMethodDefinitionish = {
  methodName: "Login",
  service: UserSessionDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return LoginRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = LoginResponse.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const UserSessionQueryLoginDesc: UnaryMethodDefinitionish = {
  methodName: "QueryLogin",
  service: UserSessionDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return AuthToken.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = LoginInfo.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Promise<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;

    debug?: boolean;
    metadata?: grpc.Metadata;
    upStreamRetryCodes?: number[];
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;

      debug?: boolean;
      metadata?: grpc.Metadata;
      upStreamRetryCodes?: number[];
    },
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata = metadata && this.options.metadata
      ? new BrowserHeaders({ ...this.options?.metadata.headersMap, ...metadata?.headersMap })
      : metadata ?? this.options.metadata;
    return new Promise((resolve, reject) => {
      const client = grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata ?? {},
        ...(this.options.transport !== undefined ? { transport: this.options.transport } : {}),
        debug: this.options.debug ?? false,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message!.toObject());
          } else {
            const err = new GrpcWebError(response.statusMessage, response.status, response.trailers);
            reject(err);
          }
        },
      });

      if (abortSignal) {
        abortSignal.addEventListener("abort", () => {
          client.close();
          reject(abortSignal.reason);
        });
      }
    });
  }
}

declare const self: any | undefined;
declare const window: any | undefined;
declare const global: any | undefined;
const tsProtoGlobalThis: any = (() => {
  if (typeof globalThis !== "undefined") {
    return globalThis;
  }
  if (typeof self !== "undefined") {
    return self;
  }
  if (typeof window !== "undefined") {
    return window;
  }
  if (typeof global !== "undefined") {
    return global;
  }
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function toTimestamp(date: Date): Timestamp {
  const seconds = date.getTime() / 1_000;
  const nanos = (date.getTime() % 1_000) * 1_000_000;
  return { seconds, nanos };
}

function fromTimestamp(t: Timestamp): Date {
  let millis = (t.seconds || 0) * 1_000;
  millis += (t.nanos || 0) / 1_000_000;
  return new Date(millis);
}

function fromJsonTimestamp(o: any): Date {
  if (o instanceof Date) {
    return o;
  } else if (typeof o === "string") {
    return new Date(o);
  } else {
    return fromTimestamp(Timestamp.fromJSON(o));
  }
}

function isObject(value: any): boolean {
  return typeof value === "object" && value !== null;
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

export class GrpcWebError extends tsProtoGlobalThis.Error {
  constructor(message: string, public code: grpc.Code, public metadata: grpc.Metadata) {
    super(message);
  }
}
