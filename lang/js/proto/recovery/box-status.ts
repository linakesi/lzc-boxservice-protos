/* eslint-disable */
import { grpc } from "@improbable-eng/grpc-web";
import { BrowserHeaders } from "browser-headers";
import Long from "long";
import _m0 from "protobufjs/minimal";
import { Observable } from "rxjs";
import { share } from "rxjs/operators";
import { Empty } from "../../google/protobuf/empty";

export interface GetDataDisksCountReply {
  count: number;
}

export interface DataDecryptResult {
  /** 如果失败，则代表密码错误。其它错误属于 bug，会抛出异常 */
  success: boolean;
}

export interface DataChangePasswordResult {
  /** 如果失败，则代表密码错误。其它错误属于 bug，会抛出异常 */
  success: boolean;
}

export interface DataListEncryptedResult {
  disks: string[];
}

export interface DataDiskFormat {
  /** 用于数据盘的磁盘（例如 /dev/vdb。如果留空，则自动扫描并使用所有的空盘） */
  disks: string[];
  /** 数据盘的密码（如果留空，则不加密） */
  password: string;
  /** 是否使用 bcache */
  bcache: boolean;
  /**
   * 磁盘阵列 raid 相关
   * btrfs 对此选项的标准全称叫作 "block group profile"
   * 不主动设置，即使用 btrfs 默认，当前默认是 data=single, metadata=dup
   * 我厌倦了搞一堆 enum 然后 string 和 enum 转换来转换去，还不如直接进 string 呢
   */
  dataProfile: string;
  metadataProfile: string;
}

export interface DataDiskDecrypt {
  /** 加密的密码，如果留空，则表示不加密 */
  password: string;
}

export interface DataDiskChangePassword {
  /** 需要修改的磁盘（例如 /dev/vdb。如果留空，则自动扫描并使用所有的数据盘） */
  disks: string[];
  oldPassword: string;
  newPassword: string;
}

export interface BoxSystemStatus {
  status: BoxSystemStatus_SysStatus;
  serviceUrl: string;
  exceptionReason?: BoxSystemStatus_ExceptionReason | undefined;
}

export enum BoxSystemStatus_SysStatus {
  /** Booting - 系统正在启动中 */
  Booting = 0,
  /** Normal - 处于正常可用状态 */
  Normal = 1,
  /** Exception - 处于异常状态，具体原因可读取ExceptionReason */
  Exception = 2,
  /** Upgrading2 - 微服升级中，数秒后会自动重启，在该状态下，用户不应该使用盒子，以免导致数据丢失 */
  Upgrading2 = 3,
  /** Reserved - 保留 */
  Reserved = 4,
  UNRECOGNIZED = -1,
}

export function boxSystemStatus_SysStatusFromJSON(object: any): BoxSystemStatus_SysStatus {
  switch (object) {
    case 0:
    case "Booting":
      return BoxSystemStatus_SysStatus.Booting;
    case 1:
    case "Normal":
      return BoxSystemStatus_SysStatus.Normal;
    case 2:
    case "Exception":
      return BoxSystemStatus_SysStatus.Exception;
    case 3:
    case "Upgrading2":
      return BoxSystemStatus_SysStatus.Upgrading2;
    case 4:
    case "Reserved":
      return BoxSystemStatus_SysStatus.Reserved;
    case -1:
    case "UNRECOGNIZED":
    default:
      return BoxSystemStatus_SysStatus.UNRECOGNIZED;
  }
}

export function boxSystemStatus_SysStatusToJSON(object: BoxSystemStatus_SysStatus): string {
  switch (object) {
    case BoxSystemStatus_SysStatus.Booting:
      return "Booting";
    case BoxSystemStatus_SysStatus.Normal:
      return "Normal";
    case BoxSystemStatus_SysStatus.Exception:
      return "Exception";
    case BoxSystemStatus_SysStatus.Upgrading2:
      return "Upgrading2";
    case BoxSystemStatus_SysStatus.Reserved:
      return "Reserved";
    case BoxSystemStatus_SysStatus.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export enum BoxSystemStatus_ExceptionReason {
  /** Unknown - 其他原因 */
  Unknown = 0,
  /** WaitDataDiskDecryption - 等待加密数据盘被解密 */
  WaitDataDiskDecryption = 1,
  /** DataNotInit - 数据盘没有初始化 */
  DataNotInit = 2,
  /**
   * DataException - 1. 数据盘不存在（数据盘未插，或内部的 box.data.path 配置不正确）
   * 2. 数据盘里已有分区（非 btrfs）
   * 3. 数据盘无法挂载（文件系统损坏等）
   * 4. 其它未意料的数据盘相关异常
   */
  DataException = 3,
  /**
   * Upgrading - 微服升级中，数秒后会自动重启，在该状态下，用户不应该使用盒子，以免导致数据丢失
   * deprecated 请使用SysStatus的Upgrading2
   */
  Upgrading = 4,
  UNRECOGNIZED = -1,
}

export function boxSystemStatus_ExceptionReasonFromJSON(object: any): BoxSystemStatus_ExceptionReason {
  switch (object) {
    case 0:
    case "Unknown":
      return BoxSystemStatus_ExceptionReason.Unknown;
    case 1:
    case "WaitDataDiskDecryption":
      return BoxSystemStatus_ExceptionReason.WaitDataDiskDecryption;
    case 2:
    case "DataNotInit":
      return BoxSystemStatus_ExceptionReason.DataNotInit;
    case 3:
    case "DataException":
      return BoxSystemStatus_ExceptionReason.DataException;
    case 4:
    case "Upgrading":
      return BoxSystemStatus_ExceptionReason.Upgrading;
    case -1:
    case "UNRECOGNIZED":
    default:
      return BoxSystemStatus_ExceptionReason.UNRECOGNIZED;
  }
}

export function boxSystemStatus_ExceptionReasonToJSON(object: BoxSystemStatus_ExceptionReason): string {
  switch (object) {
    case BoxSystemStatus_ExceptionReason.Unknown:
      return "Unknown";
    case BoxSystemStatus_ExceptionReason.WaitDataDiskDecryption:
      return "WaitDataDiskDecryption";
    case BoxSystemStatus_ExceptionReason.DataNotInit:
      return "DataNotInit";
    case BoxSystemStatus_ExceptionReason.DataException:
      return "DataException";
    case BoxSystemStatus_ExceptionReason.Upgrading:
      return "Upgrading";
    case BoxSystemStatus_ExceptionReason.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

function createBaseGetDataDisksCountReply(): GetDataDisksCountReply {
  return { count: 0 };
}

export const GetDataDisksCountReply = {
  encode(message: GetDataDisksCountReply, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.count !== 0) {
      writer.uint32(8).int64(message.count);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetDataDisksCountReply {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGetDataDisksCountReply();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.count = longToNumber(reader.int64() as Long);
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): GetDataDisksCountReply {
    return { count: isSet(object.count) ? Number(object.count) : 0 };
  },

  toJSON(message: GetDataDisksCountReply): unknown {
    const obj: any = {};
    if (message.count !== 0) {
      obj.count = Math.round(message.count);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<GetDataDisksCountReply>, I>>(base?: I): GetDataDisksCountReply {
    return GetDataDisksCountReply.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<GetDataDisksCountReply>, I>>(object: I): GetDataDisksCountReply {
    const message = createBaseGetDataDisksCountReply();
    message.count = object.count ?? 0;
    return message;
  },
};

function createBaseDataDecryptResult(): DataDecryptResult {
  return { success: false };
}

export const DataDecryptResult = {
  encode(message: DataDecryptResult, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.success === true) {
      writer.uint32(8).bool(message.success);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): DataDecryptResult {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseDataDecryptResult();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.success = reader.bool();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): DataDecryptResult {
    return { success: isSet(object.success) ? Boolean(object.success) : false };
  },

  toJSON(message: DataDecryptResult): unknown {
    const obj: any = {};
    if (message.success === true) {
      obj.success = message.success;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<DataDecryptResult>, I>>(base?: I): DataDecryptResult {
    return DataDecryptResult.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<DataDecryptResult>, I>>(object: I): DataDecryptResult {
    const message = createBaseDataDecryptResult();
    message.success = object.success ?? false;
    return message;
  },
};

function createBaseDataChangePasswordResult(): DataChangePasswordResult {
  return { success: false };
}

export const DataChangePasswordResult = {
  encode(message: DataChangePasswordResult, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.success === true) {
      writer.uint32(8).bool(message.success);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): DataChangePasswordResult {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseDataChangePasswordResult();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.success = reader.bool();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): DataChangePasswordResult {
    return { success: isSet(object.success) ? Boolean(object.success) : false };
  },

  toJSON(message: DataChangePasswordResult): unknown {
    const obj: any = {};
    if (message.success === true) {
      obj.success = message.success;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<DataChangePasswordResult>, I>>(base?: I): DataChangePasswordResult {
    return DataChangePasswordResult.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<DataChangePasswordResult>, I>>(object: I): DataChangePasswordResult {
    const message = createBaseDataChangePasswordResult();
    message.success = object.success ?? false;
    return message;
  },
};

function createBaseDataListEncryptedResult(): DataListEncryptedResult {
  return { disks: [] };
}

export const DataListEncryptedResult = {
  encode(message: DataListEncryptedResult, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.disks) {
      writer.uint32(10).string(v!);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): DataListEncryptedResult {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseDataListEncryptedResult();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.disks.push(reader.string());
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): DataListEncryptedResult {
    return { disks: Array.isArray(object?.disks) ? object.disks.map((e: any) => String(e)) : [] };
  },

  toJSON(message: DataListEncryptedResult): unknown {
    const obj: any = {};
    if (message.disks?.length) {
      obj.disks = message.disks;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<DataListEncryptedResult>, I>>(base?: I): DataListEncryptedResult {
    return DataListEncryptedResult.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<DataListEncryptedResult>, I>>(object: I): DataListEncryptedResult {
    const message = createBaseDataListEncryptedResult();
    message.disks = object.disks?.map((e) => e) || [];
    return message;
  },
};

function createBaseDataDiskFormat(): DataDiskFormat {
  return { disks: [], password: "", bcache: false, dataProfile: "", metadataProfile: "" };
}

export const DataDiskFormat = {
  encode(message: DataDiskFormat, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.disks) {
      writer.uint32(10).string(v!);
    }
    if (message.password !== "") {
      writer.uint32(18).string(message.password);
    }
    if (message.bcache === true) {
      writer.uint32(24).bool(message.bcache);
    }
    if (message.dataProfile !== "") {
      writer.uint32(34).string(message.dataProfile);
    }
    if (message.metadataProfile !== "") {
      writer.uint32(42).string(message.metadataProfile);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): DataDiskFormat {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseDataDiskFormat();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.disks.push(reader.string());
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.password = reader.string();
          continue;
        case 3:
          if (tag !== 24) {
            break;
          }

          message.bcache = reader.bool();
          continue;
        case 4:
          if (tag !== 34) {
            break;
          }

          message.dataProfile = reader.string();
          continue;
        case 5:
          if (tag !== 42) {
            break;
          }

          message.metadataProfile = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): DataDiskFormat {
    return {
      disks: Array.isArray(object?.disks) ? object.disks.map((e: any) => String(e)) : [],
      password: isSet(object.password) ? String(object.password) : "",
      bcache: isSet(object.bcache) ? Boolean(object.bcache) : false,
      dataProfile: isSet(object.dataProfile) ? String(object.dataProfile) : "",
      metadataProfile: isSet(object.metadataProfile) ? String(object.metadataProfile) : "",
    };
  },

  toJSON(message: DataDiskFormat): unknown {
    const obj: any = {};
    if (message.disks?.length) {
      obj.disks = message.disks;
    }
    if (message.password !== "") {
      obj.password = message.password;
    }
    if (message.bcache === true) {
      obj.bcache = message.bcache;
    }
    if (message.dataProfile !== "") {
      obj.dataProfile = message.dataProfile;
    }
    if (message.metadataProfile !== "") {
      obj.metadataProfile = message.metadataProfile;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<DataDiskFormat>, I>>(base?: I): DataDiskFormat {
    return DataDiskFormat.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<DataDiskFormat>, I>>(object: I): DataDiskFormat {
    const message = createBaseDataDiskFormat();
    message.disks = object.disks?.map((e) => e) || [];
    message.password = object.password ?? "";
    message.bcache = object.bcache ?? false;
    message.dataProfile = object.dataProfile ?? "";
    message.metadataProfile = object.metadataProfile ?? "";
    return message;
  },
};

function createBaseDataDiskDecrypt(): DataDiskDecrypt {
  return { password: "" };
}

export const DataDiskDecrypt = {
  encode(message: DataDiskDecrypt, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.password !== "") {
      writer.uint32(10).string(message.password);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): DataDiskDecrypt {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseDataDiskDecrypt();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.password = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): DataDiskDecrypt {
    return { password: isSet(object.password) ? String(object.password) : "" };
  },

  toJSON(message: DataDiskDecrypt): unknown {
    const obj: any = {};
    if (message.password !== "") {
      obj.password = message.password;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<DataDiskDecrypt>, I>>(base?: I): DataDiskDecrypt {
    return DataDiskDecrypt.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<DataDiskDecrypt>, I>>(object: I): DataDiskDecrypt {
    const message = createBaseDataDiskDecrypt();
    message.password = object.password ?? "";
    return message;
  },
};

function createBaseDataDiskChangePassword(): DataDiskChangePassword {
  return { disks: [], oldPassword: "", newPassword: "" };
}

export const DataDiskChangePassword = {
  encode(message: DataDiskChangePassword, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.disks) {
      writer.uint32(26).string(v!);
    }
    if (message.oldPassword !== "") {
      writer.uint32(10).string(message.oldPassword);
    }
    if (message.newPassword !== "") {
      writer.uint32(18).string(message.newPassword);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): DataDiskChangePassword {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseDataDiskChangePassword();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 3:
          if (tag !== 26) {
            break;
          }

          message.disks.push(reader.string());
          continue;
        case 1:
          if (tag !== 10) {
            break;
          }

          message.oldPassword = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.newPassword = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): DataDiskChangePassword {
    return {
      disks: Array.isArray(object?.disks) ? object.disks.map((e: any) => String(e)) : [],
      oldPassword: isSet(object.oldPassword) ? String(object.oldPassword) : "",
      newPassword: isSet(object.newPassword) ? String(object.newPassword) : "",
    };
  },

  toJSON(message: DataDiskChangePassword): unknown {
    const obj: any = {};
    if (message.disks?.length) {
      obj.disks = message.disks;
    }
    if (message.oldPassword !== "") {
      obj.oldPassword = message.oldPassword;
    }
    if (message.newPassword !== "") {
      obj.newPassword = message.newPassword;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<DataDiskChangePassword>, I>>(base?: I): DataDiskChangePassword {
    return DataDiskChangePassword.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<DataDiskChangePassword>, I>>(object: I): DataDiskChangePassword {
    const message = createBaseDataDiskChangePassword();
    message.disks = object.disks?.map((e) => e) || [];
    message.oldPassword = object.oldPassword ?? "";
    message.newPassword = object.newPassword ?? "";
    return message;
  },
};

function createBaseBoxSystemStatus(): BoxSystemStatus {
  return { status: 0, serviceUrl: "", exceptionReason: undefined };
}

export const BoxSystemStatus = {
  encode(message: BoxSystemStatus, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.status !== 0) {
      writer.uint32(8).int32(message.status);
    }
    if (message.serviceUrl !== "") {
      writer.uint32(18).string(message.serviceUrl);
    }
    if (message.exceptionReason !== undefined) {
      writer.uint32(24).int32(message.exceptionReason);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BoxSystemStatus {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseBoxSystemStatus();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.status = reader.int32() as any;
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.serviceUrl = reader.string();
          continue;
        case 3:
          if (tag !== 24) {
            break;
          }

          message.exceptionReason = reader.int32() as any;
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): BoxSystemStatus {
    return {
      status: isSet(object.status) ? boxSystemStatus_SysStatusFromJSON(object.status) : 0,
      serviceUrl: isSet(object.serviceUrl) ? String(object.serviceUrl) : "",
      exceptionReason: isSet(object.exceptionReason)
        ? boxSystemStatus_ExceptionReasonFromJSON(object.exceptionReason)
        : undefined,
    };
  },

  toJSON(message: BoxSystemStatus): unknown {
    const obj: any = {};
    if (message.status !== 0) {
      obj.status = boxSystemStatus_SysStatusToJSON(message.status);
    }
    if (message.serviceUrl !== "") {
      obj.serviceUrl = message.serviceUrl;
    }
    if (message.exceptionReason !== undefined) {
      obj.exceptionReason = boxSystemStatus_ExceptionReasonToJSON(message.exceptionReason);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<BoxSystemStatus>, I>>(base?: I): BoxSystemStatus {
    return BoxSystemStatus.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<BoxSystemStatus>, I>>(object: I): BoxSystemStatus {
    const message = createBaseBoxSystemStatus();
    message.status = object.status ?? 0;
    message.serviceUrl = object.serviceUrl ?? "";
    message.exceptionReason = object.exceptionReason ?? undefined;
    return message;
  },
};

export interface BoxStatusService {
  /** 格式化（包含可选的设密码） */
  DataFormat(request: DeepPartial<DataDiskFormat>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty>;
  /** 解密（无密码时不必调用） */
  DataDecrypt(
    request: DeepPartial<DataDiskDecrypt>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<DataDecryptResult>;
  /** 修改密码 */
  DataChangePassword(
    request: DeepPartial<DataDiskChangePassword>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<DataChangePasswordResult>;
  /** 获得加密的磁盘列表 */
  DataListEncrypted(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<DataListEncryptedResult>;
  /** 临时接口，获取数据盘数量 */
  GetDataDisksCount(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<GetDataDisksCountReply>;
  /**
   * lzc-docker的数据可能因为断电、重启等原因损坏，且无法自动恢复。
   * 遇到此问题只能删除全部的docker数据，提供此接口在出现问题时，由客服引导进入调试界面调用
   */
  ClearLZCDockerData(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty>;
  /**
   * TODO: 在bridge.basic不再等待数据盘启动后可以迁移走此服务
   *
   * @deprecated
   */
  LatestSysStatus(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Observable<BoxSystemStatus>;
  /**
   * 不要调用!!!!!!!!!!!!! 请直接使用shell-api去获取相关状态
   *
   * @deprecated
   */
  GetStatus(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<BoxSystemStatus>;
}

export class BoxStatusServiceClientImpl implements BoxStatusService {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.DataFormat = this.DataFormat.bind(this);
    this.DataDecrypt = this.DataDecrypt.bind(this);
    this.DataChangePassword = this.DataChangePassword.bind(this);
    this.DataListEncrypted = this.DataListEncrypted.bind(this);
    this.GetDataDisksCount = this.GetDataDisksCount.bind(this);
    this.ClearLZCDockerData = this.ClearLZCDockerData.bind(this);
    this.LatestSysStatus = this.LatestSysStatus.bind(this);
    this.GetStatus = this.GetStatus.bind(this);
  }

  DataFormat(
    request: DeepPartial<DataDiskFormat>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<Empty> {
    return this.rpc.unary(BoxStatusServiceDataFormatDesc, DataDiskFormat.fromPartial(request), metadata, abortSignal);
  }

  DataDecrypt(
    request: DeepPartial<DataDiskDecrypt>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<DataDecryptResult> {
    return this.rpc.unary(BoxStatusServiceDataDecryptDesc, DataDiskDecrypt.fromPartial(request), metadata, abortSignal);
  }

  DataChangePassword(
    request: DeepPartial<DataDiskChangePassword>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<DataChangePasswordResult> {
    return this.rpc.unary(
      BoxStatusServiceDataChangePasswordDesc,
      DataDiskChangePassword.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  DataListEncrypted(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<DataListEncryptedResult> {
    return this.rpc.unary(BoxStatusServiceDataListEncryptedDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  GetDataDisksCount(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<GetDataDisksCountReply> {
    return this.rpc.unary(BoxStatusServiceGetDataDisksCountDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  ClearLZCDockerData(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty> {
    return this.rpc.unary(BoxStatusServiceClearLZCDockerDataDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  LatestSysStatus(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Observable<BoxSystemStatus> {
    return this.rpc.invoke(BoxStatusServiceLatestSysStatusDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  GetStatus(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<BoxSystemStatus> {
    return this.rpc.unary(BoxStatusServiceGetStatusDesc, Empty.fromPartial(request), metadata, abortSignal);
  }
}

export const BoxStatusServiceDesc = { serviceName: "cloud.lazycat.boxservice.recovery.BoxStatusService" };

export const BoxStatusServiceDataFormatDesc: UnaryMethodDefinitionish = {
  methodName: "DataFormat",
  service: BoxStatusServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return DataDiskFormat.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const BoxStatusServiceDataDecryptDesc: UnaryMethodDefinitionish = {
  methodName: "DataDecrypt",
  service: BoxStatusServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return DataDiskDecrypt.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = DataDecryptResult.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const BoxStatusServiceDataChangePasswordDesc: UnaryMethodDefinitionish = {
  methodName: "DataChangePassword",
  service: BoxStatusServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return DataDiskChangePassword.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = DataChangePasswordResult.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const BoxStatusServiceDataListEncryptedDesc: UnaryMethodDefinitionish = {
  methodName: "DataListEncrypted",
  service: BoxStatusServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = DataListEncryptedResult.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const BoxStatusServiceGetDataDisksCountDesc: UnaryMethodDefinitionish = {
  methodName: "GetDataDisksCount",
  service: BoxStatusServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = GetDataDisksCountReply.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const BoxStatusServiceClearLZCDockerDataDesc: UnaryMethodDefinitionish = {
  methodName: "ClearLZCDockerData",
  service: BoxStatusServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const BoxStatusServiceLatestSysStatusDesc: UnaryMethodDefinitionish = {
  methodName: "LatestSysStatus",
  service: BoxStatusServiceDesc,
  requestStream: false,
  responseStream: true,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = BoxSystemStatus.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const BoxStatusServiceGetStatusDesc: UnaryMethodDefinitionish = {
  methodName: "GetStatus",
  service: BoxStatusServiceDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = BoxSystemStatus.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Promise<any>;
  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Observable<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;
    streamingTransport?: grpc.TransportFactory;
    debug?: boolean;
    metadata?: grpc.Metadata;
    upStreamRetryCodes?: number[];
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;
      streamingTransport?: grpc.TransportFactory;
      debug?: boolean;
      metadata?: grpc.Metadata;
      upStreamRetryCodes?: number[];
    },
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata = metadata && this.options.metadata
      ? new BrowserHeaders({ ...this.options?.metadata.headersMap, ...metadata?.headersMap })
      : metadata ?? this.options.metadata;
    return new Promise((resolve, reject) => {
      const client = grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata ?? {},
        ...(this.options.transport !== undefined ? { transport: this.options.transport } : {}),
        debug: this.options.debug ?? false,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message!.toObject());
          } else {
            const err = new GrpcWebError(response.statusMessage, response.status, response.trailers);
            reject(err);
          }
        },
      });

      if (abortSignal) {
        abortSignal.addEventListener("abort", () => {
          client.close();
          reject(abortSignal.reason);
        });
      }
    });
  }

  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Observable<any> {
    const upStreamCodes = this.options.upStreamRetryCodes ?? [];
    const DEFAULT_TIMEOUT_TIME: number = 3_000;
    const request = { ..._request, ...methodDesc.requestType };
    const transport = this.options.streamingTransport ?? this.options.transport;
    const maybeCombinedMetadata = metadata && this.options.metadata
      ? new BrowserHeaders({ ...this.options?.metadata.headersMap, ...metadata?.headersMap })
      : metadata ?? this.options.metadata;
    return new Observable((observer) => {
      const upStream = () => {
        const client = grpc.invoke(methodDesc, {
          host: this.host,
          request,
          ...(transport !== undefined ? { transport } : {}),
          metadata: maybeCombinedMetadata ?? {},
          debug: this.options.debug ?? false,
          onMessage: (next) => observer.next(next),
          onEnd: (code: grpc.Code, message: string, trailers: grpc.Metadata) => {
            if (code === 0) {
              observer.complete();
            } else if (upStreamCodes.includes(code)) {
              setTimeout(upStream, DEFAULT_TIMEOUT_TIME);
            } else {
              const err = new Error(message) as any;
              err.code = code;
              err.metadata = trailers;
              observer.error(err);
            }
          },
        });
        observer.add(() => {
          if (!abortSignal || !abortSignal.aborted) {
            return client.close();
          }
        });

        if (abortSignal) {
          abortSignal.addEventListener("abort", () => {
            observer.error(abortSignal.reason);
            client.close();
          });
        }
      };
      upStream();
    }).pipe(share());
  }
}

declare const self: any | undefined;
declare const window: any | undefined;
declare const global: any | undefined;
const tsProtoGlobalThis: any = (() => {
  if (typeof globalThis !== "undefined") {
    return globalThis;
  }
  if (typeof self !== "undefined") {
    return self;
  }
  if (typeof window !== "undefined") {
    return window;
  }
  if (typeof global !== "undefined") {
    return global;
  }
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new tsProtoGlobalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

export class GrpcWebError extends tsProtoGlobalThis.Error {
  constructor(message: string, public code: grpc.Code, public metadata: grpc.Metadata) {
    super(message);
  }
}
