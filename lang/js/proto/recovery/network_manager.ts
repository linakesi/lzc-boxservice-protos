/* eslint-disable */
import { grpc } from "@improbable-eng/grpc-web";
import { BrowserHeaders } from "browser-headers";
import Long from "long";
import _m0 from "protobufjs/minimal";
import { Empty } from "../../google/protobuf/empty";

export enum NetworkDeviceStatus {
  /** NetworkDeviceStatusUnavailable - 不可用 */
  NetworkDeviceStatusUnavailable = 0,
  /** NetworkDeviceStatusDisconnected - 未连接 */
  NetworkDeviceStatusDisconnected = 1,
  /** NetworkDeviceStatusConnecting - 正在连接 */
  NetworkDeviceStatusConnecting = 2,
  /** NetworkDeviceStatusConnected - 已连接 */
  NetworkDeviceStatusConnected = 3,
  /** NetworkDeviceStatusDisconnecting - 正在断开 */
  NetworkDeviceStatusDisconnecting = 4,
  /** NetworkDeviceStatusDisabled - 已禁用 */
  NetworkDeviceStatusDisabled = 5,
  UNRECOGNIZED = -1,
}

export function networkDeviceStatusFromJSON(object: any): NetworkDeviceStatus {
  switch (object) {
    case 0:
    case "NetworkDeviceStatusUnavailable":
      return NetworkDeviceStatus.NetworkDeviceStatusUnavailable;
    case 1:
    case "NetworkDeviceStatusDisconnected":
      return NetworkDeviceStatus.NetworkDeviceStatusDisconnected;
    case 2:
    case "NetworkDeviceStatusConnecting":
      return NetworkDeviceStatus.NetworkDeviceStatusConnecting;
    case 3:
    case "NetworkDeviceStatusConnected":
      return NetworkDeviceStatus.NetworkDeviceStatusConnected;
    case 4:
    case "NetworkDeviceStatusDisconnecting":
      return NetworkDeviceStatus.NetworkDeviceStatusDisconnecting;
    case 5:
    case "NetworkDeviceStatusDisabled":
      return NetworkDeviceStatus.NetworkDeviceStatusDisabled;
    case -1:
    case "UNRECOGNIZED":
    default:
      return NetworkDeviceStatus.UNRECOGNIZED;
  }
}

export function networkDeviceStatusToJSON(object: NetworkDeviceStatus): string {
  switch (object) {
    case NetworkDeviceStatus.NetworkDeviceStatusUnavailable:
      return "NetworkDeviceStatusUnavailable";
    case NetworkDeviceStatus.NetworkDeviceStatusDisconnected:
      return "NetworkDeviceStatusDisconnected";
    case NetworkDeviceStatus.NetworkDeviceStatusConnecting:
      return "NetworkDeviceStatusConnecting";
    case NetworkDeviceStatus.NetworkDeviceStatusConnected:
      return "NetworkDeviceStatusConnected";
    case NetworkDeviceStatus.NetworkDeviceStatusDisconnecting:
      return "NetworkDeviceStatusDisconnecting";
    case NetworkDeviceStatus.NetworkDeviceStatusDisabled:
      return "NetworkDeviceStatusDisabled";
    case NetworkDeviceStatus.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export enum KeyMgmt {
  KeyMgmtNone = 0,
  /** KeyMgmtWEP - WEP  （最老的协议了，目前几乎没人用） */
  KeyMgmtWEP = 1,
  /** KeyMgmtWPA_PSK - WPA/WPA2-Personal  （一般大概率都是这个，应当作为默认值） */
  KeyMgmtWPA_PSK = 2,
  /** KeyMgmtSAE - WPA3-Personal  （新一代协议，用的人比较少） */
  KeyMgmtSAE = 3,
  UNRECOGNIZED = -1,
}

export function keyMgmtFromJSON(object: any): KeyMgmt {
  switch (object) {
    case 0:
    case "KeyMgmtNone":
      return KeyMgmt.KeyMgmtNone;
    case 1:
    case "KeyMgmtWEP":
      return KeyMgmt.KeyMgmtWEP;
    case 2:
    case "KeyMgmtWPA_PSK":
      return KeyMgmt.KeyMgmtWPA_PSK;
    case 3:
    case "KeyMgmtSAE":
      return KeyMgmt.KeyMgmtSAE;
    case -1:
    case "UNRECOGNIZED":
    default:
      return KeyMgmt.UNRECOGNIZED;
  }
}

export function keyMgmtToJSON(object: KeyMgmt): string {
  switch (object) {
    case KeyMgmt.KeyMgmtNone:
      return "KeyMgmtNone";
    case KeyMgmt.KeyMgmtWEP:
      return "KeyMgmtWEP";
    case KeyMgmt.KeyMgmtWPA_PSK:
      return "KeyMgmtWPA_PSK";
    case KeyMgmt.KeyMgmtSAE:
      return "KeyMgmtSAE";
    case KeyMgmt.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export enum WifiConnectResult {
  WifiConnectResultSuccess = 0,
  WifiConnectResultNoSuchBssid = 1,
  WifiConnectResultWrongPassword = 2,
  WifiConnectResultUnknownError = 3,
  UNRECOGNIZED = -1,
}

export function wifiConnectResultFromJSON(object: any): WifiConnectResult {
  switch (object) {
    case 0:
    case "WifiConnectResultSuccess":
      return WifiConnectResult.WifiConnectResultSuccess;
    case 1:
    case "WifiConnectResultNoSuchBssid":
      return WifiConnectResult.WifiConnectResultNoSuchBssid;
    case 2:
    case "WifiConnectResultWrongPassword":
      return WifiConnectResult.WifiConnectResultWrongPassword;
    case 3:
    case "WifiConnectResultUnknownError":
      return WifiConnectResult.WifiConnectResultUnknownError;
    case -1:
    case "UNRECOGNIZED":
    default:
      return WifiConnectResult.UNRECOGNIZED;
  }
}

export function wifiConnectResultToJSON(object: WifiConnectResult): string {
  switch (object) {
    case WifiConnectResult.WifiConnectResultSuccess:
      return "WifiConnectResultSuccess";
    case WifiConnectResult.WifiConnectResultNoSuchBssid:
      return "WifiConnectResultNoSuchBssid";
    case WifiConnectResult.WifiConnectResultWrongPassword:
      return "WifiConnectResultWrongPassword";
    case WifiConnectResult.WifiConnectResultUnknownError:
      return "WifiConnectResultUnknownError";
    case WifiConnectResult.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface AccessPointInfo {
  /** 热点的网卡 mac 地址（由于 ssid 可能重复，所以将此字段作为整个列表的 index） */
  bssid: string;
  /** 热点的 ssid */
  ssid: string;
  /** 信号强度（范围 0 <= signal <= 100） */
  signal: number;
  /** 是否需要密码 */
  security: boolean;
  /** 是否已连接 */
  connected: boolean;
  /** 是否已保存密码 */
  saved: boolean;
  /** 是否自动连接 */
  autoConnected: boolean;
  /** 是否锁定 bssid （锁定后，不会自动连接其他同名不同 bssid 的热点） */
  bssidLocked: boolean;
}

export interface AccessPointInfoList {
  list: AccessPointInfo[];
}

export interface NetworkDeviceStatusInfo {
  /** 是否已连接到互联网 */
  hasInternet: boolean;
  /** 有线连接状态（已假设只有一块有线网卡） */
  WiredDevice: NetworkDeviceStatus;
  /** 无线设备状态（已假设只有一块无线网卡） */
  WirelessDevice: NetworkDeviceStatus;
  /** 若无线设备已连接，则该字段表示已连接的 wifi 的信息 */
  info?: AccessPointInfo | undefined;
}

export interface WifiConnectInfo {
  /** bssid 和 ssid 指定其一即可 */
  bssid: string;
  /** 当指定了 bssid 时，该值无效。所以要指定该值就不要指定 bssid */
  ssid: string;
  /** wifi 密码，如果 KeyMgmt 值是 KeyMgmtNone 则该值无效 */
  password: string;
  /** 加密类型，该值一般不用指定，会自动嗅探，只有连接隐藏网络或手动添加网络才会用到此字段 */
  keyMgmt?:
    | KeyMgmt
    | undefined;
  /**
   * 连接超时时间（单位：秒），如果不指定则为 NetworkManager 默认行为。
   * 如果连接超时，会当作失败处理抛出异常
   */
  timeout?: number | undefined;
}

export interface WifiConfigInfo {
  ssid: string;
  password: string;
  keyMgmt: KeyMgmt;
}

export interface WifiConnectReply {
  result: WifiConnectResult;
}

function createBaseAccessPointInfo(): AccessPointInfo {
  return {
    bssid: "",
    ssid: "",
    signal: 0,
    security: false,
    connected: false,
    saved: false,
    autoConnected: false,
    bssidLocked: false,
  };
}

export const AccessPointInfo = {
  encode(message: AccessPointInfo, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.bssid !== "") {
      writer.uint32(10).string(message.bssid);
    }
    if (message.ssid !== "") {
      writer.uint32(18).string(message.ssid);
    }
    if (message.signal !== 0) {
      writer.uint32(24).int32(message.signal);
    }
    if (message.security === true) {
      writer.uint32(32).bool(message.security);
    }
    if (message.connected === true) {
      writer.uint32(40).bool(message.connected);
    }
    if (message.saved === true) {
      writer.uint32(48).bool(message.saved);
    }
    if (message.autoConnected === true) {
      writer.uint32(56).bool(message.autoConnected);
    }
    if (message.bssidLocked === true) {
      writer.uint32(64).bool(message.bssidLocked);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): AccessPointInfo {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseAccessPointInfo();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.bssid = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.ssid = reader.string();
          continue;
        case 3:
          if (tag !== 24) {
            break;
          }

          message.signal = reader.int32();
          continue;
        case 4:
          if (tag !== 32) {
            break;
          }

          message.security = reader.bool();
          continue;
        case 5:
          if (tag !== 40) {
            break;
          }

          message.connected = reader.bool();
          continue;
        case 6:
          if (tag !== 48) {
            break;
          }

          message.saved = reader.bool();
          continue;
        case 7:
          if (tag !== 56) {
            break;
          }

          message.autoConnected = reader.bool();
          continue;
        case 8:
          if (tag !== 64) {
            break;
          }

          message.bssidLocked = reader.bool();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): AccessPointInfo {
    return {
      bssid: isSet(object.bssid) ? String(object.bssid) : "",
      ssid: isSet(object.ssid) ? String(object.ssid) : "",
      signal: isSet(object.signal) ? Number(object.signal) : 0,
      security: isSet(object.security) ? Boolean(object.security) : false,
      connected: isSet(object.connected) ? Boolean(object.connected) : false,
      saved: isSet(object.saved) ? Boolean(object.saved) : false,
      autoConnected: isSet(object.autoConnected) ? Boolean(object.autoConnected) : false,
      bssidLocked: isSet(object.bssidLocked) ? Boolean(object.bssidLocked) : false,
    };
  },

  toJSON(message: AccessPointInfo): unknown {
    const obj: any = {};
    if (message.bssid !== "") {
      obj.bssid = message.bssid;
    }
    if (message.ssid !== "") {
      obj.ssid = message.ssid;
    }
    if (message.signal !== 0) {
      obj.signal = Math.round(message.signal);
    }
    if (message.security === true) {
      obj.security = message.security;
    }
    if (message.connected === true) {
      obj.connected = message.connected;
    }
    if (message.saved === true) {
      obj.saved = message.saved;
    }
    if (message.autoConnected === true) {
      obj.autoConnected = message.autoConnected;
    }
    if (message.bssidLocked === true) {
      obj.bssidLocked = message.bssidLocked;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<AccessPointInfo>, I>>(base?: I): AccessPointInfo {
    return AccessPointInfo.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<AccessPointInfo>, I>>(object: I): AccessPointInfo {
    const message = createBaseAccessPointInfo();
    message.bssid = object.bssid ?? "";
    message.ssid = object.ssid ?? "";
    message.signal = object.signal ?? 0;
    message.security = object.security ?? false;
    message.connected = object.connected ?? false;
    message.saved = object.saved ?? false;
    message.autoConnected = object.autoConnected ?? false;
    message.bssidLocked = object.bssidLocked ?? false;
    return message;
  },
};

function createBaseAccessPointInfoList(): AccessPointInfoList {
  return { list: [] };
}

export const AccessPointInfoList = {
  encode(message: AccessPointInfoList, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.list) {
      AccessPointInfo.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): AccessPointInfoList {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseAccessPointInfoList();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.list.push(AccessPointInfo.decode(reader, reader.uint32()));
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): AccessPointInfoList {
    return { list: Array.isArray(object?.list) ? object.list.map((e: any) => AccessPointInfo.fromJSON(e)) : [] };
  },

  toJSON(message: AccessPointInfoList): unknown {
    const obj: any = {};
    if (message.list?.length) {
      obj.list = message.list.map((e) => AccessPointInfo.toJSON(e));
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<AccessPointInfoList>, I>>(base?: I): AccessPointInfoList {
    return AccessPointInfoList.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<AccessPointInfoList>, I>>(object: I): AccessPointInfoList {
    const message = createBaseAccessPointInfoList();
    message.list = object.list?.map((e) => AccessPointInfo.fromPartial(e)) || [];
    return message;
  },
};

function createBaseNetworkDeviceStatusInfo(): NetworkDeviceStatusInfo {
  return { hasInternet: false, WiredDevice: 0, WirelessDevice: 0, info: undefined };
}

export const NetworkDeviceStatusInfo = {
  encode(message: NetworkDeviceStatusInfo, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.hasInternet === true) {
      writer.uint32(8).bool(message.hasInternet);
    }
    if (message.WiredDevice !== 0) {
      writer.uint32(16).int32(message.WiredDevice);
    }
    if (message.WirelessDevice !== 0) {
      writer.uint32(24).int32(message.WirelessDevice);
    }
    if (message.info !== undefined) {
      AccessPointInfo.encode(message.info, writer.uint32(34).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): NetworkDeviceStatusInfo {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseNetworkDeviceStatusInfo();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.hasInternet = reader.bool();
          continue;
        case 2:
          if (tag !== 16) {
            break;
          }

          message.WiredDevice = reader.int32() as any;
          continue;
        case 3:
          if (tag !== 24) {
            break;
          }

          message.WirelessDevice = reader.int32() as any;
          continue;
        case 4:
          if (tag !== 34) {
            break;
          }

          message.info = AccessPointInfo.decode(reader, reader.uint32());
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): NetworkDeviceStatusInfo {
    return {
      hasInternet: isSet(object.hasInternet) ? Boolean(object.hasInternet) : false,
      WiredDevice: isSet(object.WiredDevice) ? networkDeviceStatusFromJSON(object.WiredDevice) : 0,
      WirelessDevice: isSet(object.WirelessDevice) ? networkDeviceStatusFromJSON(object.WirelessDevice) : 0,
      info: isSet(object.info) ? AccessPointInfo.fromJSON(object.info) : undefined,
    };
  },

  toJSON(message: NetworkDeviceStatusInfo): unknown {
    const obj: any = {};
    if (message.hasInternet === true) {
      obj.hasInternet = message.hasInternet;
    }
    if (message.WiredDevice !== 0) {
      obj.WiredDevice = networkDeviceStatusToJSON(message.WiredDevice);
    }
    if (message.WirelessDevice !== 0) {
      obj.WirelessDevice = networkDeviceStatusToJSON(message.WirelessDevice);
    }
    if (message.info !== undefined) {
      obj.info = AccessPointInfo.toJSON(message.info);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<NetworkDeviceStatusInfo>, I>>(base?: I): NetworkDeviceStatusInfo {
    return NetworkDeviceStatusInfo.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<NetworkDeviceStatusInfo>, I>>(object: I): NetworkDeviceStatusInfo {
    const message = createBaseNetworkDeviceStatusInfo();
    message.hasInternet = object.hasInternet ?? false;
    message.WiredDevice = object.WiredDevice ?? 0;
    message.WirelessDevice = object.WirelessDevice ?? 0;
    message.info = (object.info !== undefined && object.info !== null)
      ? AccessPointInfo.fromPartial(object.info)
      : undefined;
    return message;
  },
};

function createBaseWifiConnectInfo(): WifiConnectInfo {
  return { bssid: "", ssid: "", password: "", keyMgmt: undefined, timeout: undefined };
}

export const WifiConnectInfo = {
  encode(message: WifiConnectInfo, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.bssid !== "") {
      writer.uint32(10).string(message.bssid);
    }
    if (message.ssid !== "") {
      writer.uint32(18).string(message.ssid);
    }
    if (message.password !== "") {
      writer.uint32(26).string(message.password);
    }
    if (message.keyMgmt !== undefined) {
      writer.uint32(32).int32(message.keyMgmt);
    }
    if (message.timeout !== undefined) {
      writer.uint32(40).int64(message.timeout);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): WifiConnectInfo {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseWifiConnectInfo();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.bssid = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.ssid = reader.string();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.password = reader.string();
          continue;
        case 4:
          if (tag !== 32) {
            break;
          }

          message.keyMgmt = reader.int32() as any;
          continue;
        case 5:
          if (tag !== 40) {
            break;
          }

          message.timeout = longToNumber(reader.int64() as Long);
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): WifiConnectInfo {
    return {
      bssid: isSet(object.bssid) ? String(object.bssid) : "",
      ssid: isSet(object.ssid) ? String(object.ssid) : "",
      password: isSet(object.password) ? String(object.password) : "",
      keyMgmt: isSet(object.keyMgmt) ? keyMgmtFromJSON(object.keyMgmt) : undefined,
      timeout: isSet(object.timeout) ? Number(object.timeout) : undefined,
    };
  },

  toJSON(message: WifiConnectInfo): unknown {
    const obj: any = {};
    if (message.bssid !== "") {
      obj.bssid = message.bssid;
    }
    if (message.ssid !== "") {
      obj.ssid = message.ssid;
    }
    if (message.password !== "") {
      obj.password = message.password;
    }
    if (message.keyMgmt !== undefined) {
      obj.keyMgmt = keyMgmtToJSON(message.keyMgmt);
    }
    if (message.timeout !== undefined) {
      obj.timeout = Math.round(message.timeout);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<WifiConnectInfo>, I>>(base?: I): WifiConnectInfo {
    return WifiConnectInfo.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<WifiConnectInfo>, I>>(object: I): WifiConnectInfo {
    const message = createBaseWifiConnectInfo();
    message.bssid = object.bssid ?? "";
    message.ssid = object.ssid ?? "";
    message.password = object.password ?? "";
    message.keyMgmt = object.keyMgmt ?? undefined;
    message.timeout = object.timeout ?? undefined;
    return message;
  },
};

function createBaseWifiConfigInfo(): WifiConfigInfo {
  return { ssid: "", password: "", keyMgmt: 0 };
}

export const WifiConfigInfo = {
  encode(message: WifiConfigInfo, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.ssid !== "") {
      writer.uint32(10).string(message.ssid);
    }
    if (message.password !== "") {
      writer.uint32(18).string(message.password);
    }
    if (message.keyMgmt !== 0) {
      writer.uint32(24).int32(message.keyMgmt);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): WifiConfigInfo {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseWifiConfigInfo();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.ssid = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.password = reader.string();
          continue;
        case 3:
          if (tag !== 24) {
            break;
          }

          message.keyMgmt = reader.int32() as any;
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): WifiConfigInfo {
    return {
      ssid: isSet(object.ssid) ? String(object.ssid) : "",
      password: isSet(object.password) ? String(object.password) : "",
      keyMgmt: isSet(object.keyMgmt) ? keyMgmtFromJSON(object.keyMgmt) : 0,
    };
  },

  toJSON(message: WifiConfigInfo): unknown {
    const obj: any = {};
    if (message.ssid !== "") {
      obj.ssid = message.ssid;
    }
    if (message.password !== "") {
      obj.password = message.password;
    }
    if (message.keyMgmt !== 0) {
      obj.keyMgmt = keyMgmtToJSON(message.keyMgmt);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<WifiConfigInfo>, I>>(base?: I): WifiConfigInfo {
    return WifiConfigInfo.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<WifiConfigInfo>, I>>(object: I): WifiConfigInfo {
    const message = createBaseWifiConfigInfo();
    message.ssid = object.ssid ?? "";
    message.password = object.password ?? "";
    message.keyMgmt = object.keyMgmt ?? 0;
    return message;
  },
};

function createBaseWifiConnectReply(): WifiConnectReply {
  return { result: 0 };
}

export const WifiConnectReply = {
  encode(message: WifiConnectReply, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.result !== 0) {
      writer.uint32(8).int32(message.result);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): WifiConnectReply {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseWifiConnectReply();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.result = reader.int32() as any;
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): WifiConnectReply {
    return { result: isSet(object.result) ? wifiConnectResultFromJSON(object.result) : 0 };
  },

  toJSON(message: WifiConnectReply): unknown {
    const obj: any = {};
    if (message.result !== 0) {
      obj.result = wifiConnectResultToJSON(message.result);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<WifiConnectReply>, I>>(base?: I): WifiConnectReply {
    return WifiConnectReply.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<WifiConnectReply>, I>>(object: I): WifiConnectReply {
    const message = createBaseWifiConnectReply();
    message.result = object.result ?? 0;
    return message;
  },
};

export interface NetworkManager {
  /** 获取网络设备的状态（是否已连接，连接了哪个） */
  Status(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<NetworkDeviceStatusInfo>;
  /** Scan 扫描附近wifi热点信息，扫描结果在内部缓存里（阻塞，可能耗费数秒） */
  WifiScan(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty>;
  /** List 列出内部缓存中的 wifi 列表 */
  WifiList(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<AccessPointInfoList>;
  /**
   * 连接一个 wifi 热点
   *   连接失败会删除已保存的配置，并自动连回上一次连接的 wifi（如果有的话），防止失联
   */
  WifiConnect(
    request: DeepPartial<WifiConnectInfo>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<WifiConnectReply>;
  /** 手动添加和连接一个 wifi 热点配置（用于连接隐藏网络） */
  WifiConfigAdd(
    request: DeepPartial<WifiConfigInfo>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<WifiConnectReply>;
}

export class NetworkManagerClientImpl implements NetworkManager {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.Status = this.Status.bind(this);
    this.WifiScan = this.WifiScan.bind(this);
    this.WifiList = this.WifiList.bind(this);
    this.WifiConnect = this.WifiConnect.bind(this);
    this.WifiConfigAdd = this.WifiConfigAdd.bind(this);
  }

  Status(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<NetworkDeviceStatusInfo> {
    return this.rpc.unary(NetworkManagerStatusDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  WifiScan(request: DeepPartial<Empty>, metadata?: grpc.Metadata, abortSignal?: AbortSignal): Promise<Empty> {
    return this.rpc.unary(NetworkManagerWifiScanDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  WifiList(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<AccessPointInfoList> {
    return this.rpc.unary(NetworkManagerWifiListDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  WifiConnect(
    request: DeepPartial<WifiConnectInfo>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<WifiConnectReply> {
    return this.rpc.unary(NetworkManagerWifiConnectDesc, WifiConnectInfo.fromPartial(request), metadata, abortSignal);
  }

  WifiConfigAdd(
    request: DeepPartial<WifiConfigInfo>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<WifiConnectReply> {
    return this.rpc.unary(NetworkManagerWifiConfigAddDesc, WifiConfigInfo.fromPartial(request), metadata, abortSignal);
  }
}

export const NetworkManagerDesc = { serviceName: "cloud.lazycat.boxservice.recovery.NetworkManager" };

export const NetworkManagerStatusDesc: UnaryMethodDefinitionish = {
  methodName: "Status",
  service: NetworkManagerDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = NetworkDeviceStatusInfo.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const NetworkManagerWifiScanDesc: UnaryMethodDefinitionish = {
  methodName: "WifiScan",
  service: NetworkManagerDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const NetworkManagerWifiListDesc: UnaryMethodDefinitionish = {
  methodName: "WifiList",
  service: NetworkManagerDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = AccessPointInfoList.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const NetworkManagerWifiConnectDesc: UnaryMethodDefinitionish = {
  methodName: "WifiConnect",
  service: NetworkManagerDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return WifiConnectInfo.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = WifiConnectReply.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const NetworkManagerWifiConfigAddDesc: UnaryMethodDefinitionish = {
  methodName: "WifiConfigAdd",
  service: NetworkManagerDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return WifiConfigInfo.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = WifiConnectReply.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Promise<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;

    debug?: boolean;
    metadata?: grpc.Metadata;
    upStreamRetryCodes?: number[];
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;

      debug?: boolean;
      metadata?: grpc.Metadata;
      upStreamRetryCodes?: number[];
    },
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata = metadata && this.options.metadata
      ? new BrowserHeaders({ ...this.options?.metadata.headersMap, ...metadata?.headersMap })
      : metadata ?? this.options.metadata;
    return new Promise((resolve, reject) => {
      const client = grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata ?? {},
        ...(this.options.transport !== undefined ? { transport: this.options.transport } : {}),
        debug: this.options.debug ?? false,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message!.toObject());
          } else {
            const err = new GrpcWebError(response.statusMessage, response.status, response.trailers);
            reject(err);
          }
        },
      });

      if (abortSignal) {
        abortSignal.addEventListener("abort", () => {
          client.close();
          reject(abortSignal.reason);
        });
      }
    });
  }
}

declare const self: any | undefined;
declare const window: any | undefined;
declare const global: any | undefined;
const tsProtoGlobalThis: any = (() => {
  if (typeof globalThis !== "undefined") {
    return globalThis;
  }
  if (typeof self !== "undefined") {
    return self;
  }
  if (typeof window !== "undefined") {
    return window;
  }
  if (typeof global !== "undefined") {
    return global;
  }
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new tsProtoGlobalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

export class GrpcWebError extends tsProtoGlobalThis.Error {
  constructor(message: string, public code: grpc.Code, public metadata: grpc.Metadata) {
    super(message);
  }
}
