/* eslint-disable */
import { grpc } from "@improbable-eng/grpc-web";
import { BrowserHeaders } from "browser-headers";
import Long from "long";
import _m0 from "protobufjs/minimal";
import { Observable } from "rxjs";
import { share } from "rxjs/operators";
import { Empty } from "../../google/protobuf/empty";

export interface ListPhoneNumberResponse {
  numbers: string[];
}

export interface RemovePhoneNumberRequest {
  phoneNumber: string[];
}

export interface RemovePhoneNumberResponse {
  success: boolean;
}

export interface unreadMsgCountResponse {
  count: number;
}

export interface NewTrustPhoneNumberRequest {
  phoneNumber: string;
  /** 手机验证码 */
  verifyCode?: string | undefined;
}

export interface NewTrustPhoneNumberResponse {
  success: boolean;
}

export interface MatchOSVersionRequest {
  clientRule: ClientVersionRule | undefined;
}

export interface MatchOSVersionReply {
  /** 盒子系统的版本号 */
  boxosVersion: number;
  matchResult: MatchOSVersionReply_Result;
  /** 如果Result不为OK,则期望的最小版本 */
  wantedMinVersion: number;
}

export enum MatchOSVersionReply_Result {
  MatchOK = 0,
  /** ClientVersionTooLow - 客户端版本过低 */
  ClientVersionTooLow = 1,
  /** BoxOSVersionTooLow - 盒子系统版本过低 */
  BoxOSVersionTooLow = 2,
  UNRECOGNIZED = -1,
}

export function matchOSVersionReply_ResultFromJSON(object: any): MatchOSVersionReply_Result {
  switch (object) {
    case 0:
    case "MatchOK":
      return MatchOSVersionReply_Result.MatchOK;
    case 1:
    case "ClientVersionTooLow":
      return MatchOSVersionReply_Result.ClientVersionTooLow;
    case 2:
    case "BoxOSVersionTooLow":
      return MatchOSVersionReply_Result.BoxOSVersionTooLow;
    case -1:
    case "UNRECOGNIZED":
    default:
      return MatchOSVersionReply_Result.UNRECOGNIZED;
  }
}

export function matchOSVersionReply_ResultToJSON(object: MatchOSVersionReply_Result): string {
  switch (object) {
    case MatchOSVersionReply_Result.MatchOK:
      return "MatchOK";
    case MatchOSVersionReply_Result.ClientVersionTooLow:
      return "ClientVersionTooLow";
    case MatchOSVersionReply_Result.BoxOSVersionTooLow:
      return "BoxOSVersionTooLow";
    case MatchOSVersionReply_Result.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

/** 内嵌到客户端 */
export interface ClientVersionRule {
  clientId: string;
  /** 客户端的大版本号 */
  clientVersion: number;
  /** 支持的最低盒子系统版本 */
  boxosMinVersion: number;
}

export interface BoxSystemStatus {
  status: BoxSystemStatus_SysStatus;
  serviceUrl: string;
  exceptionReason?: BoxSystemStatus_ExceptionReason | undefined;
}

export enum BoxSystemStatus_SysStatus {
  /** Booting - 系统正在启动中 */
  Booting = 0,
  /** Normal - 处于正常可用状态 */
  Normal = 1,
  /** Exception - 处于异常状态，具体原因可读取ExceptionReason */
  Exception = 2,
  /** Upgrading2 - 微服升级中，数秒后会自动重启，在该状态下，用户不应该使用盒子，以免导致数据丢失 */
  Upgrading2 = 3,
  /** Reserved - 保留 */
  Reserved = 4,
  UNRECOGNIZED = -1,
}

export function boxSystemStatus_SysStatusFromJSON(object: any): BoxSystemStatus_SysStatus {
  switch (object) {
    case 0:
    case "Booting":
      return BoxSystemStatus_SysStatus.Booting;
    case 1:
    case "Normal":
      return BoxSystemStatus_SysStatus.Normal;
    case 2:
    case "Exception":
      return BoxSystemStatus_SysStatus.Exception;
    case 3:
    case "Upgrading2":
      return BoxSystemStatus_SysStatus.Upgrading2;
    case 4:
    case "Reserved":
      return BoxSystemStatus_SysStatus.Reserved;
    case -1:
    case "UNRECOGNIZED":
    default:
      return BoxSystemStatus_SysStatus.UNRECOGNIZED;
  }
}

export function boxSystemStatus_SysStatusToJSON(object: BoxSystemStatus_SysStatus): string {
  switch (object) {
    case BoxSystemStatus_SysStatus.Booting:
      return "Booting";
    case BoxSystemStatus_SysStatus.Normal:
      return "Normal";
    case BoxSystemStatus_SysStatus.Exception:
      return "Exception";
    case BoxSystemStatus_SysStatus.Upgrading2:
      return "Upgrading2";
    case BoxSystemStatus_SysStatus.Reserved:
      return "Reserved";
    case BoxSystemStatus_SysStatus.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export enum BoxSystemStatus_ExceptionReason {
  /** Unknown - 其他原因 */
  Unknown = 0,
  /** WaitDataDiskDecryption - 等待加密数据盘被解密 */
  WaitDataDiskDecryption = 1,
  /** DataNotInit - 数据盘没有初始化 */
  DataNotInit = 2,
  /**
   * DataException - 1. 数据盘不存在（数据盘未插，或内部的 box.data.path 配置不正确）
   * 2. 数据盘里已有分区（非 btrfs）
   * 3. 数据盘无法挂载（文件系统损坏等）
   * 4. 其它未意料的数据盘相关异常
   */
  DataException = 3,
  /**
   * Upgrading - 微服升级中，数秒后会自动重启，在该状态下，用户不应该使用盒子，以免导致数据丢失
   * deprecated 请使用SysStatus的Upgrading2
   */
  Upgrading = 4,
  UNRECOGNIZED = -1,
}

export function boxSystemStatus_ExceptionReasonFromJSON(object: any): BoxSystemStatus_ExceptionReason {
  switch (object) {
    case 0:
    case "Unknown":
      return BoxSystemStatus_ExceptionReason.Unknown;
    case 1:
    case "WaitDataDiskDecryption":
      return BoxSystemStatus_ExceptionReason.WaitDataDiskDecryption;
    case 2:
    case "DataNotInit":
      return BoxSystemStatus_ExceptionReason.DataNotInit;
    case 3:
    case "DataException":
      return BoxSystemStatus_ExceptionReason.DataException;
    case 4:
    case "Upgrading":
      return BoxSystemStatus_ExceptionReason.Upgrading;
    case -1:
    case "UNRECOGNIZED":
    default:
      return BoxSystemStatus_ExceptionReason.UNRECOGNIZED;
  }
}

export function boxSystemStatus_ExceptionReasonToJSON(object: BoxSystemStatus_ExceptionReason): string {
  switch (object) {
    case BoxSystemStatus_ExceptionReason.Unknown:
      return "Unknown";
    case BoxSystemStatus_ExceptionReason.WaitDataDiskDecryption:
      return "WaitDataDiskDecryption";
    case BoxSystemStatus_ExceptionReason.DataNotInit:
      return "DataNotInit";
    case BoxSystemStatus_ExceptionReason.DataException:
      return "DataException";
    case BoxSystemStatus_ExceptionReason.Upgrading:
      return "Upgrading";
    case BoxSystemStatus_ExceptionReason.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

function createBaseListPhoneNumberResponse(): ListPhoneNumberResponse {
  return { numbers: [] };
}

export const ListPhoneNumberResponse = {
  encode(message: ListPhoneNumberResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.numbers) {
      writer.uint32(10).string(v!);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ListPhoneNumberResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseListPhoneNumberResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.numbers.push(reader.string());
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): ListPhoneNumberResponse {
    return { numbers: Array.isArray(object?.numbers) ? object.numbers.map((e: any) => String(e)) : [] };
  },

  toJSON(message: ListPhoneNumberResponse): unknown {
    const obj: any = {};
    if (message.numbers?.length) {
      obj.numbers = message.numbers;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<ListPhoneNumberResponse>, I>>(base?: I): ListPhoneNumberResponse {
    return ListPhoneNumberResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<ListPhoneNumberResponse>, I>>(object: I): ListPhoneNumberResponse {
    const message = createBaseListPhoneNumberResponse();
    message.numbers = object.numbers?.map((e) => e) || [];
    return message;
  },
};

function createBaseRemovePhoneNumberRequest(): RemovePhoneNumberRequest {
  return { phoneNumber: [] };
}

export const RemovePhoneNumberRequest = {
  encode(message: RemovePhoneNumberRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.phoneNumber) {
      writer.uint32(10).string(v!);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): RemovePhoneNumberRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseRemovePhoneNumberRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.phoneNumber.push(reader.string());
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): RemovePhoneNumberRequest {
    return { phoneNumber: Array.isArray(object?.phoneNumber) ? object.phoneNumber.map((e: any) => String(e)) : [] };
  },

  toJSON(message: RemovePhoneNumberRequest): unknown {
    const obj: any = {};
    if (message.phoneNumber?.length) {
      obj.phoneNumber = message.phoneNumber;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<RemovePhoneNumberRequest>, I>>(base?: I): RemovePhoneNumberRequest {
    return RemovePhoneNumberRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<RemovePhoneNumberRequest>, I>>(object: I): RemovePhoneNumberRequest {
    const message = createBaseRemovePhoneNumberRequest();
    message.phoneNumber = object.phoneNumber?.map((e) => e) || [];
    return message;
  },
};

function createBaseRemovePhoneNumberResponse(): RemovePhoneNumberResponse {
  return { success: false };
}

export const RemovePhoneNumberResponse = {
  encode(message: RemovePhoneNumberResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.success === true) {
      writer.uint32(8).bool(message.success);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): RemovePhoneNumberResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseRemovePhoneNumberResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.success = reader.bool();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): RemovePhoneNumberResponse {
    return { success: isSet(object.success) ? Boolean(object.success) : false };
  },

  toJSON(message: RemovePhoneNumberResponse): unknown {
    const obj: any = {};
    if (message.success === true) {
      obj.success = message.success;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<RemovePhoneNumberResponse>, I>>(base?: I): RemovePhoneNumberResponse {
    return RemovePhoneNumberResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<RemovePhoneNumberResponse>, I>>(object: I): RemovePhoneNumberResponse {
    const message = createBaseRemovePhoneNumberResponse();
    message.success = object.success ?? false;
    return message;
  },
};

function createBaseunreadMsgCountResponse(): unreadMsgCountResponse {
  return { count: 0 };
}

export const unreadMsgCountResponse = {
  encode(message: unreadMsgCountResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.count !== 0) {
      writer.uint32(8).int64(message.count);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): unreadMsgCountResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseunreadMsgCountResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.count = longToNumber(reader.int64() as Long);
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): unreadMsgCountResponse {
    return { count: isSet(object.count) ? Number(object.count) : 0 };
  },

  toJSON(message: unreadMsgCountResponse): unknown {
    const obj: any = {};
    if (message.count !== 0) {
      obj.count = Math.round(message.count);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<unreadMsgCountResponse>, I>>(base?: I): unreadMsgCountResponse {
    return unreadMsgCountResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<unreadMsgCountResponse>, I>>(object: I): unreadMsgCountResponse {
    const message = createBaseunreadMsgCountResponse();
    message.count = object.count ?? 0;
    return message;
  },
};

function createBaseNewTrustPhoneNumberRequest(): NewTrustPhoneNumberRequest {
  return { phoneNumber: "", verifyCode: undefined };
}

export const NewTrustPhoneNumberRequest = {
  encode(message: NewTrustPhoneNumberRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.phoneNumber !== "") {
      writer.uint32(10).string(message.phoneNumber);
    }
    if (message.verifyCode !== undefined) {
      writer.uint32(26).string(message.verifyCode);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): NewTrustPhoneNumberRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseNewTrustPhoneNumberRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.phoneNumber = reader.string();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.verifyCode = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): NewTrustPhoneNumberRequest {
    return {
      phoneNumber: isSet(object.phoneNumber) ? String(object.phoneNumber) : "",
      verifyCode: isSet(object.verifyCode) ? String(object.verifyCode) : undefined,
    };
  },

  toJSON(message: NewTrustPhoneNumberRequest): unknown {
    const obj: any = {};
    if (message.phoneNumber !== "") {
      obj.phoneNumber = message.phoneNumber;
    }
    if (message.verifyCode !== undefined) {
      obj.verifyCode = message.verifyCode;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<NewTrustPhoneNumberRequest>, I>>(base?: I): NewTrustPhoneNumberRequest {
    return NewTrustPhoneNumberRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<NewTrustPhoneNumberRequest>, I>>(object: I): NewTrustPhoneNumberRequest {
    const message = createBaseNewTrustPhoneNumberRequest();
    message.phoneNumber = object.phoneNumber ?? "";
    message.verifyCode = object.verifyCode ?? undefined;
    return message;
  },
};

function createBaseNewTrustPhoneNumberResponse(): NewTrustPhoneNumberResponse {
  return { success: false };
}

export const NewTrustPhoneNumberResponse = {
  encode(message: NewTrustPhoneNumberResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.success === true) {
      writer.uint32(8).bool(message.success);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): NewTrustPhoneNumberResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseNewTrustPhoneNumberResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.success = reader.bool();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): NewTrustPhoneNumberResponse {
    return { success: isSet(object.success) ? Boolean(object.success) : false };
  },

  toJSON(message: NewTrustPhoneNumberResponse): unknown {
    const obj: any = {};
    if (message.success === true) {
      obj.success = message.success;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<NewTrustPhoneNumberResponse>, I>>(base?: I): NewTrustPhoneNumberResponse {
    return NewTrustPhoneNumberResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<NewTrustPhoneNumberResponse>, I>>(object: I): NewTrustPhoneNumberResponse {
    const message = createBaseNewTrustPhoneNumberResponse();
    message.success = object.success ?? false;
    return message;
  },
};

function createBaseMatchOSVersionRequest(): MatchOSVersionRequest {
  return { clientRule: undefined };
}

export const MatchOSVersionRequest = {
  encode(message: MatchOSVersionRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.clientRule !== undefined) {
      ClientVersionRule.encode(message.clientRule, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MatchOSVersionRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMatchOSVersionRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.clientRule = ClientVersionRule.decode(reader, reader.uint32());
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): MatchOSVersionRequest {
    return { clientRule: isSet(object.clientRule) ? ClientVersionRule.fromJSON(object.clientRule) : undefined };
  },

  toJSON(message: MatchOSVersionRequest): unknown {
    const obj: any = {};
    if (message.clientRule !== undefined) {
      obj.clientRule = ClientVersionRule.toJSON(message.clientRule);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<MatchOSVersionRequest>, I>>(base?: I): MatchOSVersionRequest {
    return MatchOSVersionRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<MatchOSVersionRequest>, I>>(object: I): MatchOSVersionRequest {
    const message = createBaseMatchOSVersionRequest();
    message.clientRule = (object.clientRule !== undefined && object.clientRule !== null)
      ? ClientVersionRule.fromPartial(object.clientRule)
      : undefined;
    return message;
  },
};

function createBaseMatchOSVersionReply(): MatchOSVersionReply {
  return { boxosVersion: 0, matchResult: 0, wantedMinVersion: 0 };
}

export const MatchOSVersionReply = {
  encode(message: MatchOSVersionReply, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.boxosVersion !== 0) {
      writer.uint32(8).int32(message.boxosVersion);
    }
    if (message.matchResult !== 0) {
      writer.uint32(16).int32(message.matchResult);
    }
    if (message.wantedMinVersion !== 0) {
      writer.uint32(24).int32(message.wantedMinVersion);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MatchOSVersionReply {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMatchOSVersionReply();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.boxosVersion = reader.int32();
          continue;
        case 2:
          if (tag !== 16) {
            break;
          }

          message.matchResult = reader.int32() as any;
          continue;
        case 3:
          if (tag !== 24) {
            break;
          }

          message.wantedMinVersion = reader.int32();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): MatchOSVersionReply {
    return {
      boxosVersion: isSet(object.boxosVersion) ? Number(object.boxosVersion) : 0,
      matchResult: isSet(object.matchResult) ? matchOSVersionReply_ResultFromJSON(object.matchResult) : 0,
      wantedMinVersion: isSet(object.wantedMinVersion) ? Number(object.wantedMinVersion) : 0,
    };
  },

  toJSON(message: MatchOSVersionReply): unknown {
    const obj: any = {};
    if (message.boxosVersion !== 0) {
      obj.boxosVersion = Math.round(message.boxosVersion);
    }
    if (message.matchResult !== 0) {
      obj.matchResult = matchOSVersionReply_ResultToJSON(message.matchResult);
    }
    if (message.wantedMinVersion !== 0) {
      obj.wantedMinVersion = Math.round(message.wantedMinVersion);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<MatchOSVersionReply>, I>>(base?: I): MatchOSVersionReply {
    return MatchOSVersionReply.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<MatchOSVersionReply>, I>>(object: I): MatchOSVersionReply {
    const message = createBaseMatchOSVersionReply();
    message.boxosVersion = object.boxosVersion ?? 0;
    message.matchResult = object.matchResult ?? 0;
    message.wantedMinVersion = object.wantedMinVersion ?? 0;
    return message;
  },
};

function createBaseClientVersionRule(): ClientVersionRule {
  return { clientId: "", clientVersion: 0, boxosMinVersion: 0 };
}

export const ClientVersionRule = {
  encode(message: ClientVersionRule, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.clientId !== "") {
      writer.uint32(10).string(message.clientId);
    }
    if (message.clientVersion !== 0) {
      writer.uint32(16).int32(message.clientVersion);
    }
    if (message.boxosMinVersion !== 0) {
      writer.uint32(24).int32(message.boxosMinVersion);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ClientVersionRule {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseClientVersionRule();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.clientId = reader.string();
          continue;
        case 2:
          if (tag !== 16) {
            break;
          }

          message.clientVersion = reader.int32();
          continue;
        case 3:
          if (tag !== 24) {
            break;
          }

          message.boxosMinVersion = reader.int32();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): ClientVersionRule {
    return {
      clientId: isSet(object.clientId) ? String(object.clientId) : "",
      clientVersion: isSet(object.clientVersion) ? Number(object.clientVersion) : 0,
      boxosMinVersion: isSet(object.boxosMinVersion) ? Number(object.boxosMinVersion) : 0,
    };
  },

  toJSON(message: ClientVersionRule): unknown {
    const obj: any = {};
    if (message.clientId !== "") {
      obj.clientId = message.clientId;
    }
    if (message.clientVersion !== 0) {
      obj.clientVersion = Math.round(message.clientVersion);
    }
    if (message.boxosMinVersion !== 0) {
      obj.boxosMinVersion = Math.round(message.boxosMinVersion);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<ClientVersionRule>, I>>(base?: I): ClientVersionRule {
    return ClientVersionRule.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<ClientVersionRule>, I>>(object: I): ClientVersionRule {
    const message = createBaseClientVersionRule();
    message.clientId = object.clientId ?? "";
    message.clientVersion = object.clientVersion ?? 0;
    message.boxosMinVersion = object.boxosMinVersion ?? 0;
    return message;
  },
};

function createBaseBoxSystemStatus(): BoxSystemStatus {
  return { status: 0, serviceUrl: "", exceptionReason: undefined };
}

export const BoxSystemStatus = {
  encode(message: BoxSystemStatus, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.status !== 0) {
      writer.uint32(8).int32(message.status);
    }
    if (message.serviceUrl !== "") {
      writer.uint32(18).string(message.serviceUrl);
    }
    if (message.exceptionReason !== undefined) {
      writer.uint32(24).int32(message.exceptionReason);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BoxSystemStatus {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseBoxSystemStatus();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.status = reader.int32() as any;
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.serviceUrl = reader.string();
          continue;
        case 3:
          if (tag !== 24) {
            break;
          }

          message.exceptionReason = reader.int32() as any;
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): BoxSystemStatus {
    return {
      status: isSet(object.status) ? boxSystemStatus_SysStatusFromJSON(object.status) : 0,
      serviceUrl: isSet(object.serviceUrl) ? String(object.serviceUrl) : "",
      exceptionReason: isSet(object.exceptionReason)
        ? boxSystemStatus_ExceptionReasonFromJSON(object.exceptionReason)
        : undefined,
    };
  },

  toJSON(message: BoxSystemStatus): unknown {
    const obj: any = {};
    if (message.status !== 0) {
      obj.status = boxSystemStatus_SysStatusToJSON(message.status);
    }
    if (message.serviceUrl !== "") {
      obj.serviceUrl = message.serviceUrl;
    }
    if (message.exceptionReason !== undefined) {
      obj.exceptionReason = boxSystemStatus_ExceptionReasonToJSON(message.exceptionReason);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<BoxSystemStatus>, I>>(base?: I): BoxSystemStatus {
    return BoxSystemStatus.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<BoxSystemStatus>, I>>(object: I): BoxSystemStatus {
    const message = createBaseBoxSystemStatus();
    message.status = object.status ?? 0;
    message.serviceUrl = object.serviceUrl ?? "";
    message.exceptionReason = object.exceptionReason ?? undefined;
    return message;
  },
};

export interface BasicBridge {
  /**
   * 新增受信任电话号码
   * 如果没有传递验证码那么会发送验证码
   * 如果传递了验证码会验证验证码
   */
  NewTrustPhoneNumber(
    request: DeepPartial<NewTrustPhoneNumberRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<NewTrustPhoneNumberResponse>;
  /** 列出用户所有手机号 */
  ListPhoneNumber(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<ListPhoneNumberResponse>;
  /** 移除手机号 */
  RemovePhoneNumber(
    request: DeepPartial<RemovePhoneNumberRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<RemovePhoneNumberResponse>;
  /** 未读消息数量 */
  UnreadMsgCount(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<unreadMsgCountResponse>;
  /** 查询客户端与盒子系统的版本是否匹配 */
  MatchOSVersion(
    request: DeepPartial<MatchOSVersionRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<MatchOSVersionReply>;
  /** 单向流的方式获取微服状态 */
  LatestSysStatus(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Observable<BoxSystemStatus>;
}

export class BasicBridgeClientImpl implements BasicBridge {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.NewTrustPhoneNumber = this.NewTrustPhoneNumber.bind(this);
    this.ListPhoneNumber = this.ListPhoneNumber.bind(this);
    this.RemovePhoneNumber = this.RemovePhoneNumber.bind(this);
    this.UnreadMsgCount = this.UnreadMsgCount.bind(this);
    this.MatchOSVersion = this.MatchOSVersion.bind(this);
    this.LatestSysStatus = this.LatestSysStatus.bind(this);
  }

  NewTrustPhoneNumber(
    request: DeepPartial<NewTrustPhoneNumberRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<NewTrustPhoneNumberResponse> {
    return this.rpc.unary(
      BasicBridgeNewTrustPhoneNumberDesc,
      NewTrustPhoneNumberRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  ListPhoneNumber(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<ListPhoneNumberResponse> {
    return this.rpc.unary(BasicBridgeListPhoneNumberDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  RemovePhoneNumber(
    request: DeepPartial<RemovePhoneNumberRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<RemovePhoneNumberResponse> {
    return this.rpc.unary(
      BasicBridgeRemovePhoneNumberDesc,
      RemovePhoneNumberRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  UnreadMsgCount(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<unreadMsgCountResponse> {
    return this.rpc.unary(BasicBridgeUnreadMsgCountDesc, Empty.fromPartial(request), metadata, abortSignal);
  }

  MatchOSVersion(
    request: DeepPartial<MatchOSVersionRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<MatchOSVersionReply> {
    return this.rpc.unary(
      BasicBridgeMatchOSVersionDesc,
      MatchOSVersionRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  LatestSysStatus(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Observable<BoxSystemStatus> {
    return this.rpc.invoke(BasicBridgeLatestSysStatusDesc, Empty.fromPartial(request), metadata, abortSignal);
  }
}

export const BasicBridgeDesc = { serviceName: "cloud.lazycat.bridge.BasicBridge" };

export const BasicBridgeNewTrustPhoneNumberDesc: UnaryMethodDefinitionish = {
  methodName: "NewTrustPhoneNumber",
  service: BasicBridgeDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return NewTrustPhoneNumberRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = NewTrustPhoneNumberResponse.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const BasicBridgeListPhoneNumberDesc: UnaryMethodDefinitionish = {
  methodName: "ListPhoneNumber",
  service: BasicBridgeDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = ListPhoneNumberResponse.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const BasicBridgeRemovePhoneNumberDesc: UnaryMethodDefinitionish = {
  methodName: "RemovePhoneNumber",
  service: BasicBridgeDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return RemovePhoneNumberRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = RemovePhoneNumberResponse.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const BasicBridgeUnreadMsgCountDesc: UnaryMethodDefinitionish = {
  methodName: "UnreadMsgCount",
  service: BasicBridgeDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = unreadMsgCountResponse.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const BasicBridgeMatchOSVersionDesc: UnaryMethodDefinitionish = {
  methodName: "MatchOSVersion",
  service: BasicBridgeDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return MatchOSVersionRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = MatchOSVersionReply.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const BasicBridgeLatestSysStatusDesc: UnaryMethodDefinitionish = {
  methodName: "LatestSysStatus",
  service: BasicBridgeDesc,
  requestStream: false,
  responseStream: true,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = BoxSystemStatus.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Promise<any>;
  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Observable<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;
    streamingTransport?: grpc.TransportFactory;
    debug?: boolean;
    metadata?: grpc.Metadata;
    upStreamRetryCodes?: number[];
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;
      streamingTransport?: grpc.TransportFactory;
      debug?: boolean;
      metadata?: grpc.Metadata;
      upStreamRetryCodes?: number[];
    },
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata = metadata && this.options.metadata
      ? new BrowserHeaders({ ...this.options?.metadata.headersMap, ...metadata?.headersMap })
      : metadata ?? this.options.metadata;
    return new Promise((resolve, reject) => {
      const client = grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata ?? {},
        ...(this.options.transport !== undefined ? { transport: this.options.transport } : {}),
        debug: this.options.debug ?? false,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message!.toObject());
          } else {
            const err = new GrpcWebError(response.statusMessage, response.status, response.trailers);
            reject(err);
          }
        },
      });

      if (abortSignal) {
        abortSignal.addEventListener("abort", () => {
          client.close();
          reject(abortSignal.reason);
        });
      }
    });
  }

  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Observable<any> {
    const upStreamCodes = this.options.upStreamRetryCodes ?? [];
    const DEFAULT_TIMEOUT_TIME: number = 3_000;
    const request = { ..._request, ...methodDesc.requestType };
    const transport = this.options.streamingTransport ?? this.options.transport;
    const maybeCombinedMetadata = metadata && this.options.metadata
      ? new BrowserHeaders({ ...this.options?.metadata.headersMap, ...metadata?.headersMap })
      : metadata ?? this.options.metadata;
    return new Observable((observer) => {
      const upStream = () => {
        const client = grpc.invoke(methodDesc, {
          host: this.host,
          request,
          ...(transport !== undefined ? { transport } : {}),
          metadata: maybeCombinedMetadata ?? {},
          debug: this.options.debug ?? false,
          onMessage: (next) => observer.next(next),
          onEnd: (code: grpc.Code, message: string, trailers: grpc.Metadata) => {
            if (code === 0) {
              observer.complete();
            } else if (upStreamCodes.includes(code)) {
              setTimeout(upStream, DEFAULT_TIMEOUT_TIME);
            } else {
              const err = new Error(message) as any;
              err.code = code;
              err.metadata = trailers;
              observer.error(err);
            }
          },
        });
        observer.add(() => {
          if (!abortSignal || !abortSignal.aborted) {
            return client.close();
          }
        });

        if (abortSignal) {
          abortSignal.addEventListener("abort", () => {
            observer.error(abortSignal.reason);
            client.close();
          });
        }
      };
      upStream();
    }).pipe(share());
  }
}

declare const self: any | undefined;
declare const window: any | undefined;
declare const global: any | undefined;
const tsProtoGlobalThis: any = (() => {
  if (typeof globalThis !== "undefined") {
    return globalThis;
  }
  if (typeof self !== "undefined") {
    return self;
  }
  if (typeof window !== "undefined") {
    return window;
  }
  if (typeof global !== "undefined") {
    return global;
  }
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new tsProtoGlobalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

export class GrpcWebError extends tsProtoGlobalThis.Error {
  constructor(message: string, public code: grpc.Code, public metadata: grpc.Metadata) {
    super(message);
  }
}
