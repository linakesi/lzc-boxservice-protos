/* eslint-disable */
import { grpc } from "@improbable-eng/grpc-web";
import { BrowserHeaders } from "browser-headers";
import _m0 from "protobufjs/minimal";
import { Observable } from "rxjs";
import { share } from "rxjs/operators";
import { Empty } from "../../google/protobuf/empty";

export interface TrustDeviceRequest {
  user: string;
  password: string;
  /** 指定的预备手机号 */
  phoneNumber: string;
  verificationCode?: string | undefined;
}

export interface TrustDeviceResponse {
  status: TrustDeviceResponse_TrustDeviceStatus;
  reason: string;
}

export enum TrustDeviceResponse_TrustDeviceStatus {
  /** PHONENUMBER_INVALID - 手机号不合法 */
  PHONENUMBER_INVALID = 0,
  /** WAIT_VERIFYCODE - 等待验证码 */
  WAIT_VERIFYCODE = 1,
  /** DEVICE_TRUSTED - 已添加至受信任设备 */
  DEVICE_TRUSTED = 2,
  /** USERINFO_INVALID - 密码有误,用户信息有误 */
  USERINFO_INVALID = 3,
  /** OPERATION_FAILED - 操作失败 */
  OPERATION_FAILED = 4,
  UNRECOGNIZED = -1,
}

export function trustDeviceResponse_TrustDeviceStatusFromJSON(object: any): TrustDeviceResponse_TrustDeviceStatus {
  switch (object) {
    case 0:
    case "PHONENUMBER_INVALID":
      return TrustDeviceResponse_TrustDeviceStatus.PHONENUMBER_INVALID;
    case 1:
    case "WAIT_VERIFYCODE":
      return TrustDeviceResponse_TrustDeviceStatus.WAIT_VERIFYCODE;
    case 2:
    case "DEVICE_TRUSTED":
      return TrustDeviceResponse_TrustDeviceStatus.DEVICE_TRUSTED;
    case 3:
    case "USERINFO_INVALID":
      return TrustDeviceResponse_TrustDeviceStatus.USERINFO_INVALID;
    case 4:
    case "OPERATION_FAILED":
      return TrustDeviceResponse_TrustDeviceStatus.OPERATION_FAILED;
    case -1:
    case "UNRECOGNIZED":
    default:
      return TrustDeviceResponse_TrustDeviceStatus.UNRECOGNIZED;
  }
}

export function trustDeviceResponse_TrustDeviceStatusToJSON(object: TrustDeviceResponse_TrustDeviceStatus): string {
  switch (object) {
    case TrustDeviceResponse_TrustDeviceStatus.PHONENUMBER_INVALID:
      return "PHONENUMBER_INVALID";
    case TrustDeviceResponse_TrustDeviceStatus.WAIT_VERIFYCODE:
      return "WAIT_VERIFYCODE";
    case TrustDeviceResponse_TrustDeviceStatus.DEVICE_TRUSTED:
      return "DEVICE_TRUSTED";
    case TrustDeviceResponse_TrustDeviceStatus.USERINFO_INVALID:
      return "USERINFO_INVALID";
    case TrustDeviceResponse_TrustDeviceStatus.OPERATION_FAILED:
      return "OPERATION_FAILED";
    case TrustDeviceResponse_TrustDeviceStatus.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface ForgetPasswordRequest {
  uid: string;
  token: string;
  /** 新密码 */
  password: string;
}

export interface ForgetPasswdResponse {
  status: ForgetPasswdResponse_ForgetPasswdStatus;
  reason: string;
}

export enum ForgetPasswdResponse_ForgetPasswdStatus {
  /** SUCCESS - 成功 */
  SUCCESS = 0,
  /** TOKEN_INVALID - Token无效 */
  TOKEN_INVALID = 1,
  /** USER_NOT_FOUND - 没有此用户 */
  USER_NOT_FOUND = 2,
  /** OPERATION_FAILED - 操作失败 */
  OPERATION_FAILED = 3,
  /** PENDING_BUTTON_CLICK - 等待按钮点击 */
  PENDING_BUTTON_CLICK = 4,
  /** WAIT_BUTTON_TIMEOUT - 等待按按钮超时 */
  WAIT_BUTTON_TIMEOUT = 5,
  UNRECOGNIZED = -1,
}

export function forgetPasswdResponse_ForgetPasswdStatusFromJSON(object: any): ForgetPasswdResponse_ForgetPasswdStatus {
  switch (object) {
    case 0:
    case "SUCCESS":
      return ForgetPasswdResponse_ForgetPasswdStatus.SUCCESS;
    case 1:
    case "TOKEN_INVALID":
      return ForgetPasswdResponse_ForgetPasswdStatus.TOKEN_INVALID;
    case 2:
    case "USER_NOT_FOUND":
      return ForgetPasswdResponse_ForgetPasswdStatus.USER_NOT_FOUND;
    case 3:
    case "OPERATION_FAILED":
      return ForgetPasswdResponse_ForgetPasswdStatus.OPERATION_FAILED;
    case 4:
    case "PENDING_BUTTON_CLICK":
      return ForgetPasswdResponse_ForgetPasswdStatus.PENDING_BUTTON_CLICK;
    case 5:
    case "WAIT_BUTTON_TIMEOUT":
      return ForgetPasswdResponse_ForgetPasswdStatus.WAIT_BUTTON_TIMEOUT;
    case -1:
    case "UNRECOGNIZED":
    default:
      return ForgetPasswdResponse_ForgetPasswdStatus.UNRECOGNIZED;
  }
}

export function forgetPasswdResponse_ForgetPasswdStatusToJSON(object: ForgetPasswdResponse_ForgetPasswdStatus): string {
  switch (object) {
    case ForgetPasswdResponse_ForgetPasswdStatus.SUCCESS:
      return "SUCCESS";
    case ForgetPasswdResponse_ForgetPasswdStatus.TOKEN_INVALID:
      return "TOKEN_INVALID";
    case ForgetPasswdResponse_ForgetPasswdStatus.USER_NOT_FOUND:
      return "USER_NOT_FOUND";
    case ForgetPasswdResponse_ForgetPasswdStatus.OPERATION_FAILED:
      return "OPERATION_FAILED";
    case ForgetPasswdResponse_ForgetPasswdStatus.PENDING_BUTTON_CLICK:
      return "PENDING_BUTTON_CLICK";
    case ForgetPasswdResponse_ForgetPasswdStatus.WAIT_BUTTON_TIMEOUT:
      return "WAIT_BUTTON_TIMEOUT";
    case ForgetPasswdResponse_ForgetPasswdStatus.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface NewForgetPasswdTokenRequest {
  uid: string;
  securityCode?: string | undefined;
  phoneNumber?: string | undefined;
  verifyCode?: string | undefined;
}

export interface NewForgetPasswdTokenResponse {
  token: string;
}

export interface JoinWithInviteCodeRequest {
  token: string;
  username: string;
  password: string;
}

export interface JoinWithInviteCodeResponse {
  success: boolean;
}

export interface VerifyInviteCodeRequest {
  token: string;
}

export interface VerifyInviteCodeResponse {
  success: boolean;
}

export interface NoTrustDeviceAdminRequest {
  uid: string;
  passwd: string;
}

export interface NoTrustDeviceAdminReply {
  status: NoTrustDeviceAdminReply_Status;
  reason: string;
}

export enum NoTrustDeviceAdminReply_Status {
  SUCCESS = 0,
  PENDING_BUTTON_CLICK = 1,
  WAIT_BUTTON_TIMEOUT = 2,
  AUTH_FAILED = 3,
  OPERATION_FAILED = 4,
  UNRECOGNIZED = -1,
}

export function noTrustDeviceAdminReply_StatusFromJSON(object: any): NoTrustDeviceAdminReply_Status {
  switch (object) {
    case 0:
    case "SUCCESS":
      return NoTrustDeviceAdminReply_Status.SUCCESS;
    case 1:
    case "PENDING_BUTTON_CLICK":
      return NoTrustDeviceAdminReply_Status.PENDING_BUTTON_CLICK;
    case 2:
    case "WAIT_BUTTON_TIMEOUT":
      return NoTrustDeviceAdminReply_Status.WAIT_BUTTON_TIMEOUT;
    case 3:
    case "AUTH_FAILED":
      return NoTrustDeviceAdminReply_Status.AUTH_FAILED;
    case 4:
    case "OPERATION_FAILED":
      return NoTrustDeviceAdminReply_Status.OPERATION_FAILED;
    case -1:
    case "UNRECOGNIZED":
    default:
      return NoTrustDeviceAdminReply_Status.UNRECOGNIZED;
  }
}

export function noTrustDeviceAdminReply_StatusToJSON(object: NoTrustDeviceAdminReply_Status): string {
  switch (object) {
    case NoTrustDeviceAdminReply_Status.SUCCESS:
      return "SUCCESS";
    case NoTrustDeviceAdminReply_Status.PENDING_BUTTON_CLICK:
      return "PENDING_BUTTON_CLICK";
    case NoTrustDeviceAdminReply_Status.WAIT_BUTTON_TIMEOUT:
      return "WAIT_BUTTON_TIMEOUT";
    case NoTrustDeviceAdminReply_Status.AUTH_FAILED:
      return "AUTH_FAILED";
    case NoTrustDeviceAdminReply_Status.OPERATION_FAILED:
      return "OPERATION_FAILED";
    case NoTrustDeviceAdminReply_Status.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface NoTrustDeviceUserRequest {
  uid: string;
  passwd: string;
}

export interface NewForgetPasswdTokenAdminRequest {
  uid: string;
}

export interface NewForgetPasswdTokenAdminRely {
  token?: string | undefined;
  status: NewForgetPasswdTokenAdminRely_Status;
  reason: string;
}

export enum NewForgetPasswdTokenAdminRely_Status {
  SUCCESS = 0,
  PENDING_BUTTON_CLICK = 1,
  WAIT_BUTTON_TIMEOUT = 2,
  OPERATION_FAILED = 3,
  UNRECOGNIZED = -1,
}

export function newForgetPasswdTokenAdminRely_StatusFromJSON(object: any): NewForgetPasswdTokenAdminRely_Status {
  switch (object) {
    case 0:
    case "SUCCESS":
      return NewForgetPasswdTokenAdminRely_Status.SUCCESS;
    case 1:
    case "PENDING_BUTTON_CLICK":
      return NewForgetPasswdTokenAdminRely_Status.PENDING_BUTTON_CLICK;
    case 2:
    case "WAIT_BUTTON_TIMEOUT":
      return NewForgetPasswdTokenAdminRely_Status.WAIT_BUTTON_TIMEOUT;
    case 3:
    case "OPERATION_FAILED":
      return NewForgetPasswdTokenAdminRely_Status.OPERATION_FAILED;
    case -1:
    case "UNRECOGNIZED":
    default:
      return NewForgetPasswdTokenAdminRely_Status.UNRECOGNIZED;
  }
}

export function newForgetPasswdTokenAdminRely_StatusToJSON(object: NewForgetPasswdTokenAdminRely_Status): string {
  switch (object) {
    case NewForgetPasswdTokenAdminRely_Status.SUCCESS:
      return "SUCCESS";
    case NewForgetPasswdTokenAdminRely_Status.PENDING_BUTTON_CLICK:
      return "PENDING_BUTTON_CLICK";
    case NewForgetPasswdTokenAdminRely_Status.WAIT_BUTTON_TIMEOUT:
      return "WAIT_BUTTON_TIMEOUT";
    case NewForgetPasswdTokenAdminRely_Status.OPERATION_FAILED:
      return "OPERATION_FAILED";
    case NewForgetPasswdTokenAdminRely_Status.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

export interface QueryUserRoleRequest {
  uid: string;
  passwd: string;
}

export interface QueryUserRoleResponse {
  role: QueryUserRoleResponse_Role;
}

export enum QueryUserRoleResponse_Role {
  USER = 0,
  ADMIN = 1,
  UNRECOGNIZED = -1,
}

export function queryUserRoleResponse_RoleFromJSON(object: any): QueryUserRoleResponse_Role {
  switch (object) {
    case 0:
    case "USER":
      return QueryUserRoleResponse_Role.USER;
    case 1:
    case "ADMIN":
      return QueryUserRoleResponse_Role.ADMIN;
    case -1:
    case "UNRECOGNIZED":
    default:
      return QueryUserRoleResponse_Role.UNRECOGNIZED;
  }
}

export function queryUserRoleResponse_RoleToJSON(object: QueryUserRoleResponse_Role): string {
  switch (object) {
    case QueryUserRoleResponse_Role.USER:
      return "USER";
    case QueryUserRoleResponse_Role.ADMIN:
      return "ADMIN";
    case QueryUserRoleResponse_Role.UNRECOGNIZED:
    default:
      return "UNRECOGNIZED";
  }
}

function createBaseTrustDeviceRequest(): TrustDeviceRequest {
  return { user: "", password: "", phoneNumber: "", verificationCode: undefined };
}

export const TrustDeviceRequest = {
  encode(message: TrustDeviceRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.user !== "") {
      writer.uint32(18).string(message.user);
    }
    if (message.password !== "") {
      writer.uint32(26).string(message.password);
    }
    if (message.phoneNumber !== "") {
      writer.uint32(34).string(message.phoneNumber);
    }
    if (message.verificationCode !== undefined) {
      writer.uint32(42).string(message.verificationCode);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): TrustDeviceRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseTrustDeviceRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 2:
          if (tag !== 18) {
            break;
          }

          message.user = reader.string();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.password = reader.string();
          continue;
        case 4:
          if (tag !== 34) {
            break;
          }

          message.phoneNumber = reader.string();
          continue;
        case 5:
          if (tag !== 42) {
            break;
          }

          message.verificationCode = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): TrustDeviceRequest {
    return {
      user: isSet(object.user) ? String(object.user) : "",
      password: isSet(object.password) ? String(object.password) : "",
      phoneNumber: isSet(object.phoneNumber) ? String(object.phoneNumber) : "",
      verificationCode: isSet(object.verificationCode) ? String(object.verificationCode) : undefined,
    };
  },

  toJSON(message: TrustDeviceRequest): unknown {
    const obj: any = {};
    if (message.user !== "") {
      obj.user = message.user;
    }
    if (message.password !== "") {
      obj.password = message.password;
    }
    if (message.phoneNumber !== "") {
      obj.phoneNumber = message.phoneNumber;
    }
    if (message.verificationCode !== undefined) {
      obj.verificationCode = message.verificationCode;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<TrustDeviceRequest>, I>>(base?: I): TrustDeviceRequest {
    return TrustDeviceRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<TrustDeviceRequest>, I>>(object: I): TrustDeviceRequest {
    const message = createBaseTrustDeviceRequest();
    message.user = object.user ?? "";
    message.password = object.password ?? "";
    message.phoneNumber = object.phoneNumber ?? "";
    message.verificationCode = object.verificationCode ?? undefined;
    return message;
  },
};

function createBaseTrustDeviceResponse(): TrustDeviceResponse {
  return { status: 0, reason: "" };
}

export const TrustDeviceResponse = {
  encode(message: TrustDeviceResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.status !== 0) {
      writer.uint32(8).int32(message.status);
    }
    if (message.reason !== "") {
      writer.uint32(18).string(message.reason);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): TrustDeviceResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseTrustDeviceResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.status = reader.int32() as any;
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.reason = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): TrustDeviceResponse {
    return {
      status: isSet(object.status) ? trustDeviceResponse_TrustDeviceStatusFromJSON(object.status) : 0,
      reason: isSet(object.reason) ? String(object.reason) : "",
    };
  },

  toJSON(message: TrustDeviceResponse): unknown {
    const obj: any = {};
    if (message.status !== 0) {
      obj.status = trustDeviceResponse_TrustDeviceStatusToJSON(message.status);
    }
    if (message.reason !== "") {
      obj.reason = message.reason;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<TrustDeviceResponse>, I>>(base?: I): TrustDeviceResponse {
    return TrustDeviceResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<TrustDeviceResponse>, I>>(object: I): TrustDeviceResponse {
    const message = createBaseTrustDeviceResponse();
    message.status = object.status ?? 0;
    message.reason = object.reason ?? "";
    return message;
  },
};

function createBaseForgetPasswordRequest(): ForgetPasswordRequest {
  return { uid: "", token: "", password: "" };
}

export const ForgetPasswordRequest = {
  encode(message: ForgetPasswordRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uid !== "") {
      writer.uint32(10).string(message.uid);
    }
    if (message.token !== "") {
      writer.uint32(18).string(message.token);
    }
    if (message.password !== "") {
      writer.uint32(26).string(message.password);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ForgetPasswordRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseForgetPasswordRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uid = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.token = reader.string();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.password = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): ForgetPasswordRequest {
    return {
      uid: isSet(object.uid) ? String(object.uid) : "",
      token: isSet(object.token) ? String(object.token) : "",
      password: isSet(object.password) ? String(object.password) : "",
    };
  },

  toJSON(message: ForgetPasswordRequest): unknown {
    const obj: any = {};
    if (message.uid !== "") {
      obj.uid = message.uid;
    }
    if (message.token !== "") {
      obj.token = message.token;
    }
    if (message.password !== "") {
      obj.password = message.password;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<ForgetPasswordRequest>, I>>(base?: I): ForgetPasswordRequest {
    return ForgetPasswordRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<ForgetPasswordRequest>, I>>(object: I): ForgetPasswordRequest {
    const message = createBaseForgetPasswordRequest();
    message.uid = object.uid ?? "";
    message.token = object.token ?? "";
    message.password = object.password ?? "";
    return message;
  },
};

function createBaseForgetPasswdResponse(): ForgetPasswdResponse {
  return { status: 0, reason: "" };
}

export const ForgetPasswdResponse = {
  encode(message: ForgetPasswdResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.status !== 0) {
      writer.uint32(8).int32(message.status);
    }
    if (message.reason !== "") {
      writer.uint32(18).string(message.reason);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ForgetPasswdResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseForgetPasswdResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.status = reader.int32() as any;
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.reason = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): ForgetPasswdResponse {
    return {
      status: isSet(object.status) ? forgetPasswdResponse_ForgetPasswdStatusFromJSON(object.status) : 0,
      reason: isSet(object.reason) ? String(object.reason) : "",
    };
  },

  toJSON(message: ForgetPasswdResponse): unknown {
    const obj: any = {};
    if (message.status !== 0) {
      obj.status = forgetPasswdResponse_ForgetPasswdStatusToJSON(message.status);
    }
    if (message.reason !== "") {
      obj.reason = message.reason;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<ForgetPasswdResponse>, I>>(base?: I): ForgetPasswdResponse {
    return ForgetPasswdResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<ForgetPasswdResponse>, I>>(object: I): ForgetPasswdResponse {
    const message = createBaseForgetPasswdResponse();
    message.status = object.status ?? 0;
    message.reason = object.reason ?? "";
    return message;
  },
};

function createBaseNewForgetPasswdTokenRequest(): NewForgetPasswdTokenRequest {
  return { uid: "", securityCode: undefined, phoneNumber: undefined, verifyCode: undefined };
}

export const NewForgetPasswdTokenRequest = {
  encode(message: NewForgetPasswdTokenRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uid !== "") {
      writer.uint32(10).string(message.uid);
    }
    if (message.securityCode !== undefined) {
      writer.uint32(18).string(message.securityCode);
    }
    if (message.phoneNumber !== undefined) {
      writer.uint32(26).string(message.phoneNumber);
    }
    if (message.verifyCode !== undefined) {
      writer.uint32(34).string(message.verifyCode);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): NewForgetPasswdTokenRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseNewForgetPasswdTokenRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uid = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.securityCode = reader.string();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.phoneNumber = reader.string();
          continue;
        case 4:
          if (tag !== 34) {
            break;
          }

          message.verifyCode = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): NewForgetPasswdTokenRequest {
    return {
      uid: isSet(object.uid) ? String(object.uid) : "",
      securityCode: isSet(object.securityCode) ? String(object.securityCode) : undefined,
      phoneNumber: isSet(object.phoneNumber) ? String(object.phoneNumber) : undefined,
      verifyCode: isSet(object.verifyCode) ? String(object.verifyCode) : undefined,
    };
  },

  toJSON(message: NewForgetPasswdTokenRequest): unknown {
    const obj: any = {};
    if (message.uid !== "") {
      obj.uid = message.uid;
    }
    if (message.securityCode !== undefined) {
      obj.securityCode = message.securityCode;
    }
    if (message.phoneNumber !== undefined) {
      obj.phoneNumber = message.phoneNumber;
    }
    if (message.verifyCode !== undefined) {
      obj.verifyCode = message.verifyCode;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<NewForgetPasswdTokenRequest>, I>>(base?: I): NewForgetPasswdTokenRequest {
    return NewForgetPasswdTokenRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<NewForgetPasswdTokenRequest>, I>>(object: I): NewForgetPasswdTokenRequest {
    const message = createBaseNewForgetPasswdTokenRequest();
    message.uid = object.uid ?? "";
    message.securityCode = object.securityCode ?? undefined;
    message.phoneNumber = object.phoneNumber ?? undefined;
    message.verifyCode = object.verifyCode ?? undefined;
    return message;
  },
};

function createBaseNewForgetPasswdTokenResponse(): NewForgetPasswdTokenResponse {
  return { token: "" };
}

export const NewForgetPasswdTokenResponse = {
  encode(message: NewForgetPasswdTokenResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.token !== "") {
      writer.uint32(10).string(message.token);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): NewForgetPasswdTokenResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseNewForgetPasswdTokenResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.token = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): NewForgetPasswdTokenResponse {
    return { token: isSet(object.token) ? String(object.token) : "" };
  },

  toJSON(message: NewForgetPasswdTokenResponse): unknown {
    const obj: any = {};
    if (message.token !== "") {
      obj.token = message.token;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<NewForgetPasswdTokenResponse>, I>>(base?: I): NewForgetPasswdTokenResponse {
    return NewForgetPasswdTokenResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<NewForgetPasswdTokenResponse>, I>>(object: I): NewForgetPasswdTokenResponse {
    const message = createBaseNewForgetPasswdTokenResponse();
    message.token = object.token ?? "";
    return message;
  },
};

function createBaseJoinWithInviteCodeRequest(): JoinWithInviteCodeRequest {
  return { token: "", username: "", password: "" };
}

export const JoinWithInviteCodeRequest = {
  encode(message: JoinWithInviteCodeRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.token !== "") {
      writer.uint32(10).string(message.token);
    }
    if (message.username !== "") {
      writer.uint32(18).string(message.username);
    }
    if (message.password !== "") {
      writer.uint32(26).string(message.password);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): JoinWithInviteCodeRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseJoinWithInviteCodeRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.token = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.username = reader.string();
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.password = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): JoinWithInviteCodeRequest {
    return {
      token: isSet(object.token) ? String(object.token) : "",
      username: isSet(object.username) ? String(object.username) : "",
      password: isSet(object.password) ? String(object.password) : "",
    };
  },

  toJSON(message: JoinWithInviteCodeRequest): unknown {
    const obj: any = {};
    if (message.token !== "") {
      obj.token = message.token;
    }
    if (message.username !== "") {
      obj.username = message.username;
    }
    if (message.password !== "") {
      obj.password = message.password;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<JoinWithInviteCodeRequest>, I>>(base?: I): JoinWithInviteCodeRequest {
    return JoinWithInviteCodeRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<JoinWithInviteCodeRequest>, I>>(object: I): JoinWithInviteCodeRequest {
    const message = createBaseJoinWithInviteCodeRequest();
    message.token = object.token ?? "";
    message.username = object.username ?? "";
    message.password = object.password ?? "";
    return message;
  },
};

function createBaseJoinWithInviteCodeResponse(): JoinWithInviteCodeResponse {
  return { success: false };
}

export const JoinWithInviteCodeResponse = {
  encode(message: JoinWithInviteCodeResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.success === true) {
      writer.uint32(8).bool(message.success);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): JoinWithInviteCodeResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseJoinWithInviteCodeResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.success = reader.bool();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): JoinWithInviteCodeResponse {
    return { success: isSet(object.success) ? Boolean(object.success) : false };
  },

  toJSON(message: JoinWithInviteCodeResponse): unknown {
    const obj: any = {};
    if (message.success === true) {
      obj.success = message.success;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<JoinWithInviteCodeResponse>, I>>(base?: I): JoinWithInviteCodeResponse {
    return JoinWithInviteCodeResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<JoinWithInviteCodeResponse>, I>>(object: I): JoinWithInviteCodeResponse {
    const message = createBaseJoinWithInviteCodeResponse();
    message.success = object.success ?? false;
    return message;
  },
};

function createBaseVerifyInviteCodeRequest(): VerifyInviteCodeRequest {
  return { token: "" };
}

export const VerifyInviteCodeRequest = {
  encode(message: VerifyInviteCodeRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.token !== "") {
      writer.uint32(10).string(message.token);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): VerifyInviteCodeRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseVerifyInviteCodeRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.token = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): VerifyInviteCodeRequest {
    return { token: isSet(object.token) ? String(object.token) : "" };
  },

  toJSON(message: VerifyInviteCodeRequest): unknown {
    const obj: any = {};
    if (message.token !== "") {
      obj.token = message.token;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<VerifyInviteCodeRequest>, I>>(base?: I): VerifyInviteCodeRequest {
    return VerifyInviteCodeRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<VerifyInviteCodeRequest>, I>>(object: I): VerifyInviteCodeRequest {
    const message = createBaseVerifyInviteCodeRequest();
    message.token = object.token ?? "";
    return message;
  },
};

function createBaseVerifyInviteCodeResponse(): VerifyInviteCodeResponse {
  return { success: false };
}

export const VerifyInviteCodeResponse = {
  encode(message: VerifyInviteCodeResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.success === true) {
      writer.uint32(8).bool(message.success);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): VerifyInviteCodeResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseVerifyInviteCodeResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.success = reader.bool();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): VerifyInviteCodeResponse {
    return { success: isSet(object.success) ? Boolean(object.success) : false };
  },

  toJSON(message: VerifyInviteCodeResponse): unknown {
    const obj: any = {};
    if (message.success === true) {
      obj.success = message.success;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<VerifyInviteCodeResponse>, I>>(base?: I): VerifyInviteCodeResponse {
    return VerifyInviteCodeResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<VerifyInviteCodeResponse>, I>>(object: I): VerifyInviteCodeResponse {
    const message = createBaseVerifyInviteCodeResponse();
    message.success = object.success ?? false;
    return message;
  },
};

function createBaseNoTrustDeviceAdminRequest(): NoTrustDeviceAdminRequest {
  return { uid: "", passwd: "" };
}

export const NoTrustDeviceAdminRequest = {
  encode(message: NoTrustDeviceAdminRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uid !== "") {
      writer.uint32(10).string(message.uid);
    }
    if (message.passwd !== "") {
      writer.uint32(18).string(message.passwd);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): NoTrustDeviceAdminRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseNoTrustDeviceAdminRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uid = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.passwd = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): NoTrustDeviceAdminRequest {
    return {
      uid: isSet(object.uid) ? String(object.uid) : "",
      passwd: isSet(object.passwd) ? String(object.passwd) : "",
    };
  },

  toJSON(message: NoTrustDeviceAdminRequest): unknown {
    const obj: any = {};
    if (message.uid !== "") {
      obj.uid = message.uid;
    }
    if (message.passwd !== "") {
      obj.passwd = message.passwd;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<NoTrustDeviceAdminRequest>, I>>(base?: I): NoTrustDeviceAdminRequest {
    return NoTrustDeviceAdminRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<NoTrustDeviceAdminRequest>, I>>(object: I): NoTrustDeviceAdminRequest {
    const message = createBaseNoTrustDeviceAdminRequest();
    message.uid = object.uid ?? "";
    message.passwd = object.passwd ?? "";
    return message;
  },
};

function createBaseNoTrustDeviceAdminReply(): NoTrustDeviceAdminReply {
  return { status: 0, reason: "" };
}

export const NoTrustDeviceAdminReply = {
  encode(message: NoTrustDeviceAdminReply, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.status !== 0) {
      writer.uint32(8).int32(message.status);
    }
    if (message.reason !== "") {
      writer.uint32(18).string(message.reason);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): NoTrustDeviceAdminReply {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseNoTrustDeviceAdminReply();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.status = reader.int32() as any;
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.reason = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): NoTrustDeviceAdminReply {
    return {
      status: isSet(object.status) ? noTrustDeviceAdminReply_StatusFromJSON(object.status) : 0,
      reason: isSet(object.reason) ? String(object.reason) : "",
    };
  },

  toJSON(message: NoTrustDeviceAdminReply): unknown {
    const obj: any = {};
    if (message.status !== 0) {
      obj.status = noTrustDeviceAdminReply_StatusToJSON(message.status);
    }
    if (message.reason !== "") {
      obj.reason = message.reason;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<NoTrustDeviceAdminReply>, I>>(base?: I): NoTrustDeviceAdminReply {
    return NoTrustDeviceAdminReply.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<NoTrustDeviceAdminReply>, I>>(object: I): NoTrustDeviceAdminReply {
    const message = createBaseNoTrustDeviceAdminReply();
    message.status = object.status ?? 0;
    message.reason = object.reason ?? "";
    return message;
  },
};

function createBaseNoTrustDeviceUserRequest(): NoTrustDeviceUserRequest {
  return { uid: "", passwd: "" };
}

export const NoTrustDeviceUserRequest = {
  encode(message: NoTrustDeviceUserRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uid !== "") {
      writer.uint32(10).string(message.uid);
    }
    if (message.passwd !== "") {
      writer.uint32(18).string(message.passwd);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): NoTrustDeviceUserRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseNoTrustDeviceUserRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uid = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.passwd = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): NoTrustDeviceUserRequest {
    return {
      uid: isSet(object.uid) ? String(object.uid) : "",
      passwd: isSet(object.passwd) ? String(object.passwd) : "",
    };
  },

  toJSON(message: NoTrustDeviceUserRequest): unknown {
    const obj: any = {};
    if (message.uid !== "") {
      obj.uid = message.uid;
    }
    if (message.passwd !== "") {
      obj.passwd = message.passwd;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<NoTrustDeviceUserRequest>, I>>(base?: I): NoTrustDeviceUserRequest {
    return NoTrustDeviceUserRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<NoTrustDeviceUserRequest>, I>>(object: I): NoTrustDeviceUserRequest {
    const message = createBaseNoTrustDeviceUserRequest();
    message.uid = object.uid ?? "";
    message.passwd = object.passwd ?? "";
    return message;
  },
};

function createBaseNewForgetPasswdTokenAdminRequest(): NewForgetPasswdTokenAdminRequest {
  return { uid: "" };
}

export const NewForgetPasswdTokenAdminRequest = {
  encode(message: NewForgetPasswdTokenAdminRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uid !== "") {
      writer.uint32(10).string(message.uid);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): NewForgetPasswdTokenAdminRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseNewForgetPasswdTokenAdminRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uid = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): NewForgetPasswdTokenAdminRequest {
    return { uid: isSet(object.uid) ? String(object.uid) : "" };
  },

  toJSON(message: NewForgetPasswdTokenAdminRequest): unknown {
    const obj: any = {};
    if (message.uid !== "") {
      obj.uid = message.uid;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<NewForgetPasswdTokenAdminRequest>, I>>(
    base?: I,
  ): NewForgetPasswdTokenAdminRequest {
    return NewForgetPasswdTokenAdminRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<NewForgetPasswdTokenAdminRequest>, I>>(
    object: I,
  ): NewForgetPasswdTokenAdminRequest {
    const message = createBaseNewForgetPasswdTokenAdminRequest();
    message.uid = object.uid ?? "";
    return message;
  },
};

function createBaseNewForgetPasswdTokenAdminRely(): NewForgetPasswdTokenAdminRely {
  return { token: undefined, status: 0, reason: "" };
}

export const NewForgetPasswdTokenAdminRely = {
  encode(message: NewForgetPasswdTokenAdminRely, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.token !== undefined) {
      writer.uint32(10).string(message.token);
    }
    if (message.status !== 0) {
      writer.uint32(16).int32(message.status);
    }
    if (message.reason !== "") {
      writer.uint32(26).string(message.reason);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): NewForgetPasswdTokenAdminRely {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseNewForgetPasswdTokenAdminRely();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.token = reader.string();
          continue;
        case 2:
          if (tag !== 16) {
            break;
          }

          message.status = reader.int32() as any;
          continue;
        case 3:
          if (tag !== 26) {
            break;
          }

          message.reason = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): NewForgetPasswdTokenAdminRely {
    return {
      token: isSet(object.token) ? String(object.token) : undefined,
      status: isSet(object.status) ? newForgetPasswdTokenAdminRely_StatusFromJSON(object.status) : 0,
      reason: isSet(object.reason) ? String(object.reason) : "",
    };
  },

  toJSON(message: NewForgetPasswdTokenAdminRely): unknown {
    const obj: any = {};
    if (message.token !== undefined) {
      obj.token = message.token;
    }
    if (message.status !== 0) {
      obj.status = newForgetPasswdTokenAdminRely_StatusToJSON(message.status);
    }
    if (message.reason !== "") {
      obj.reason = message.reason;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<NewForgetPasswdTokenAdminRely>, I>>(base?: I): NewForgetPasswdTokenAdminRely {
    return NewForgetPasswdTokenAdminRely.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<NewForgetPasswdTokenAdminRely>, I>>(
    object: I,
  ): NewForgetPasswdTokenAdminRely {
    const message = createBaseNewForgetPasswdTokenAdminRely();
    message.token = object.token ?? undefined;
    message.status = object.status ?? 0;
    message.reason = object.reason ?? "";
    return message;
  },
};

function createBaseQueryUserRoleRequest(): QueryUserRoleRequest {
  return { uid: "", passwd: "" };
}

export const QueryUserRoleRequest = {
  encode(message: QueryUserRoleRequest, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.uid !== "") {
      writer.uint32(10).string(message.uid);
    }
    if (message.passwd !== "") {
      writer.uint32(18).string(message.passwd);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryUserRoleRequest {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryUserRoleRequest();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 10) {
            break;
          }

          message.uid = reader.string();
          continue;
        case 2:
          if (tag !== 18) {
            break;
          }

          message.passwd = reader.string();
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): QueryUserRoleRequest {
    return {
      uid: isSet(object.uid) ? String(object.uid) : "",
      passwd: isSet(object.passwd) ? String(object.passwd) : "",
    };
  },

  toJSON(message: QueryUserRoleRequest): unknown {
    const obj: any = {};
    if (message.uid !== "") {
      obj.uid = message.uid;
    }
    if (message.passwd !== "") {
      obj.passwd = message.passwd;
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<QueryUserRoleRequest>, I>>(base?: I): QueryUserRoleRequest {
    return QueryUserRoleRequest.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<QueryUserRoleRequest>, I>>(object: I): QueryUserRoleRequest {
    const message = createBaseQueryUserRoleRequest();
    message.uid = object.uid ?? "";
    message.passwd = object.passwd ?? "";
    return message;
  },
};

function createBaseQueryUserRoleResponse(): QueryUserRoleResponse {
  return { role: 0 };
}

export const QueryUserRoleResponse = {
  encode(message: QueryUserRoleResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.role !== 0) {
      writer.uint32(8).int32(message.role);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): QueryUserRoleResponse {
    const reader = input instanceof _m0.Reader ? input : _m0.Reader.create(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseQueryUserRoleResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          if (tag !== 8) {
            break;
          }

          message.role = reader.int32() as any;
          continue;
      }
      if ((tag & 7) === 4 || tag === 0) {
        break;
      }
      reader.skipType(tag & 7);
    }
    return message;
  },

  fromJSON(object: any): QueryUserRoleResponse {
    return { role: isSet(object.role) ? queryUserRoleResponse_RoleFromJSON(object.role) : 0 };
  },

  toJSON(message: QueryUserRoleResponse): unknown {
    const obj: any = {};
    if (message.role !== 0) {
      obj.role = queryUserRoleResponse_RoleToJSON(message.role);
    }
    return obj;
  },

  create<I extends Exact<DeepPartial<QueryUserRoleResponse>, I>>(base?: I): QueryUserRoleResponse {
    return QueryUserRoleResponse.fromPartial(base ?? {});
  },

  fromPartial<I extends Exact<DeepPartial<QueryUserRoleResponse>, I>>(object: I): QueryUserRoleResponse {
    const message = createBaseQueryUserRoleResponse();
    message.role = object.role ?? 0;
    return message;
  },
};

export interface UnsafeBridge {
  /**
   * 使用手机验证码的方式增加授信设备
   * 不传递VerifyCode时发送验证码
   * 传递了就会验证手机号和验证码并添加到受信任设备
   */
  TrustDevice(
    request: DeepPartial<TrustDeviceRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<TrustDeviceResponse>;
  /**
   * 使用token找回密码
   *
   * 普通用户使用unsafe的NewForgetPasswordToken生成token
   * 管理员用户使用admin的NewForgetPasswordTokenAdmin生成token
   */
  ForgetPassword(
    request: DeepPartial<ForgetPasswordRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<ForgetPasswdResponse>;
  /**
   * 获取用于找回密码的token
   * 普通用户可基于管理员生成的安全码或受信任手机号找回
   * 管理员用户可使用受信任手机号找回
   *
   * 传递手机号但是短信验证码未传递时 => 发送短信验证码
   * 传递手机号也传递短信验证码时 => 验证并返回找回密码的token
   * 传递了安全码时 => 验证并返回找回密码的token
   */
  NewForgetPasswordToken(
    request: DeepPartial<NewForgetPasswdTokenRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<NewForgetPasswdTokenResponse>;
  /** 生成用于忘记密码的token，管理员版本(通过功能按钮找回) */
  NewForgetPasswdTokenAdmin(
    request: DeepPartial<NewForgetPasswdTokenAdminRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Observable<NewForgetPasswdTokenAdminRely>;
  /**
   * 使用邀请二维码创建用户
   * token有效期为30分钟
   * 用户名格式: 1-32位小写英文字母或数字
   * 密码格式: 1-32位大小写英文字母或数字
   */
  JoinWithInviteCode(
    request: DeepPartial<JoinWithInviteCodeRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<JoinWithInviteCodeResponse>;
  /** 验证邀请码是否可用 */
  VerifyInviteCode(
    request: DeepPartial<VerifyInviteCodeRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<VerifyInviteCodeResponse>;
  /**
   * 没有受信任设备管理员处理方式
   * 按按钮后把发起请求的peer client，加入到受信任列表中
   */
  NoTrustDeviceAdmin(
    request: DeepPartial<NoTrustDeviceAdminRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Observable<NoTrustDeviceAdminReply>;
  /**
   * 没有受信任设备普通用户处理方式
   * 给管理员的消息列表中发送一条消息请求，管理员点击允许同意后才行
   * TODO: 最终版本应该是发送给管理员受信任设备验证码列表，界面在客户端内部
   */
  NoTrustDeviceUser(
    request: DeepPartial<NoTrustDeviceUserRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<Empty>;
  /**
   * TODO: 不应该接收用户密码，要利用DialerRole的规则
   * RequireDialerRole hserver.DialerRole_NORMAL_USER
   * RequireTrustDev false
   * 查询用户是否是管理员
   */
  QueryUserRole(
    request: DeepPartial<QueryUserRoleRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<QueryUserRoleResponse>;
}

export class UnsafeBridgeClientImpl implements UnsafeBridge {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.TrustDevice = this.TrustDevice.bind(this);
    this.ForgetPassword = this.ForgetPassword.bind(this);
    this.NewForgetPasswordToken = this.NewForgetPasswordToken.bind(this);
    this.NewForgetPasswdTokenAdmin = this.NewForgetPasswdTokenAdmin.bind(this);
    this.JoinWithInviteCode = this.JoinWithInviteCode.bind(this);
    this.VerifyInviteCode = this.VerifyInviteCode.bind(this);
    this.NoTrustDeviceAdmin = this.NoTrustDeviceAdmin.bind(this);
    this.NoTrustDeviceUser = this.NoTrustDeviceUser.bind(this);
    this.QueryUserRole = this.QueryUserRole.bind(this);
  }

  TrustDevice(
    request: DeepPartial<TrustDeviceRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<TrustDeviceResponse> {
    return this.rpc.unary(UnsafeBridgeTrustDeviceDesc, TrustDeviceRequest.fromPartial(request), metadata, abortSignal);
  }

  ForgetPassword(
    request: DeepPartial<ForgetPasswordRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<ForgetPasswdResponse> {
    return this.rpc.unary(
      UnsafeBridgeForgetPasswordDesc,
      ForgetPasswordRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  NewForgetPasswordToken(
    request: DeepPartial<NewForgetPasswdTokenRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<NewForgetPasswdTokenResponse> {
    return this.rpc.unary(
      UnsafeBridgeNewForgetPasswordTokenDesc,
      NewForgetPasswdTokenRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  NewForgetPasswdTokenAdmin(
    request: DeepPartial<NewForgetPasswdTokenAdminRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Observable<NewForgetPasswdTokenAdminRely> {
    return this.rpc.invoke(
      UnsafeBridgeNewForgetPasswdTokenAdminDesc,
      NewForgetPasswdTokenAdminRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  JoinWithInviteCode(
    request: DeepPartial<JoinWithInviteCodeRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<JoinWithInviteCodeResponse> {
    return this.rpc.unary(
      UnsafeBridgeJoinWithInviteCodeDesc,
      JoinWithInviteCodeRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  VerifyInviteCode(
    request: DeepPartial<VerifyInviteCodeRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<VerifyInviteCodeResponse> {
    return this.rpc.unary(
      UnsafeBridgeVerifyInviteCodeDesc,
      VerifyInviteCodeRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  NoTrustDeviceAdmin(
    request: DeepPartial<NoTrustDeviceAdminRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Observable<NoTrustDeviceAdminReply> {
    return this.rpc.invoke(
      UnsafeBridgeNoTrustDeviceAdminDesc,
      NoTrustDeviceAdminRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  NoTrustDeviceUser(
    request: DeepPartial<NoTrustDeviceUserRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<Empty> {
    return this.rpc.unary(
      UnsafeBridgeNoTrustDeviceUserDesc,
      NoTrustDeviceUserRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }

  QueryUserRole(
    request: DeepPartial<QueryUserRoleRequest>,
    metadata?: grpc.Metadata,
    abortSignal?: AbortSignal,
  ): Promise<QueryUserRoleResponse> {
    return this.rpc.unary(
      UnsafeBridgeQueryUserRoleDesc,
      QueryUserRoleRequest.fromPartial(request),
      metadata,
      abortSignal,
    );
  }
}

export const UnsafeBridgeDesc = { serviceName: "cloud.lazycat.bridge.UnsafeBridge" };

export const UnsafeBridgeTrustDeviceDesc: UnaryMethodDefinitionish = {
  methodName: "TrustDevice",
  service: UnsafeBridgeDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return TrustDeviceRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = TrustDeviceResponse.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const UnsafeBridgeForgetPasswordDesc: UnaryMethodDefinitionish = {
  methodName: "ForgetPassword",
  service: UnsafeBridgeDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ForgetPasswordRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = ForgetPasswdResponse.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const UnsafeBridgeNewForgetPasswordTokenDesc: UnaryMethodDefinitionish = {
  methodName: "NewForgetPasswordToken",
  service: UnsafeBridgeDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return NewForgetPasswdTokenRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = NewForgetPasswdTokenResponse.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const UnsafeBridgeNewForgetPasswdTokenAdminDesc: UnaryMethodDefinitionish = {
  methodName: "NewForgetPasswdTokenAdmin",
  service: UnsafeBridgeDesc,
  requestStream: false,
  responseStream: true,
  requestType: {
    serializeBinary() {
      return NewForgetPasswdTokenAdminRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = NewForgetPasswdTokenAdminRely.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const UnsafeBridgeJoinWithInviteCodeDesc: UnaryMethodDefinitionish = {
  methodName: "JoinWithInviteCode",
  service: UnsafeBridgeDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return JoinWithInviteCodeRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = JoinWithInviteCodeResponse.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const UnsafeBridgeVerifyInviteCodeDesc: UnaryMethodDefinitionish = {
  methodName: "VerifyInviteCode",
  service: UnsafeBridgeDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return VerifyInviteCodeRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = VerifyInviteCodeResponse.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const UnsafeBridgeNoTrustDeviceAdminDesc: UnaryMethodDefinitionish = {
  methodName: "NoTrustDeviceAdmin",
  service: UnsafeBridgeDesc,
  requestStream: false,
  responseStream: true,
  requestType: {
    serializeBinary() {
      return NoTrustDeviceAdminRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = NoTrustDeviceAdminReply.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const UnsafeBridgeNoTrustDeviceUserDesc: UnaryMethodDefinitionish = {
  methodName: "NoTrustDeviceUser",
  service: UnsafeBridgeDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return NoTrustDeviceUserRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = Empty.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

export const UnsafeBridgeQueryUserRoleDesc: UnaryMethodDefinitionish = {
  methodName: "QueryUserRole",
  service: UnsafeBridgeDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return QueryUserRoleRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      const value = QueryUserRoleResponse.decode(data);
      return {
        ...value,
        toObject() {
          return value;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Promise<any>;
  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Observable<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;
    streamingTransport?: grpc.TransportFactory;
    debug?: boolean;
    metadata?: grpc.Metadata;
    upStreamRetryCodes?: number[];
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;
      streamingTransport?: grpc.TransportFactory;
      debug?: boolean;
      metadata?: grpc.Metadata;
      upStreamRetryCodes?: number[];
    },
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata = metadata && this.options.metadata
      ? new BrowserHeaders({ ...this.options?.metadata.headersMap, ...metadata?.headersMap })
      : metadata ?? this.options.metadata;
    return new Promise((resolve, reject) => {
      const client = grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata ?? {},
        ...(this.options.transport !== undefined ? { transport: this.options.transport } : {}),
        debug: this.options.debug ?? false,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message!.toObject());
          } else {
            const err = new GrpcWebError(response.statusMessage, response.status, response.trailers);
            reject(err);
          }
        },
      });

      if (abortSignal) {
        abortSignal.addEventListener("abort", () => {
          client.close();
          reject(abortSignal.reason);
        });
      }
    });
  }

  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined,
    abortSignal?: AbortSignal,
  ): Observable<any> {
    const upStreamCodes = this.options.upStreamRetryCodes ?? [];
    const DEFAULT_TIMEOUT_TIME: number = 3_000;
    const request = { ..._request, ...methodDesc.requestType };
    const transport = this.options.streamingTransport ?? this.options.transport;
    const maybeCombinedMetadata = metadata && this.options.metadata
      ? new BrowserHeaders({ ...this.options?.metadata.headersMap, ...metadata?.headersMap })
      : metadata ?? this.options.metadata;
    return new Observable((observer) => {
      const upStream = () => {
        const client = grpc.invoke(methodDesc, {
          host: this.host,
          request,
          ...(transport !== undefined ? { transport } : {}),
          metadata: maybeCombinedMetadata ?? {},
          debug: this.options.debug ?? false,
          onMessage: (next) => observer.next(next),
          onEnd: (code: grpc.Code, message: string, trailers: grpc.Metadata) => {
            if (code === 0) {
              observer.complete();
            } else if (upStreamCodes.includes(code)) {
              setTimeout(upStream, DEFAULT_TIMEOUT_TIME);
            } else {
              const err = new Error(message) as any;
              err.code = code;
              err.metadata = trailers;
              observer.error(err);
            }
          },
        });
        observer.add(() => {
          if (!abortSignal || !abortSignal.aborted) {
            return client.close();
          }
        });

        if (abortSignal) {
          abortSignal.addEventListener("abort", () => {
            observer.error(abortSignal.reason);
            client.close();
          });
        }
      };
      upStream();
    }).pipe(share());
  }
}

declare const self: any | undefined;
declare const window: any | undefined;
declare const global: any | undefined;
const tsProtoGlobalThis: any = (() => {
  if (typeof globalThis !== "undefined") {
    return globalThis;
  }
  if (typeof self !== "undefined") {
    return self;
  }
  if (typeof window !== "undefined") {
    return window;
  }
  if (typeof global !== "undefined") {
    return global;
  }
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

export class GrpcWebError extends tsProtoGlobalThis.Error {
  constructor(message: string, public code: grpc.Code, public metadata: grpc.Metadata) {
    super(message);
  }
}
