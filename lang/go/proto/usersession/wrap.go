package usersession

import (
	"net"
	"os"

	grpc "google.golang.org/grpc"
)

//go:generate protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative portal-server.proto

var (
	SocketPath = "/run/lzc-sys/user-session.socket"
	LzcAppSocketPath = "/lzcapp/run/sys/user-session.socket"
)

func Serve(srv UserSessionServer) error {
	s := grpc.NewServer()

	RegisterUserSessionServer(s, srv)

	os.Remove(SocketPath)

	lis, err := net.Listen("unix", SocketPath)
	if err != nil {
		return err
	}
	os.Chmod(SocketPath, 0777)

	return s.Serve(lis)
}

type Client struct {
	UserSessionClient
	conn *grpc.ClientConn
}

func (c Client) Close() error { return c.conn.Close() }

func NewClient() (*Client, error) {
	opts := []grpc.DialOption{
		grpc.WithInsecure(),
	}
	conn, err := grpc.Dial("unix://"+LzcAppSocketPath, opts...)
	if err != nil {
		return nil, err
	}
	return &Client{UserSessionClient: NewUserSessionClient(conn), conn: conn}, nil
}
